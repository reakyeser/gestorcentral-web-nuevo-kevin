
//---Crea la paleta de colores con jquery-minicolors-rails
$('.demo').each(function () {
    $(this).minicolors({
        control: $(this).attr('data-control') || 'hue',
        defaultValue: $(this).attr('data-defaultValue') || '',
        format: $(this).attr('data-format') || 'hex',
        keywords: $(this).attr('data-keywords') || '',
        inline: $(this).attr('data-inline') === 'true',
        letterCase: $(this).attr('data-letterCase') || 'lowercase',
        opacity: $(this).attr('data-opacity'),
        position: $(this).attr('data-position') || 'bottom left',
        swatches: $(this).attr('data-swatches') ? $(this).attr('data-swatches').split('|') : [],
        change: function (value, opacity) {
            if (!value) return;
            if (opacity) value += ', ' + opacity;
            if (typeof console === 'object') {
                //console.log(value);
            }
        },
        theme: 'bootstrap'
    });
});

$(document).ready(function(){
    //Tipo de filtro
   let tipoFiltro = $('#tipoFiltro').val();
   $('#typeFilter').val(`${tipoFiltro}`);
});

// $(document).ready(function(){
//
//     //-- Id de la sesión a la cual se le agregara el nuevo filtro
//     idSession = $('#idSession').val();
//     console.log(idSession);
//
//     for (let i = 0; i < gon.sesiones.length; i++){
//         let sesionId = gon.sesiones[i].Id
//         let layId = gon.sesiones[i].Lay_id
//         let formatId = gon.sesiones[i].Format_id
//
//         if ( idSession == sesionId ){
//             // alert('Sessiones iguales');
//
//
//             for ( let c = 0; c < gon.tCamposFormatos.length; c++ ){
//
//                 if ( (gon.tCamposFormatos[c].IdLay == layId ) && ( gon.tCamposFormatos[c].IdFormato == formatId) ){
//
//                     for( let d = 0; d < gon.tCamposLays.length; d++ ){
//
//                         if ( ( gon.tCamposLays[d].IdLay == gon.tCamposFormatos[c].IdLay ) && ( gon.tCamposLays[d].IdCampo == gon.tCamposFormatos[c].IdCampo )  ){
//
//                             if ( gon.tCamposLays[d].Alias != ""){
//
//                                 $("#selectCampo").append(`<option value="${gon.tCamposLays[d].IdCampo}" name="select_feild">${gon.tCamposLays[d].Alias}</option>`);
//
//                             } else{
//                                 $("#selectCampo").append(`<option value="${gon.tCamposLays[d].IdCampo}" name="select_feild">${gon.tCamposLays[d].Nombre}</option>`);
//                             }
//
//                         }
//                     }
//                 }
//             }
//         }
//     }
//
//     //Agrego el valor a select Campos
//     let idCampo = $('#campoId').val();
//     $("#selectCampo").val(`${idCampo}`);
//
//     //Agregar valor a select Operador
//     let operVal = $('#operatorVal').val();
//     $('#selectOperator').val(`${operVal}`);
// });
//
// // function clickDenied(){
// //     document.getElementById('tx_filter_detail_denied').click()
// // }
//
// // Cambiar valor de Negado
// $('#negado').on('click', function(){
//     let $_this = $(this)
//     console.log(this.value);
//
//     if( $_this.val() == "0" ){
//         $_this.val('1');
//     } else {
//         $_this.val('0');
//     };
// });
//
// //Cambiar valores para conector
// let or = $('#or').val(),
//     and = $('#and').val();
// $('.conectorEdit').on('click', function(){
//    let $_this = $(this),
//        cheConec = $('#connector');
//
//    if ( cheConec.val() == '0' ){
//        cheConec.val('1');
//        $_this.html(`${and}`);
//    } else {
//         cheConec.val('0');
//        $_this.html(`${or}`);
//    }
//
// });
//
// //Cabiar Campo
// $('#selectCampo').on('change', function(){
//     let idCampo = this.value;
//
//     for (let i = 0; i < gon.tCamposLays.length; i++){
//         let campo = gon.tCamposLays[i].IdCampo
//
//         if ( idCampo == campo ){
//             let pos =  gon.tCamposLays[i].Posicion,
//                 long =  gon.tCamposLays[i].Longitud;
//
//             $('#posEdit').val(`${pos}`);
//             $('#lengEdit').val(`${long}`);
//         }
//     }
//
// });
//
// $('#selectOperator').on('change', function () {
//     let val = this.value
//
//     if ( val == "8" ){
//         $('.valDos').css('display','block');
//     } else {
//         $('.valDos').css('display','none');
//         $('#valorDos').val('');
//     }
// });
