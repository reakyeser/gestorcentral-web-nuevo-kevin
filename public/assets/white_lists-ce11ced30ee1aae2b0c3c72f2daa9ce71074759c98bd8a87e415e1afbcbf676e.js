let you_sure =  document.getElementById('you_sure').value,
    add_swal_text = document.getElementById('add_swal_text').value,
    confirm_add_swall_text = document.getElementById('confirm_add_swall_text').value,
    delete_swal_text = document.getElementById('delete_swal_text').value,
    confirmar = document.getElementById('confirmar').value,
    confirm_delete_swal_text = document.getElementById('confirm_delete_swal_text').value,
    action_cancel = document.getElementById('action_cancel').value,
    btn_continue = document.getElementById('btn_continue').value,
    btn_cancelar = document.getElementById('cancelar').value,
    add_success = document.getElementById('add_success').value,
    delete_success = document.getElementById('delete_success').value;

//Agregar o eliminar de lista blanca
$(document).on('click', '.checkState', function(e){
    let checked = this.checked,
        id_fil = this.value,
        inputCheck = $(this),
        id = this.id;

    //----Agregar a lista negra
    if ( checked ){
        swal({
                title: `${you_sure}`,
                text: `${add_swal_text}`,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: `${btn_continue}`,
                cancelButtonText: `${btn_cancelar}`,
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function (isConfirm) {

                if (isConfirm){

                    swal({
                            title: `${confirmar}`,
                            text: `${confirm_add_swall_text}`,
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: `${btn_continue}`,
                            cancelButtonText: `${btn_cancelar}`,
                            closeOnConfirm: false,
                            closeOnCancel: false
                        },
                        function (isConfirm) {

                            if (isConfirm){

                                //---Agregando lista balnca
                                $.ajax({
                                    type: "PUT",
                                    beforeSend: function (xhr) {
                                        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
                                    },
                                    url: `../white_lists/${id_fil}.json`,
                                    contentType: "application/json",
                                    dataType: "json",
                                    data: JSON.stringify({
                                        Estado:1
                                    }),
                                    success: function (data) {
                                        // console.log(data)

                                        document.getElementById(`name${id_fil}`).innerHTML = `${data["nombre"]}`;
                                        document.getElementById(`fecha${id_fil}`).innerHTML = `${data["fecha"]}`;
                                        //
                                        swal({
                                            position: 'top-end',
                                            type: 'success',
                                            title: 'Ok',
                                            text: `${add_success}`,
                                            showConfirmButton: true,
                                            timer: 3000
                                        });

                                        // if( valorActive == 1 ){
                                        //     swal(`${session_active_title}`);
                                        // } else {
                                        //     swal(`${session_inactive}`);
                                        // }
                                    }
                                });

                            }
                            else{

                                //Esto es para que no se chechee el input
                                inputCheck.prop('checked', false);

                                swal({
                                    position: 'top-end',
                                    type: 'error',
                                    title: 'Ok',
                                    text: `${action_cancel}`,
                                    showConfirmButton: true,
                                    timer: 3000
                                });
                            }
                        });

                } else {

                    //Esto es para que no se chechee el input
                    inputCheck.prop('checked', false);

                    swal({
                        position: 'top-end',
                        type: 'error',
                        title: 'Ok',
                        text: `${action_cancel}`,
                        showConfirmButton: true,
                        timer: 3000
                    });
                }
            });

    } else {
        // alert('no Checkeado')
        swal({
                title: `${you_sure}`,
                text: `${delete_swal_text}`,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: `${btn_continue}`,
                cancelButtonText: `${btn_cancelar}`,
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function (isConfirm) {

                if (isConfirm){

                    swal({
                            title: `${confirmar}`,
                            text: `${confirm_delete_swal_text}`,
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: `${btn_continue}`,
                            cancelButtonText: `${btn_cancelar}`,
                            closeOnConfirm: false,
                            closeOnCancel: false
                        },
                        function (isConfirm) {

                            if (isConfirm){

                                //---Eliminar un filtro de lista blanca
                                $.ajax({
                                    type: "PUT",
                                    beforeSend: function (xhr) {
                                        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
                                    },
                                    url: `../white_lists/${id_fil}.json`,
                                    contentType: "application/json",
                                    dataType: "json",
                                    data: JSON.stringify({
                                        Estado:0
                                    }),
                                    success: function (data) {
                                        console.log(data)

                                        document.getElementById(`name${id_fil}`).innerHTML = `${data["nombre"]}`;
                                        document.getElementById(`fecha${id_fil}`).innerHTML = `${data["fecha"]}`;
                                        //
                                        swal({
                                            position: 'top-end',
                                            type: 'success',
                                            title: 'Ok',
                                            text: `${delete_success}`,
                                            showConfirmButton: true,
                                            timer: 3000
                                        });
                                    }
                                });

                            }
                            else{

                                //Esto es para que no se deschechee el input
                                inputCheck.prop('checked', true);

                                swal({
                                    position: 'top-end',
                                    type: 'error',
                                    title: 'Ok',
                                    text: `${action_cancel}`,
                                    showConfirmButton: true,
                                    timer: 3000
                                });
                            }
                        });

                } else {
                    //Esto es para que no se deschechee el input
                    inputCheck.prop('checked', true);

                    swal({
                        position: 'top-end',
                        type: 'error',
                        title: 'Ok',
                        text: `${action_cancel}`,
                        showConfirmButton: true,
                        timer: 3000
                    });
                }
            });
    }

});
