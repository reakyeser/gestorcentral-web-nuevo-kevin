console.log('Entre TxFilters');

//---Estilos para el wizard
$('.content').css('min-height','auto');
$('.content').css('height','auto');


//----Gon Para llenar Select Formatos
$('.selectLay').on('change', function(){
    $(".selectFormat option").remove();
    $(".selectFormat").attr('disabled', false);
    let traduSelectFormat = $('#traduSeleFormat').val();
    id_lay = $(this).val();


    $(".selectFormat").append($("<option value></option>").text(`${traduSelectFormat}`));
    for( f = 0; f < gon.formatos.length; f++ ){
        if( gon.formatos[f].IdLay == id_lay ){

            $(".selectFormat").append(`<option value="${gon.formatos[f].IdFormato}" name="optionFormat">${gon.formatos[f].Descripcion}</option>`)
        } else {
            $(".selectFormat option").remove();
            $(".selectFormat").attr('disabled', true);
            $(".selectFormat").append($("<option value></option>").text(`${traduSelectFormat}`));
        }
    }
});

//----Gon Para Llenar SelectFeild y Select Group
$('.selectFormat').on('change', function() {


    let traSelFeild = $("#traduSeleFeild").val();
    $(".selectFeild option").remove();
    $("#group_sel option").remove();
    id_format = $(this).val();
    // console.log(`valor de Layout: ${id_lay} y de Formato: ${id_format}`)

    $(".selectFeild").append($("<option value></option>").text(`${traSelFeild}`));
    for ( c = 0; c < gon.tCamposFormatos.length; c++ ){

        if( ( gon.tCamposFormatos[c].IdLay == id_lay ) && ( gon.tCamposFormatos[c].IdFormato == id_format ) ){

            for( d = 0; d < gon.tCamposLays.length; d++ ){

                if ( ( gon.tCamposLays[d].IdLay == gon.tCamposFormatos[c].IdLay ) && ( gon.tCamposLays[d].IdCampo == gon.tCamposFormatos[c].IdCampo )  ){

                    if ( gon.tCamposLays[d].Alias != ""){

                        $(".selectFeild").append(`<option value="${gon.tCamposLays[d].IdCampo}" name="select_feild">${gon.tCamposLays[d].Alias}</option>`);
                        $("#group_sel").append(`<option value="${gon.tCamposLays[d].IdCampo}" name="select_feild">${gon.tCamposLays[d].Alias}</option>`);

                    } else{
                        $(".selectFeild").append(`<option value="${gon.tCamposLays[d].IdCampo}" name="select_feild">${gon.tCamposLays[d].Nombre}</option>`);
                        $("#group_sel").append(`<option value="${gon.tCamposLays[d].IdCampo}" name="select_feild">${gon.tCamposLays[d].Nombre}</option>`)
                    }

                }
            }

        }
    }

});

//----Ayuda a mostrar el select-chose en el wizard
setTimeout(function(){

    $('#group_sel_chosen').css('width','100%');
    var hijo = $('.search-field').children();

    hijo.css('width', '100%')

}, 2000);

//--Muestra input Main - General - Group
$('.filterSession').on('change', function(){
    var typeFilter = $(this).val();
    // alert("Entre changue");
    // console.log(option)

    if ( typeFilter === "1" ){

        $('.colorPicker').css('display', 'block');
        $('.choseSelect').css('display', 'none');
        $('.content').css('min-height','420px');
        $('.chosen-choices').html();

        //Limpar valores de los demas inputs

        $('.chosen-select').val([]).trigger('chosen:updated');

    } else if( typeFilter === "2" ) {

        $('.colorPicker').css('display', 'none');
        $('.choseSelect').css('display', 'block');
        $('.content').css('min-height','520px');

        //Limpar valores de los demas inputs
        $('.demo').val("");

    } else {

        $('.choseSelect').css('display', 'none');
        $('.colorPicker').css('display', 'none');
        $('.content').css('min-height','auto');

        //Limpar valores de los demas inputs
        $('.demo').val("");
        $('.chosen-select').val([]).trigger('chosen:updated');

    }
});

//----inicilizo chose-select
$(".chosen-select").chosen();


//---Crea la paleta de colores con jquery-minicolors-rails
$('.demo').each(function () {
    $(this).minicolors({
        control: $(this).attr('data-control') || 'hue',
        defaultValue: $(this).attr('data-defaultValue') || '',
        format: $(this).attr('data-format') || 'hex',
        keywords: $(this).attr('data-keywords') || '',
        inline: $(this).attr('data-inline') === 'true',
        letterCase: $(this).attr('data-letterCase') || 'lowercase',
        opacity: $(this).attr('data-opacity'),
        position: $(this).attr('data-position') || 'bottom left',
        swatches: $(this).attr('data-swatches') ? $(this).attr('data-swatches').split('|') : [],
        change: function (value, opacity) {
            if (!value) return;
            if (opacity) value += ', ' + opacity;
            if (typeof console === 'object') {
                //console.log(value);
            }
        },
        theme: 'bootstrap'
    });
});

//---Validaciones para mostar (VALUE) || (BETWEEN / NOT BETWEEN) || (IN / NOT)
$('.operator_sel').change(function(){

    $(".betweenValue label").remove();
    $(".inNot label").remove();
    let operadorText = $('.operator_sel option:selected').text();
    let operadorVal = $('.operator_sel').val();

    //---Validaciones para mostar (VALUE) || (BETWEEN / NOT BETWEEN) || (IN / NOT)
    if ( operadorVal === "8" ){
        $('.betweenValue').prepend(`<label style="display: block">${operadorText}</label>`);

        $('.oneValue, .inValue, .contDenied').css('display','none');
        $('.betweenValue').css('display','block');
        $('#valor, #selectList').val("");

        //---Reset CheckBox Denied
        $('#denied').val('0');
        $('.icheckbox_square-green').removeClass('checked');

    } else if( operadorVal === "7" ){
        $('.inNot').prepend(`<label style="display: block">${operadorText}</label>`);

        $('.oneValue, .betweenValue, .contDenied').css('display','none');
        $('.inValue').css('display','flex');
        $('#valor, .valOneBetween, .valTwoBetween').val("");

        //---Reset CheckBox Denied
        $('#denied').val('0');
        $('.icheckbox_square-green').removeClass('checked');

    } else {

        $('.contDenied, .oneValue').css('display', 'block');
        $('.betweenValue, .inValue').css('display','none');
        $('.valOneBetween, .valTwoBetween, #selectList').val("");

    }

});


//---Script para el boton de carga archivos falso
var btnReal = document.getElementById('realBtnFile');

$('.falseBtnFile').on('click', function(){
    btnReal.click();
});

$('.realBtnFile').change(function(){
    var valFile = $(this).val()

    if ( valFile ){
        let txtFile = valFile.match( /[\/\\]([\w\d\s\.\-\(\)]+)$/)[1];
        $('.customText').html(txtFile);
        $('.customText').attr('value', txtFile);
        $('#selectList').fadeOut(100);
    } else {
        $('.customText').html(`${placeAddList}`);
        $('.customText').attr('value', '');
        $('#selectList').fadeIn(100);
    }
});

// Bloqueo select In
$('#selectList').on('change', function(){
    // alert('Entre list')
    let $_this = $(this).val();

    if( $_this !== "" ){
        $('.upFiles').fadeOut(100);
    } else {
        $('.upFiles').fadeIn(100);
    }

});


//---Radio Button i-checks
$('.i-checks').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green',
});

//---Cambia el valor del input Denied
$('.icheckbox_square-green').on('ifClicked', function(){
    let _this = $('.icheckbox_square-green').hasClass('checked');

    if ( !_this ){

        $('#denied').val('1');

    } else {

        $('#denied').val('0');

    }
});
