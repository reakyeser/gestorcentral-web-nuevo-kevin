/**
 * Created by aflores8 on 3/01/17.
 */


$(document).ready(function () {
    cierreSession();
});

function parar() {
    clearTimeout(myvar);
}

function cierreSession() {
    console.log("Soy cierre_session.js");
    myvar = setTimeout(function () {
        var expired = false;
        var contador = 60 * 1000
        var id = document.getElementById('currentUserId').value;
        var title_txt = document.getElementById('title_txt').value;
        var text_txt = document.getElementById('text_txt').value;
        var btn_conf_txt = document.getElementById('btn_conf_txt').value;

        var expire_txt = document.getElementById('expire_txt').value;
        var exp_title_txt = document.getElementById('exp_title_txt').value;
        var exp_mess_txt = document.getElementById('exp_mess_txt').value;

        setTimeout(function () {
            expired = true;
        }, contador);

        swal({
                title: title_txt,
                text: text_txt,
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: btn_conf_txt,
                cancelButtonText: "",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    if (expired) {
                        swal(exp_title_txt, exp_mess_txt , "warning");
                        $.ajax({
                                type: "GET",
                                beforeSend: function (xhr) {
                                    xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
                                },
                                url: "/users/expire?id=" + document.getElementById('currentUserId').value,
                                success: function (result) {
                                    location.reload(true);
                                },
                                error: function (result) {
                                    location.reload(true);
                                }
                            }
                        );
                    } else {
                        //swal("Ok!", "Your session is active.", "success");
                        swal("Ok!", expire_txt , "success");
                        cierreSession();
                    }
                }
            });
    }, 10 * 60 * 1000)
    //}, 250 * 240)
    //}, 3000)
}
;
