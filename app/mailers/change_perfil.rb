class ChangePerfil < ApplicationMailer


  #default from: "aflores@kssoluciones.com.mx"
  default from: "krodriguez@kssoluciones.com.mx"
  #default from: "Listas_Blancas@homedepot.com.mx"

  # def mandar_permisos(user,admin)
  #
  #   #@user_name = user.username
  #   @user_email
  # = user.email
  #   @name_user = user.name
  #   @last_user = user.last_name
  #   @admin_usr = admin.to_s
  #   @id_usr = user.id
  #
  #   mail to: @admin_usr, subject: "Grant Permits!"
  #
  # end

  def mandar_permisos_signup(user,admin)

    #@user_name = user.username
    @user_email = user.email
    @name_user = user.name
    @last_user = user.last_name
    @admin_usr = admin.to_s
    @id_usr = user.id

    #@id_usuario = current_user.id
    @id_usuario = user.id
    @id_aplicacion = 25
    @idTipoReg = 15 #Mailer
    @fecha = Time.now.strftime("%F")
    @hora = Time.now.strftime("%k:%M:%S.00000")
    @men = "Permisos Mail"


    @tbit = Tbitacora.new
    @tbit.IdUsuario = @id_usuario
    @tbit.IdAplicacion = @id_aplicacion
    @tbit.IdTipoReg = @idTipoReg #Mailer
    @tbit.Fecha = @fecha
    @tbit.Hora  = @hora
    @tbit.Mensaje = @men
    @tbit.save

    mail to: @admin_usr, subject: default_i18n_subject(user: user.name)

  end

  def permisos_otorgados(user)

    @user_id = user.id
    @user_email = user.email
    @user_name = user.name
    @last_name = user.last_name
    @login_name = user.user_name
    @role = user.profile.name

    @sh_profile = Permission.where("profile_id = ?", user.profile_id )

    mail to: @user_email, subject: default_i18n_subject(user: user.name)

  end


end
