class CategoriFilter < ApplicationRecord
  belongs_to :filtering_categorization, foreign_key: :id_categori
  belongs_to :tx_filters, foreign_key: :id_filter
end