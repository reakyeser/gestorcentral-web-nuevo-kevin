class Profile < ApplicationRecord
  has_many :permissions, dependent: :destroy, foreign_key: :profile_id
  has_many :users

  belongs_to :area, foreign_key: :area_id
end
