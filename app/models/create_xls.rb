class CreateXls
  def setInfoAlertXls(id, user, idioma, observaciones, historial, transacciones)
    @id = id
    @user_id = user
    @idioma = idioma
    @observaciones = observaciones
    @historial = historial
    @transacciones = transacciones

    @alerta = Tbitacora.find(@id)
  end

  def setInfoAlertLevelXls(alertas, filtro, user, idioma)
    @alertas = alertas
    @filtro = filtro
    @user_id = user
    @idioma = idioma

  end

  def setInfoGenXls(registros, user, idioma)
    @registros = registros
    @user_id = user
    @idioma = idioma
  end

  def alertXls
    begin
      puts '>------------------------------------------------------------------------<'
      puts green('Iniciando Hilo: Creación de XLS-Alerta' + @id)
      @fecha_comienzo = Time.now
      ## -------------------- COMIENZA LA CREACIÓN DEL EXCEL ------------------------##

      ##------------------------------ ANCHO DE LAS FILAS ------------------------------- ##
      col_widths = [10, 10, 12, 16, 10, 18, 12, 10.2, 14.2, 11, 12, 24, 10, 12, 10, 10, 10]
      col_widths_tran = [10, 10, 12, 16, 10, 18, 12, 10.2, 14.2, 11, 12, 24, 10, 12, 10, 10, 10]
      col_widths_per = [10, 10, 13, 12, 13, 27.3, 16, 16.8, 11, 17, 13, 14, 10, 20, 18, 20, 10]
      col_widths_h3 = [10, 11, 15, 11, 16, 13, 21, 12, 14.2, 15, 12, 12, 24, 10, 11, 10, 10, 10]
      p = Axlsx::Package.new
      wb = p.workbook

      sleep(2)

      wb.styles do |style|
        highlight_cell = style.add_style(bg_color: "396F9B", alignment: {horizontal: :center}, :sz => 12, border: Axlsx::STYLE_THIN_BORDER, :b => true)
        section__cell = style.add_style(bg_color: "003057", alignment: {horizontal: :center}, :sz => 12, border: Axlsx::STYLE_THIN_BORDER, :b => true, :fg_color => 'FFFFFFFF')
        #Titulo
        main_title_cell = style.add_style(bg_color: "396F9B", alignment: {horizontal: :center}, :sz => 12, border: Axlsx::STYLE_THIN_BORDER, :b => true)
        content_cell = style.add_style(alignment: {horizontal: :center}, :sz => 10, border: {:edges => [:left, :right], :style => :thin, :color => 'FF000000'})
        date_cell = style.add_style(:num_fmt => Axlsx::NUM_FMT_YYYYMMDDHHMMSS, alignment: {horizontal: :center}, :sz => 10, border: {:edges => [:left, :right], :style => :thin, :color => 'FF000000'})
        border_header_dates_bold = style.add_style(:sz => 12, :b => true, border: {:edges => [:top, :left], :style => :thick, :color => 'FF000000'})
        border_header_dates_notbold = style.add_style(:sz => 12, :b => false, border: {:edges => [:top], :style => :thick, :color => 'FF000000'})
        header_dates_bold = style.add_style(:sz => 12, :b => true, border: {:edges => [:left], :style => :thick, :color => 'FF000000'})
        header_dates_notbold = style.add_style(alignment: {horizontal: :left}, :sz => 12, :b => false, :num_fmt => Axlsx::NUM_FMT_YYYYMMDDHHMMSS)
        bottom = style.add_style(border: {:edges => [:top], :style => :thin, :color => 'FF000000'})
        bottom_thick = style.add_style(border: {:edges => [:top], :style => :thick, :color => 'FF000000'})
        left_thick = style.add_style(border: {:edges => [:left], :style => :thick, :color => 'FF000000'})
        center = style.add_style(alignment: {horizontal: :center, vertical: :center}, border: {:style => :thick, :color => 'FF000000'}, :sz => 16, :b => true)
        top_thick = style.add_style(border: {:edges => [:top], :style => :thick, :color => 'FF000000'}, :sz => 16, :b => true)
        border_thick = style.add_style(border: {:edges => [:top, :left, :right, :bottom], :style => :thick, :color => 'FF000000'}, :sz => 16, :b => true)

        ##**************************----------------------- HOJA DETALLE DE ALERTA -------------------------**************************##
        wb.add_worksheet(name: "Alert Detail") do |sheet|

          #FILAS COMBINADAS
          sheet.merge_cells('D2:P5') #Título del reporte
          sheet.merge_cells('B2:C6') #logo
          sheet.merge_cells('D6:E6') #fecha creación título
          sheet.merge_cells('F6:P6') #fecha creación cont

          #------- Observaciones
          sheet.merge_cells('B15:P15') #observaciones header
          sheet.merge_cells('B17:D17') #fecha/hora header
          sheet.merge_cells('E17:I17') #observacion header
          sheet.merge_cells('J17:L17') #estado header
          sheet.merge_cells('M17:P17') #usuario header


          img = File.expand_path(Rails.root + 'app/assets/images/logoKS.png', __FILE__)
          sheet.add_image(:image_src => img, :noSelect => true, :noMove => true) do |image|
            image.width = 140
            image.height = 101
            #image.start_at 2, 2
            image.anchor.from.rowOff = 2050000
            image.anchor.from.colOff = 1550000
          end

          ##---------------------------------- HEADER, LOGO ------------------------------ ##
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]
          sheet.add_row [" ", " ", " ", "Numero_de_Tarjeta: " + @alerta.IdTran, " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, nil, center, center, center, center, center, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, left_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, nil, center, center, center, center, center, center, center, center, center, center, center, center, center, left_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, nil, center, center, center, center, center, center, center, center, center, center, center, center, center, left_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, nil, center, center, center, center, center, center, center, center, center, center, center, center, center, left_thick]

          if @idioma == 'es'
            sheet.add_row [" ", " ", " ", "Fecha de exportación: ", " ", Time.now.to_date.strftime("%d/%m/%Y"), " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, nil, header_dates_bold, header_dates_bold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, left_thick]
          elsif @idioma == 'en'
            sheet.add_row [" ", " ", " ", "Export Date: ", " ", Time.now.to_date.strftime("%d/%m/%Y"), " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, nil, header_dates_bold, header_dates_bold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, left_thick]
          end

          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, nil, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]


          ##----------------------- DETALLE -------------------------##


          if @alerta.IdProducto == 1
            #------- Detalle
            sheet.merge_cells('B9:P9') #detalle header
            sheet.merge_cells('B11:D11') #ticket header
            sheet.merge_cells('B12:D12') #ticket content
            sheet.merge_cells('E11:H11') #fecha/hora header
            sheet.merge_cells('E12:H12') #fecha/hora content
            sheet.merge_cells('I11:L11') #origen header
            sheet.merge_cells('I12:L12') #origen content
            sheet.merge_cells('M11:P11') #filtro header
            sheet.merge_cells('M12:P12') #filtro content

            @origen = "Transaccional"

            if @idioma == 'es'
              sheet.add_row [" ", "Detalle de Alerta", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell]
              sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]
              sheet.add_row [" ", "Ticket ", " ", " ", "Fecha/Hora ", " ", " ", " ", "Origen ", " ", " ", " ", "Filtro ", " ", " ", " ", " "], style: [nil, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell]
            elsif @idioma == 'en'
              sheet.add_row [" ", "Alert Detail", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell]
              sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]
              sheet.add_row [" ", "Ticket ", " ", " ", "Date/Hour ", " ", " ", " ", "Origin ", " ", " ", " ", "Filter ", " ", " ", " ", " "], style: [nil, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell]
            end

            if @alerta.tx_filters.present?
              @filtro = @alerta.tx_filters.Id.to_s + " - " + @alerta.tx_filters.Description.to_s
            else
              @filtro = "N/A"
            end

            sheet.add_row [" ", @alerta.Id, " ", " ", @alerta.Fecha + " " + @alerta.Hora, " ", " ", " ", @origen, " ", " ", " ", @filtro, " ", " ", " ", " "], style: [nil, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell]
            sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom]
            sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]

          else
            #------- Detalle
            sheet.merge_cells('B9:P9') #detalle header
            sheet.merge_cells('B11:D11') #ticket header
            sheet.merge_cells('B12:D12') #ticket content
            sheet.merge_cells('E11:G11') #fecha/hora header
            sheet.merge_cells('E12:G12') #fecha/hora content
            sheet.merge_cells('H11:J11') #origen header
            sheet.merge_cells('H12:J12') #origen content
            sheet.merge_cells('K11:M11') #grupo header
            sheet.merge_cells('K12:M12') #grupo content
            sheet.merge_cells('N11:P11') #perfil header
            sheet.merge_cells('N12:P12') #perfil content

            @origen = "Perfiles"

            if @idioma == 'es'
              sheet.add_row [" ", "Detalle de Alerta", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell]
              sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]
              sheet.add_row [" ", "Ticket ", " ", " ", "Fecha/Hora ", " ", " ", "Origen", " ", " ", "Grupo", " ", " ", "Perfil", " ", " ", " "], style: [nil, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell]
            elsif @idioma == 'en'
              sheet.add_row [" ", "Alert Detail", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell]
              sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]
              sheet.add_row [" ", "Ticket ", " ", " ", "Date/Hour ", " ", " ", "Origin", " ", " ", "Group", " ", " ", "Profile", " ", " ", " "], style: [nil, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell]
            end

            if @alerta.tgrupos.present?
              @grupo = @alerta.IdGrupo.to_s + " - " + @alerta.tgrupos.Nombre.to_s
            else
              @grupo = "N/A"
            end

            if @alerta.tperfiles.present?
              @perfil = @alerta.IdPerfil.to_s + " - " + @alerta.tperfiles.Nombre.to_s
            else
              @perfil = "N/A"
            end

            sheet.add_row [" ", @alerta.Id, " ", " ", @alerta.Fecha + " " + @alerta.Hora, " ", " ", @origen, " ", " ", @grupo, " ", " ", @perfil, " ", " ", " "], style: [nil, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell]
            sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom]
            sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]

          end


          ##----------------------- Observaciones -------------------------##

          if @idioma == 'es'
            sheet.add_row [" ", "Observaciones", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell]
            sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]
            sheet.add_row [" ", "Fecha/Hora", " ", " ", "Observaciones", " ", " ", " ", " ", "Estado", " ", " ", "Usuario de Atención", " ", " ", " "], style: [nil, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell]
          elsif @idioma == 'en'
            sheet.add_row [" ", "Observations", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell]
            sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]
            sheet.add_row [" ", "Date/Hour", " ", " ", "Observations", " ", " ", " ", " ", "State", " ", " ", "User of Attention", " ", " ", " "], style: [nil, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell]
          end


          # @observaciones = Tbitaobse.where(:IdAlerta => @alerta.Id).order("Id Desc")

          @renglon = 17
          if @observaciones.present? && !@observaciones.nil?

            @observaciones.each do |ob|
              @renglon += 1
              @comb_fecha = 'B' + @renglon.to_s + ':D' + @renglon.to_s
              @comb_observ = 'E' + @renglon.to_s + ':I' + @renglon.to_s
              @comb_estado = 'J' + @renglon.to_s + ':L' + @renglon.to_s
              @comb_usuario = 'M' + @renglon.to_s + ':P' + @renglon.to_s
              sheet.merge_cells(@comb_fecha) #fecha content
              sheet.merge_cells(@comb_observ) #observaciones content
              sheet.merge_cells(@comb_estado) #estado content
              sheet.merge_cells(@comb_usuario) #usuario content

              if ob.user.present?
                @usuario = ob.IdUsuarioAtencion.to_s + " - " + ob.user.name + " " + ob.user.last_name
              else
                @usuario = "N/A"
              end
              sheet.add_row [" ", ob.Fecha + " " + ob.Hora, " ", " ", ob.Observaciones, " ", " ", " ", " ", ob.IdEstado.to_s + " - " + ob.testadosxalerta.Descripcion, " ", " ", @usuario, " ", " ", " "], style: [nil, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell], :types => [:string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string]
            end
          else
            sheet.merge_cells('B22:P22') #observaciones nulas

            if @idioma == 'es'
              sheet.add_row [" ", "No hay observaciones para esta alerta", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell], :types => [:string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string]
            elsif @idioma == 'en'
              sheet.add_row [" ", "There are no observations for this alert", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell], :types => [:string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string]
            end

          end

          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom], :types => [:string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string]

          ##----------------------- CERRANDO EXCEL -------------------------##

          if @alerta.IdProducto == 1
            sheet.column_widths *col_widths_tran
          else
            sheet.column_widths *col_widths_per
          end

          puts('Terminando hoja 1...')
          sleep(2)
        end

        wb.add_worksheet(name: "Alert Reference") do |sheet|
          @campos_referencia = Referenciadealerta.select(:idCampo, :campo).where(:idAlerta => @alerta.Id).group(:idCampo, :campo)
          @cant_campos = @campos_referencia.length.to_i
          puts cyan("Campos de la referencia: " + @cant_campos.to_s)
          @columna_fin = @cant_campos + 2 # Mas margen y la columna ticket
          @columna_fin = @columna_fin.to_s26.to_s.upcase

          puts @columna_fin

          #FILAS COMBINADAS

          @titulo = 'C2:' + @columna_fin + '5'
          @fecha_creacion_cont = 'E6:' + @columna_fin + '6'
          sheet.merge_cells(@titulo) #Título del reporte
          sheet.merge_cells('B2:B6') #logo
          sheet.merge_cells('C6:D6') #fecha creación título
          sheet.merge_cells(@fecha_creacion_cont) #fecha creación cont

          img = File.expand_path(Rails.root + 'app/assets/images/logoKS.png', __FILE__)
          sheet.add_image(:image_src => img, :noSelect => true, :noMove => true) do |image|
            image.width = 140
            image.height = 101
            #image.start_at 2, 2
            image.anchor.from.rowOff = 2050000
            image.anchor.from.colOff = 1550000
          end

          ##---------------------------------- HEADER, LOGO ------------------------------ ##
          arr_row_header = Array.new
          arr_row_header.push(" ")

          arr_empty = Array.new
          arr_empty.push (" ")
          arr_empty.push (" ")

          col_widths_ref = Array.new
          col_widths_ref.push(10)
          col_widths_ref.push(17.5)

          for i in 0..@cant_campos
            arr_row_header.push(" ")
            arr_empty.push(" ")
            col_widths_ref.push(25)
          end


          sheet.add_row arr_row_header
          if @idioma == 'es'
            arr_titulo = arr_row_header
            arr_titulo.insert(2, "Numero_de_Tarjeta: " + @alerta.IdTran)
            sheet.add_row arr_titulo, style: [nil, nil, center]
            sheet.add_row arr_row_header, style: [nil, nil, center]
            sheet.add_row arr_row_header, style: [nil, nil, center]
            sheet.add_row arr_row_header, style: [nil, nil, center]
            @estilos = 'D2:' + @columna_fin + '5'
            sheet[@estilos].each {|c| c.style = border_thick}

            #Fecha
            arr_fecha = arr_row_header
            arr_fecha.insert(2, "Fecha de exportación: ")
            arr_fecha.insert(4, Time.now.to_date.strftime("%d/%m/%Y"))
            sheet.add_row arr_titulo, style: [nil, nil, header_dates_bold, header_dates_bold, header_dates_notbold]
            @estilos_fecha = 'F6:' + @columna_fin + '6'
            sheet[@estilos_fecha].each {|c| c.style = border_thick}

          elsif @idioma == 'en'
            arr_titulo = arr_row_header
            arr_titulo.insert(2, "Numero_de_Tarjeta: " + @alerta.IdTran)
            sheet.add_row arr_titulo, style: [nil, nil, center]
            sheet.add_row arr_row_header, style: [nil, nil, center]
            sheet.add_row arr_row_header, style: [nil, nil, center]
            sheet.add_row arr_row_header, style: [nil, nil, center]
            @estilos = 'D2:' + @columna_fin + '5'
            sheet[@estilos].each {|c| c.style = border_thick}

            #Fecha
            arr_fecha = arr_row_header
            arr_fecha.insert(2, "Export Date: ")
            arr_fecha.insert(4, Time.now.to_date.strftime("%d/%m/%Y"))
            sheet.add_row arr_titulo, style: [nil, nil, header_dates_bold, header_dates_bold, header_dates_notbold]
            @estilos_fecha = 'F6:' + @columna_fin + '6'
            sheet[@estilos_fecha].each {|c| c.style = border_thick}
          end

          sheet.add_row [" ", " ", " ", " ", " "], style: [nil, nil, bottom_thick, bottom_thick, bottom_thick]
          sheet.add_row [" "]

          ##----------------------- COLUMNAS DE LA REFERENCIA -------------------------#

          arr_titulos_ref = Array.new
          arr_titulos_ref.push(" ")
          arr_titulos_ref.push("Ticket")

          arr_sect = Array.new
          arr_sect.push(" ")
          if @idioma == 'es'
            arr_sect.push("Referencia")
          elsif @idioma == 'en'
            arr_sect.push(" Reference ")
          end

          @campos_referencia.each do |tit_ref|
            arr_titulos_ref.push(tit_ref.campo.strip)
            arr_sect.push(" ")
          end

          sheet.add_row arr_sect, style: [nil]
          @estilos_columnas_sect = 'B9:' + @columna_fin + '9'
          sheet.merge_cells(@estilos_columnas_sect) #Título de sección
          sheet[@estilos_columnas_sect].each {|c| c.style = section__cell}
          sheet.add_row [" "]


          sheet.add_row arr_titulos_ref, style: [nil]
          @estilos_columnas_header = 'B11:' + @columna_fin + '11'
          sheet[@estilos_columnas_header].each {|c| c.style = highlight_cell}

          @row = 12
          arr_valor = Array.new
          arr_valor.push(" ")
          arr_valor.push(@alerta.Id.to_s)
          @campos_referencia.each do |tit_ref|
            @valor = Referenciadealerta.select(:valor).find_by(:idCampo => tit_ref.idCampo, :idAlerta => @alerta.Id)

            if @valor.present? && !@valor.nil? && @valor != "" && @valor != " "
              arr_valor.push(@valor.valor)
            else
              arr_valor.push("--")
            end
          end

          sheet.add_row arr_valor, style: [nil]
          @estilos_valor = 'B' + @row.to_s + ':' + @columna_fin + @row.to_s
          sheet[@estilos_valor].each {|c| c.style = content_cell}
          sheet[@estilos_valor].each {|c| c.type = :string}

          sheet.add_row arr_empty, style: [nil, bottom, bottom, bottom, bottom, bottom, bottom, bottom]
          @row = @row += 1
          @estilos_final = 'B' + @row.to_s + ':' + @columna_fin + @row.to_s
          sheet[@estilos_final].each {|c| c.style = bottom}
          ##----------------------- CERRANDO HOJA 2 -------------------------##
          sheet.column_widths *col_widths_ref

          puts('Terminando hoja 2...')
          sleep(2)
        end

        wb.add_worksheet(name: "Alert History") do |sheet|
          #FILAS COMBINADAS
          sheet.merge_cells('D2:P5') #Título del reporte
          sheet.merge_cells('B2:C6') #logo
          sheet.merge_cells('D6:E6') #fecha creación título
          sheet.merge_cells('F6:P6') #fecha creación cont

          #ENCABEZADOS
          sheet.merge_cells('B9:P9') #historic header
          sheet.merge_cells('B11:C11') #ticket header
          sheet.merge_cells('D11:F11') #fecha-hora header
          sheet.merge_cells('G11:H11') #origen header
          sheet.merge_cells('I11:K11') #filtro header
          sheet.merge_cells('L11:N11') #estado header
          sheet.merge_cells('O11:P11') #usuario header

          img = File.expand_path(Rails.root + 'app/assets/images/logoKS.png', __FILE__)
          sheet.add_image(:image_src => img, :noSelect => true, :noMove => true) do |image|
            image.width = 140
            image.height = 101
            #image.start_at 2, 2
            image.anchor.from.rowOff = 2050000
            image.anchor.from.colOff = 1550000
          end

          ##---------------------------------- HEADER, LOGO ------------------------------ ##
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]
          sheet.add_row [" ", " ", " ", "Numero_de_tarjeta: " + @alerta.IdTran, " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, nil, center, center, center, center, center, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, left_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, nil, center, center, center, center, center, center, center, center, center, center, center, center, center, left_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, nil, center, center, center, center, center, center, center, center, center, center, center, center, center, left_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, nil, center, center, center, center, center, center, center, center, center, center, center, center, center, left_thick]

          if @idioma == 'es'
            sheet.add_row [" ", " ", " ", "Fecha de exportación: ", " ", Time.now.to_date.strftime("%d/%m/%Y"), " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, nil, header_dates_bold, header_dates_bold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, left_thick]
          elsif @idioma == 'en'
            sheet.add_row [" ", " ", " ", "Export Date: ", " ", Time.now.to_date.strftime("%d/%m/%Y"), " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, nil, header_dates_bold, header_dates_bold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, left_thick]
          end

          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, nil, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]

          ##----------------------- Historial de Alertas -------------------------##

          if @idioma == 'es'
            sheet.add_row [" ", "Historial de Alertas", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell]
            sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]
            sheet.add_row [" ", "Ticket ", " ", "Fecha/Hora", " ", " ", "Origen", " ", "Filtro", " ", " ", "Estado", " ", " ", "Analista", " ", " "], style: [nil, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell]
          elsif @idioma == 'en'
            sheet.add_row [" ", "Alert History", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell]
            sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]
            sheet.add_row [" ", "Ticket ", " ", "Date/Hour", " ", " ", "Origin", " ", "Filter", " ", " ", "State", " ", " ", "Analyst", " ", " "], style: [nil, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell]
          end

          # @historial = Tbitacora.where(:IdTran => @alerta.IdTran).where.not(:Id => @alerta.Id).where(:IdProducto => 1).order("Fecha Desc").order("Hora Desc")

          @renglon_h = 11

          if @historial.present? && !@historial.nil?
            @historial.each do |his|
              @renglon_h += 1
              @comb_ticket = 'B' + @renglon_h.to_s + ':C' + @renglon_h.to_s
              @comb_fecha = 'D' + @renglon_h.to_s + ':F' + @renglon_h.to_s
              @comb_origen = 'G' + @renglon_h.to_s + ':H' + @renglon_h.to_s
              @comb_filtro = 'I' + @renglon_h.to_s + ':K' + @renglon_h.to_s
              @comb_state = 'L' + @renglon_h.to_s + ':N' + @renglon_h.to_s
              @comb_user = 'O' + @renglon_h.to_s + ':P' + @renglon_h.to_s
              sheet.merge_cells(@comb_ticket) #ticket content
              sheet.merge_cells(@comb_fecha) #fecha content
              sheet.merge_cells(@comb_origen) #origen content
              sheet.merge_cells(@comb_filtro) #filtro content
              sheet.merge_cells(@comb_state) #estado content
              sheet.merge_cells(@comb_user) #usuario content

              if his.IdProducto == 1
                @origen = "Transaccional"
              else
                @origen = "Perfiles"
              end

              if his.tx_filters.present?
                @filtro = his.tx_filters.Id.to_s + " - " + his.tx_filters.Description
              else
                @filtro = "N/A"
              end

              if his.testadosxalerta.present?
                @estado = his.IdEstado.to_s + " - " + his.testadosxalerta.Descripcion
              else
                @estado = "N/A"
              end

              if his.user.present?
                @usuario = his.IdUsuario.to_s + " - " + his.user.name + " " + his.user.last_name
              else
                @usuario = "N/A"
              end


              sheet.add_row [" ", his.Id, " ", his.Fecha + " " + his.Hora, " ", " ", @origen, " ", @filtro, " ", " ", @estado, " ", " ", @usuario, " ", " "], style: [nil, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell], :types => [:string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string]
            end
          else
            sheet.merge_cells('B12:P12') #historial nulo

            if @idioma == 'es'
              sheet.add_row [" ", "No existe información histórica para esta alerta", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell], :types => [:string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string]
            elsif @idioma == 'en'
              sheet.add_row [" ", "There is no historic content for this alert", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell], :types => [:string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string]
            end
          end

          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom], :types => [:string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string]
          ##----------------------- CERRANDO EXCEL -------------------------##
          sheet.column_widths *col_widths
          puts('Terminando hoja 2...')
          sleep(2)
        end

        wb.add_worksheet(name: "Transactional History") do |sheet|
          @campos_referencia = Referenciadealerta.select(:idCampo, :campo).where(:idAlerta => @alerta.Id).group(:idCampo, :campo)
          @cant_campos = @campos_referencia.length.to_i
          @columna_fin = @cant_campos + 2 # Mas margen y la columna ticket
          @columna_fin = @columna_fin.to_s26.to_s.upcase

          puts @columna_fin

          #FILAS COMBINADAS

          @titulo = 'C2:' + @columna_fin + '5'
          @fecha_creacion_cont = 'E6:' + @columna_fin + '6'
          sheet.merge_cells(@titulo) #Título del reporte
          sheet.merge_cells('B2:B6') #logo
          sheet.merge_cells('C6:D6') #fecha creación título
          sheet.merge_cells(@fecha_creacion_cont) #fecha creación cont

          img = File.expand_path(Rails.root + 'app/assets/images/logoKS.png', __FILE__)
          sheet.add_image(:image_src => img, :noSelect => true, :noMove => true) do |image|
            image.width = 140
            image.height = 101
            #image.start_at 2, 2
            image.anchor.from.rowOff = 2050000
            image.anchor.from.colOff = 1550000
          end

          ##---------------------------------- HEADER, LOGO ------------------------------ ##
          arr_row_header = Array.new
          arr_row_header.push(" ")

          arr_empty = Array.new
          arr_empty.push (" ")
          arr_empty.push (" ")

          col_widths_ref = Array.new
          col_widths_ref.push(10)
          col_widths_ref.push(17.5)

          for i in 0..@cant_campos
            arr_row_header.push(" ")
            arr_empty.push(" ")
            col_widths_ref.push(25)
          end


          sheet.add_row arr_row_header
          if @idioma == 'es'
            arr_titulo = arr_row_header
            arr_titulo.insert(2, "Historial Transaccional")
            sheet.add_row arr_titulo, style: [nil, nil, center]
            sheet.add_row arr_row_header, style: [nil, nil, center]
            sheet.add_row arr_row_header, style: [nil, nil, center]
            sheet.add_row arr_row_header, style: [nil, nil, center]
            @estilos = 'D2:' + @columna_fin + '5'
            sheet[@estilos].each {|c| c.style = border_thick}

            #Fecha
            arr_fecha = arr_row_header
            arr_fecha.insert(2, "Fecha de exportación: ")
            arr_fecha.insert(4, Time.now.to_date.strftime("%d/%m/%Y"))
            sheet.add_row arr_titulo, style: [nil, nil, header_dates_bold, header_dates_bold, header_dates_notbold]
            @estilos_fecha = 'F6:' + @columna_fin + '6'
            sheet[@estilos_fecha].each {|c| c.style = border_thick}

          elsif @idioma == 'en'
            arr_titulo = arr_row_header
            arr_titulo.insert(2, "Transactional History")
            sheet.add_row arr_titulo, style: [nil, nil, center]
            sheet.add_row arr_row_header, style: [nil, nil, center]
            sheet.add_row arr_row_header, style: [nil, nil, center]
            sheet.add_row arr_row_header, style: [nil, nil, center]
            @estilos = 'D2:' + @columna_fin + '5'
            sheet[@estilos].each {|c| c.style = border_thick}

            #Fecha
            arr_fecha = arr_row_header
            arr_fecha.insert(2, "Export Date: ")
            arr_fecha.insert(4, Time.now.to_date.strftime("%d/%m/%Y"))
            sheet.add_row arr_titulo, style: [nil, nil, header_dates_bold, header_dates_bold, header_dates_notbold]
            @estilos_fecha = 'F6:' + @columna_fin + '6'
            sheet[@estilos_fecha].each {|c| c.style = border_thick}
          end

          sheet.add_row [" ", " ", " ", " ", " "], style: [nil, nil, bottom_thick, bottom_thick, bottom_thick]
          sheet.add_row [" "]

          ##----------------------- COLUMNAS DE LA REFERENCIA -------------------------#

          arr_titulos_ref = Array.new
          arr_titulos_ref.push(" ")
          arr_titulos_ref.push("Ticket")

          arr_sect = Array.new
          arr_sect.push(" ")
          if @idioma == 'es'
            arr_sect.push("Transacciones")
          elsif @idioma == 'en'
            arr_sect.push(" Transactions ")
          end

          @campos_referencia.each do |tit_ref|
            arr_titulos_ref.push(tit_ref.campo.strip)
            arr_sect.push(" ")
          end

          sheet.add_row arr_sect, style: [nil]
          @estilos_columnas_sect = 'B9:' + @columna_fin + '9'
          sheet.merge_cells(@estilos_columnas_sect) #Título de sección
          sheet[@estilos_columnas_sect].each {|c| c.style = section__cell}
          sheet.add_row [" "]


          sheet.add_row arr_titulos_ref, style: [nil]
          @estilos_columnas_header = 'B11:' + @columna_fin + '11'
          sheet[@estilos_columnas_header].each {|c| c.style = highlight_cell}



          arr_empty_tran = Array.new
          arr_empty_tran.push(" ")
          if @idioma == 'es'
            arr_empty_tran.push("No existe información transaccional para esta alerta ")
          elsif @idioma == 'en'
            arr_empty_tran.push("There is no transactional content for this alert")
          end

          @row = 11
          if @transacciones.present? && !@transacciones.nil?
            @transacciones.each do |his|
              @row =  @row + 1
              arr_valor = Array.new
              arr_valor.push(" ")
              arr_valor.push(@alerta.Id.to_s)
              @campos_referencia.each do |c|
                @campor = c.campo.remove("'")
                arr_valor.push(his[@campor])
              end

              sheet.add_row arr_valor, style: [nil]
              @estilos_valor = 'B' + @row.to_s + ':' + @columna_fin + @row.to_s
              sheet[@estilos_valor].each {|c| c.style = content_cell}
              sheet[@estilos_valor].each {|c| c.type = :string}
            end


          else
            @campos_referencia.each do |tit_ref|
              arr_empty_tran.push(" ")
            end

            @estilos_valor = 'B' + @row.to_s + ':' + @columna_fin + @row.to_s
            sheet[@estilos_valor].each {|c| c.style = content_cell}
            sheet[@estilos_valor].each {|c| c.type = :string}
          end


          sheet.add_row arr_empty, style: [nil, bottom, bottom, bottom, bottom, bottom, bottom, bottom]
          @row = @row += 1
          @estilos_final = 'B' + @row.to_s + ':' + @columna_fin + @row.to_s
          sheet[@estilos_final].each {|c| c.style = bottom}
          ##----------------------- CERRANDO HOJA 2 -------------------------##
          sheet.column_widths *col_widths_ref

          puts('Terminando hoja 4...')
          sleep(2)
        end
      end


      ##-------------------- GUARDANDO EXCEL ----------------------##
      # p.serialize('public/generatedXLS/reporteOperativo/reporteOperativo.xlsx')
      p.serialize("public/generatedXLS/alerts/Alert#{@id}.xlsx")

      @file = "Alert#{@id}.xlsx"

      puts 'Archivo creado: ' + @file

      puts('Archivo almacenado...')

      @fecha_termino = Time.now
      # --------------------------------- CORREO ELECTRÓNICO -------------------------------- #
      usuario = User.find(@user_id)
      if @idioma == 'es'
        XlsMailer.send_xls_es(@fecha_comienzo, usuario, @file, @fecha_termino).deliver
      elsif @idioma == 'en'
        XlsMailer.send_xls(@fecha_comienzo, usuario, @file, @fecha_termino).deliver
      else
        puts 'Sin idioma'
      end

      puts 'Listo, proceso terminado'
    rescue => e
      puts red('Archivo: log/error_xls.log')
      logger = Logger.new("log/error_xls.log")
      logger.error('----------------------------------------------------------')
      logger.error(e)
    end

  end

  def alertLevelXLS
    begin
      puts '>------------------------------------------------------------------------<'
      puts green('Iniciando Hilo: Creación de XLS-Niveles de Alertamiento')
      puts green('Cantidad de registros: ' + @alertas.count().to_s)
      @fecha_comienzo = Time.now
      ## -------------------- COMIENZA LA CREACIÓN DEL EXCEL ------------------------##

      ##------------------------------ ANCHO DE LAS FILAS ------------------------------- ##
      col_widths = [10, 17.5, 21, 15, 25, 118, 15, 20, 10]
      p = Axlsx::Package.new
      p.use_autowidth = true
      wb = p.workbook

      sleep(2)

      wb.styles do |style|
        highlight_cell = style.add_style(bg_color: "396F9B", alignment: {horizontal: :center}, :sz => 12, border: Axlsx::STYLE_THIN_BORDER, :b => true)
        section__cell = style.add_style(bg_color: "003057", alignment: {horizontal: :center}, :sz => 12, border: Axlsx::STYLE_THIN_BORDER, :b => true, :fg_color => 'FFFFFFFF')
        #Titulo
        main_title_cell = style.add_style(bg_color: "396F9B", alignment: {horizontal: :center}, :sz => 12, border: Axlsx::STYLE_THIN_BORDER, :b => true)
        content_cell = style.add_style(alignment: {horizontal: :center}, :sz => 10, border: {:edges => [:left, :right], :style => :thin, :color => 'FF000000'})
        date_cell = style.add_style(:num_fmt => Axlsx::NUM_FMT_YYYYMMDDHHMMSS, alignment: {horizontal: :center}, :sz => 10, border: {:edges => [:left, :right], :style => :thin, :color => 'FF000000'})
        border_header_dates_bold = style.add_style(:sz => 12, :b => true, border: {:edges => [:top, :left], :style => :thick, :color => 'FF000000'})
        border_header_dates_notbold = style.add_style(:sz => 12, :b => false, border: {:edges => [:top], :style => :thick, :color => 'FF000000'})
        header_dates_bold = style.add_style(:sz => 12, :b => true, border: {:edges => [:left], :style => :thick, :color => 'FF000000'})
        header_dates_notbold = style.add_style(alignment: {horizontal: :left}, :sz => 12, :b => false, :num_fmt => Axlsx::NUM_FMT_YYYYMMDDHHMMSS)
        bottom = style.add_style(border: {:edges => [:top], :style => :thin, :color => 'FF000000'})
        bottom_thick = style.add_style(border: {:edges => [:top], :style => :thick, :color => 'FF000000'})
        left_thick = style.add_style(border: {:edges => [:left], :style => :thick, :color => 'FF000000'})
        center = style.add_style(alignment: {horizontal: :center, vertical: :center}, border: {:style => :thick, :color => 'FF000000'}, :sz => 16, :b => true)
        top_thick = style.add_style(border: {:edges => [:top], :style => :thick, :color => 'FF000000'}, :sz => 16, :b => true)
        border_thick = style.add_style(border: {:edges => [:top, :left, :right, :bottom], :style => :thick, :color => 'FF000000'}, :sz => 16, :b => true)

        ##**************************----------------------- HOJA ALERTAS -------------------------**************************##
        wb.add_worksheet(name: "Alerts") do |sheet|

          #FILAS COMBINADAS
          sheet.merge_cells('C2:H5') #Título del reporte
          sheet.merge_cells('B2:B6') #logo
          sheet.merge_cells('C6:D6') #fecha creación título
          sheet.merge_cells('E6:H6') #fecha creación cont

          img = File.expand_path(Rails.root + 'app/assets/images/logoKS.png', __FILE__)
          sheet.add_image(:image_src => img, :noSelect => true, :noMove => true) do |image|
            image.width = 140
            image.height = 101
            #image.start_at 2, 2
            image.anchor.from.rowOff = 2050000
            image.anchor.from.colOff = 1550000
          end

          ##---------------------------------- HEADER, LOGO ------------------------------ ##
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]
          if @idioma == 'es'
            # VIENE DE ALERTC CONTROLLER
            if @filtro == 'general transaccional' || @filtro == 'general perfiles'
              if @filtro == 'general transaccional'
                sheet.add_row [" ", " ", "Resultado de Alertas de KM Transaccional", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, border_thick, border_thick, border_thick, border_thick, border_thick, left_thick]
              elsif @filtro == 'general perfiles'
                sheet.add_row [" ", " ", "Resultado de Alertas de KM Perfiles", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, border_thick, border_thick, border_thick, border_thick, border_thick, left_thick]
              end
            else # VIENE DE NIV ALERTAMIENTO CONTROLLER
              if @filtro == 'states'
                @filtro = 'estados'
              elsif @filtro == 'users'
                @filtro = 'usuarios'
              end
              sheet.add_row [" ", " ", "Resultado de Alertas Filtro" + @filtro.titleize, " ", " ", " ", " ", " ", " "], style: [nil, nil, center, border_thick, border_thick, border_thick, border_thick, border_thick, left_thick]
            end
          elsif @idioma == 'en'
            # VIENE DE ALERTC CONTROLLER
            if @filtro == 'general transaccional' || @filtro == 'general perfiles'
              if @filtro == 'general transaccional'
                sheet.add_row [" ", " ", "Alerts Result KM Transaccional", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, border_thick, border_thick, border_thick, border_thick, border_thick, left_thick]
              elsif @filtro == 'general perfiles'
                sheet.add_row [" ", " ", "Alerts Result KM Perfiles", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, border_thick, border_thick, border_thick, border_thick, border_thick, left_thick]
              end
            else # VIENE DE NIV ALERTAMIENTO CONTROLLER
              if @filtro == 'estados'
                @filtro = 'states'
              elsif @filtro == 'usuarios'
                @filtro = 'users'
              end
              sheet.add_row [" ", " ", "Alerts Result " + @filtro.titleize + " Filter", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, border_thick, border_thick, border_thick, border_thick, border_thick, left_thick]
            end

          end

          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, center, center, center, center, center, left_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, center, center, center, center, center, left_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, center, center, center, center, center, left_thick]

          if @idioma == 'es'
            sheet.add_row [" ", " ", "Fecha de exportación: ", " ", Time.now.to_date.strftime("%d/%m/%Y"), " ", " ", " ", " "], style: [nil, nil, header_dates_bold, header_dates_bold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, left_thick]
          elsif @idioma == 'en'
            sheet.add_row [" ", " ", "Export Date: ", " ", Time.now.to_date.strftime("%d/%m/%Y"), " ", " ", " ", " "], style: [nil, nil, header_dates_bold, header_dates_bold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, left_thick]
          end

          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]


          ##----------------------- ALERTAS -------------------------#
          if @idioma == 'es'
            sheet.add_row [" ", "Ticket", "Fecha/Hora", "Origen", "Numero_de_Tarjeta", "Mensaje", "Estado", "Analista", " "], style: [nil, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell]
          elsif @idioma == 'en'
            sheet.add_row [" ", "Ticket", "Date/Hour", "Origin", "Numero_de_Tarjeta", "Message", "State", "Analyst", " "], style: [nil, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell]
          end

          @alertas.each do |al|
            if al.IdProducto == 1
              @origen = "Transaccional"
            else
              @origen = "Perfiles"
            end

            if al.user.present?
              @usuario = al.user.name + ' ' + al.user.last_name
            else
              @usuario = 'N/A'
            end

            sheet.add_row [" ", al.Id.to_s, al.Fecha.to_s + ' ' + al.Hora.to_s, @origen, al.IdTran.to_s, al.Mensaje.to_s, al.IdEstado.to_s + '-' + al.testadosxalerta.Descripcion, @usuario, " "], style: [nil, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell], :types => [:string, :string, :string, :string, :string, :string, :string]

          end

          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, bottom, bottom, bottom, bottom, bottom, bottom, bottom]
          ##----------------------- CERRANDO HOJA 1 -------------------------##
          sheet.column_widths *col_widths

          puts('Terminando hoja 1...')
          sleep(2)
        end

        ##**************************----------------------- HOJA REFERENCIAS -------------------------**************************##
        wb.add_worksheet(name: "References") do |sheet|
          @campos_referencia = Referenciadealerta.select(:idCampo, :campo).all.group(:idCampo, :campo)
          @cant_campos = @campos_referencia.length.to_i
          puts cyan("Campos de la referencia: " + @cant_campos.to_s)
          @columna_fin = @cant_campos + 2 # Mas margen y la columna ticket
          @columna_fin = @columna_fin.to_s26.to_s.upcase

          puts @columna_fin

          #FILAS COMBINADAS

          @titulo = 'C2:' + @columna_fin + '5'
          @fecha_creacion_cont = 'E6:' + @columna_fin + '6'
          sheet.merge_cells(@titulo) #Título del reporte
          sheet.merge_cells('B2:B6') #logo
          sheet.merge_cells('C6:D6') #fecha creación título
          sheet.merge_cells(@fecha_creacion_cont) #fecha creación cont

          img = File.expand_path(Rails.root + 'app/assets/images/logoKS.png', __FILE__)
          sheet.add_image(:image_src => img, :noSelect => true, :noMove => true) do |image|
            image.width = 140
            image.height = 101
            #image.start_at 2, 2
            image.anchor.from.rowOff = 2050000
            image.anchor.from.colOff = 1550000
          end

          ##---------------------------------- HEADER, LOGO ------------------------------ ##
          arr_row_header = Array.new
          arr_row_header.push(" ")

          arr_empty = Array.new
          arr_empty.push (" ")
          arr_empty.push (" ")

          col_widths_ref = Array.new
          col_widths_ref.push(10)
          col_widths_ref.push(17.5)

          for i in 0..@cant_campos
            arr_row_header.push(" ")
            arr_empty.push(" ")
            col_widths_ref.push(25)
          end


          sheet.add_row arr_row_header
          if @idioma == 'es'
            arr_titulo = arr_row_header
            arr_titulo.insert(2, "Referencias de Alertas")
            sheet.add_row arr_titulo, style: [nil, nil, center]
            sheet.add_row arr_row_header, style: [nil, nil, center]
            sheet.add_row arr_row_header, style: [nil, nil, center]
            sheet.add_row arr_row_header, style: [nil, nil, center]
            @estilos = 'D2:' + @columna_fin + '5'
            sheet[@estilos].each {|c| c.style = border_thick}

            #Fecha
            arr_fecha = arr_row_header
            arr_fecha.insert(2, "Fecha de exportación: ")
            arr_fecha.insert(4, Time.now.to_date.strftime("%d/%m/%Y"))
            sheet.add_row arr_titulo, style: [nil, nil, header_dates_bold, header_dates_bold, header_dates_notbold]
            @estilos_fecha = 'F6:' + @columna_fin + '6'
            sheet[@estilos_fecha].each {|c| c.style = border_thick}

          elsif @idioma == 'en'
            arr_titulo = arr_row_header
            arr_titulo.insert(2, "Alerts References")
            sheet.add_row arr_titulo, style: [nil, nil, center]
            sheet.add_row arr_row_header, style: [nil, nil, center]
            sheet.add_row arr_row_header, style: [nil, nil, center]
            sheet.add_row arr_row_header, style: [nil, nil, center]
            @estilos = 'D2:' + @columna_fin + '5'
            sheet[@estilos].each {|c| c.style = border_thick}

            #Fecha
            arr_fecha = arr_row_header
            arr_fecha.insert(2, "Export Date: ")
            arr_fecha.insert(4, Time.now.to_date.strftime("%d/%m/%Y"))
            sheet.add_row arr_titulo, style: [nil, nil, header_dates_bold, header_dates_bold, header_dates_notbold]
            @estilos_fecha = 'F6:' + @columna_fin + '6'
            sheet[@estilos_fecha].each {|c| c.style = border_thick}
          end

          sheet.add_row [" ", " ", " ", " ", " "], style: [nil, nil, bottom_thick, bottom_thick, bottom_thick]
          sheet.add_row [" "]

          ##----------------------- COLUMNAS DE LA REFERENCIA -------------------------#

          arr_titulos_ref = Array.new
          arr_titulos_ref.push(" ")
          arr_titulos_ref.push("Ticket")

          @campos_referencia.each do |tit_ref|
            arr_titulos_ref.push(tit_ref.campo.strip)
          end

          sheet.add_row arr_titulos_ref, style: [nil]
          @estilos_columnas_header = 'B9:' + @columna_fin + '9'
          sheet[@estilos_columnas_header].each {|c| c.style = highlight_cell}

          @row = 9
          @alertas.each do |al|
            @row += 1
            arr_valor = Array.new
            arr_valor.push(" ")
            arr_valor.push(al.Id.to_s)
            @campos_referencia.each do |tit_ref|
              @valor = Referenciadealerta.select(:valor).find_by(:idCampo => tit_ref.idCampo, :idAlerta => al.Id)

              if @valor.present? && !@valor.nil? && @valor != "" && @valor != " "
                arr_valor.push(@valor.valor)
              else
                arr_valor.push("--")
              end
            end

            sheet.add_row arr_valor, style: [nil]
            @estilos_valor = 'B' + @row.to_s + ':' + @columna_fin + @row.to_s
            sheet[@estilos_valor].each {|c| c.style = content_cell}
            sheet[@estilos_valor].each {|c| c.type = :string}
          end

          sheet.add_row arr_empty, style: [nil, bottom, bottom, bottom, bottom, bottom, bottom, bottom]
          @row = @row += 1
          @estilos_final = 'B' + @row.to_s + ':' + @columna_fin + @row.to_s
          sheet[@estilos_final].each {|c| c.style = bottom}
          ##----------------------- CERRANDO HOJA 2 -------------------------##
          sheet.column_widths *col_widths_ref

          puts('Terminando hoja 2...')
          sleep(2)
        end
      end


      ##-------------------- GUARDANDO EXCEL ----------------------##
      # p.serialize('public/generatedXLS/reporteOperativo/reporteOperativo.xlsx')
      puts @filtro
      if @filtro == 'estados' || @filtro == 'states'
        p.serialize("public/generatedXLS/niv_alert/ResultsStatesFilterGenerator-#{Time.now.strftime('%H%M%S')}.xlsx")
        @file = "ResultsStatesFilterGenerator-#{Time.now.strftime('%H%M%S')}.xlsx"
      elsif @filtro == 'usuarios' || @filtro == 'users'
        p.serialize("public/generatedXLS/niv_alert/ResultsUsersFilterGenerator-#{Time.now.strftime('%H%M%S')}.xlsx")
        @file = "ResultsUsersFilterGenerator-#{Time.now.strftime('%H%M%S')}.xlsx"
      elsif @filtro == 'general transaccional'
        p.serialize("public/generatedXLS/niv_alert/ResultsKMTxConsult-#{Time.now.strftime('%H%M%S')}.xlsx")
        @file = "ResultsKMTxConsult-#{Time.now.strftime('%H%M%S')}.xlsx"
      elsif @filtro == 'general perfiles'
        p.serialize("public/generatedXLS/niv_alert/ResultsKMPerConsult-#{Time.now.strftime('%H%M%S')}.xlsx")
        @file = "ResultsKMPerConsult-#{Time.now.strftime('%H%M%S')}.xlsx"
      end

      puts 'Archivo creado: ' + @file

      puts('Archivo almacenado...')

      @fecha_termino = Time.now
      # --------------------------------- CORREO ELECTRÓNICO -------------------------------- #
      usuario = User.find(@user_id)
      if @idioma == 'es'
        if (@filtro == 'estados' || @filtro == 'states') || (@filtro == 'usuarios' || @filtro == 'users')
          @asunto = "Exportación de Filtro Niveles de Alertamiento a Excel lista:"
        elsif @filtro == 'general transaccional' || @filtro == 'general perfiles'
          @asunto = "Exportación de Consulta de Alertas a Excel lista:"
        end
        XlsMailer.send_level_xls_es(@fecha_comienzo, usuario, @file, @fecha_termino, @asunto).deliver
      elsif @idioma == 'en'
        if (@filtro == 'estados' || @filtro == 'states') || (@filtro == 'usuarios' || @filtro == 'users')
          @asunto = "Export of Filter in Alerting Levels to Excel is ready:"
        elsif @filtro == 'general transaccional' || @filtro == 'general perfiles'
          @asunto = "Export of Alert Consult to Excel is ready:"
        end
        XlsMailer.send_level_xls(@fecha_comienzo, usuario, @file, @fecha_termino, @asunto).deliver
      else
        puts 'Sin idioma'
      end

      puts 'Listo, proceso terminado'
    rescue => e
      puts red('Archivo: log/error_xls.log')
      logger = Logger.new("log/error_xls.log")
      logger.error('----------------------------------------------------------')
      logger.error(e)
    end
  end

  def generalXls
    begin
      puts '>------------------------------------------------------------------------<'
      puts green('Iniciando Hilo: Creación de XLS-Con-Gen')
      puts green('Cantidad de registros: ' + @registros.count().to_s)
      @fecha_comienzo = Time.now

      ##------------------------------ ANCHO DE LAS FILAS ------------------------------- ##
      col_widths = [10, 15, 15, 17, 17, 30, 15, 15, 20, 70.5, 10] #Se contruye el tamaño de las celdas.
      p = Axlsx::Package.new #Se crea un nuevo libro en excel
      p.use_autowidth = true
      wb = p.workbook

      sleep(2)

      wb.styles do |style|
        highlight_cell = style.add_style(bg_color: "396F9B", alignment: {horizontal: :center}, :sz => 12, border: Axlsx::STYLE_THIN_BORDER, :b => true)
        section__cell = style.add_style(bg_color: "003057", alignment: {horizontal: :center}, :sz => 12, border: Axlsx::STYLE_THIN_BORDER, :b => true, :fg_color => 'FFFFFFFF')
        #Titulo
        main_title_cell = style.add_style(bg_color: "396F9B", alignment: {horizontal: :center}, :sz => 12, border: Axlsx::STYLE_THIN_BORDER, :b => true)
        content_cell = style.add_style(alignment: {horizontal: :center}, :sz => 10, border: {:edges => [:left, :right], :style => :thin, :color => 'FF000000'})
        date_cell = style.add_style(:num_fmt => Axlsx::NUM_FMT_YYYYMMDDHHMMSS, alignment: {horizontal: :center}, :sz => 10, border: {:edges => [:left, :right], :style => :thin, :color => 'FF000000'})
        border_header_dates_bold = style.add_style(:sz => 12, :b => true, border: {:edges => [:top, :left], :style => :thick, :color => 'FF000000'})
        border_header_dates_notbold = style.add_style(:sz => 12, :b => false, border: {:edges => [:top], :style => :thick, :color => 'FF000000'})
        header_dates_bold = style.add_style(:sz => 12, :b => true, border: {:edges => [:left], :style => :thick, :color => 'FF000000'})
        header_dates_notbold = style.add_style(alignment: {horizontal: :left}, :sz => 12, :b => false, :num_fmt => Axlsx::NUM_FMT_YYYYMMDDHHMMSS)
        bottom = style.add_style(border: {:edges => [:top], :style => :thin, :color => 'FF000000'})
        bottom_thick = style.add_style(border: {:edges => [:top], :style => :thick, :color => 'FF000000'})
        left_thick = style.add_style(border: {:edges => [:left], :style => :thick, :color => 'FF000000'})
        center = style.add_style(alignment: {horizontal: :center, vertical: :center}, border: {:style => :thick, :color => 'FF000000'}, :sz => 16, :b => true)
        top_thick = style.add_style(border: {:edges => [:top], :style => :thick, :color => 'FF000000'}, :sz => 16, :b => true)
        border_thick = style.add_style(border: {:edges => [:top, :left, :right, :bottom], :style => :thick, :color => 'FF000000'}, :sz => 16, :b => true)

        ##**************************----------------------  HOJA CONSULTA GENERAL -------------------------**************************##
        wb.add_worksheet(name: "General") do |sheet|

          #FILAS COMBINADAS
          sheet.merge_cells('C2:J5') #Título del reporte
          sheet.merge_cells('B2:B6') #logo
          sheet.merge_cells('C6:D6') #fecha creación título
          sheet.merge_cells('E6:J6') #fecha creación cont

          img = File.expand_path(Rails.root + 'app/assets/images/logoKS.png', __FILE__)
          sheet.add_image(:image_src => img, :noSelect => true, :noMove => true) do |image|
            image.width = 120
            image.height = 93
            #image.start_at 2, 2
            image.anchor.from.rowOff = 2050000
            image.anchor.from.colOff = 1550000
          end
          ##---------------------------------- HEADER, LOGO ------------------------------ ##
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]
          if @idioma == 'es'
            sheet.add_row [" ", " ", "Resultados de Consulta General", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, left_thick]
          elsif @idioma == 'en'
            sheet.add_row [" ", " ", "Result from General Search", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, left_thick]
          end

          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, center, center, center, center, center, center, center, left_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, center, center, center, center, center, center, center, left_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, center, center, center, center, center, center, center, left_thick]

          if @idioma == 'es'
            sheet.add_row [" ", " ", "Fecha de exportación: ", " ", Time.now.to_date.strftime("%d/%m/%Y"), " ", " ", " ", " ", " ", " "], style: [nil, nil, header_dates_bold, header_dates_bold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, left_thick]
          elsif @idioma == 'en'
            sheet.add_row [" ", " ", "Export Date: ", " ", Time.now.to_date.strftime("%d/%m/%Y"), " ", " ", " ", " ", " ", " "], style: [nil, nil, header_dates_bold, header_dates_bold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, left_thick]
          end

          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]

          ##----------------------- ALERTAS -------------------------#
          if @idioma == 'es'
            sheet.add_row [" ", "Ticket", "Producto", "Layout", "Formato", "Fecha/Hora", "IP", "Usuario", "Tipo de Resgistro", "Mensaje", " "], style: [nil, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell]
          elsif @idioma == 'en'
            sheet.add_row [" ", "Ticket", "Product", "Layout", "Format", "Date/Hour", "IP", "User", "Register_Type", "Message", " "], style: [nil, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell]
          end

          @registros.each do |al|
            if al.IdProducto == "1" || al.IdProducto == 1
              @producto = "Transaccional"
            elsif al.IdProducto == "2"
              @producto = "Perfiles" || al.IdProducto == 2
            end

            if al.user.present?
              @usuario = al.user.name + ' ' + al.user.last_name
            else
              @usuario = 'N/A'
            end

            @formatos = Tformatos.all
            @formatos.each do |f|
              if f.IdFormato == al.IdFormato
                @formato = f.Descripcion
              else
                @formato = 'N/A'
              end
            end

            sheet.add_row [" ", al.Id.to_s, @producto, al.tlay.Descripcion.to_s, @formato, al.Fecha.to_s + ' ' + al.Hora.to_s, al.IP.to_s, @usuario, al.ttiporegalarma.Descripcion.to_s, al.Mensaje.to_s, " "], style: [nil, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell]

          end

          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom]

          ##----------------------- CERRANDO HOJA 1 -------------------------##
          sheet.column_widths *col_widths

          puts('Terminando hoja 1...')
          sleep(2)
        end
      end

      p.serialize("public/generatedXLS/con_gen/ResultExportfromGeneralSearch-#{Time.now.strftime('%H%M%S')}.xlsx")
      @file = "ResultExportfromGeneralSearch-#{Time.now.strftime('%H%M%S')}.xlsx"

      puts 'Archivo creado: ' + @file

      puts('Archivo almacenado...')

      @fecha_termino = Time.now

      # --------------------------------- CORREO ELECTRÓNICO -------------------------------- #
      usuario = User.find(@user_id)
      if @idioma == 'es'
        XlsConGenMailer.send_con_gen_xls_es(@fecha_comienzo, usuario, @file, @fecha_termino).deliver
      elsif @idioma == 'en'
        XlsConGenMailer.send_con_gen_xls_en(@fecha_comienzo, usuario, @file, @fecha_termino).deliver
      else
        puts 'Sin idioma'
      end

      puts 'Listo, proceso terminado'

    rescue => e
      puts red('Archivo: log/error_xls.log')
      logger = Logger.new("log/error_xls.log")
      logger.error('----------------------------------------------------------')
      logger.error(e)
    end

  end

  def red(mytext)
    ; "\e[31m#{mytext}\e[0m";
  end

  def green(mytext)
    ; "\e[32m#{mytext}\e[0m";
  end

  def cyan(mytext)
    ; "\e[36m#{mytext}\e[0m"
  end


end