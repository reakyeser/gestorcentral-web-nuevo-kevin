class TxSessionsController < ApplicationController
  before_action :set_tx_session, only: [:show, :edit, :update, :destroy]

  # GET /tx_sessions
  # GET /tx_sessions.json

  @@session_name = "Sessions"

  def index

    if current_user
      @cu = current_user.profile_id

      if @cu != 0
        # @permisos = Permission.where("view_name = 'Sessions' and profile_id = ?", @cu)
        #
        # @permisos.each do |permisos|
        #   @crearSession = permisos.crear
        #   @editarSession = permisos.editar
        #   @leerSession = permisos.leer
        #   @eliminarSession = permisos.eliminar
        #
        #   if permisos.view_name == @@session_name
        #     @@crearSession = permisos.crear # 8
        #     @@editarSession = permisos.editar # 4
        #     @@leerSession = permisos.leer # 2
        #     @@eliminarSession = permisos.eliminar # 1
        #
        #     @crearSession = permisos.crear
        #     @editarSession = permisos.editar
        #     @leerSession = permisos.leer
        #     @eliminarSession = permisos.eliminar
        #
        #
        #   end
        #
        # end

        if current_user.habilitado == 0
          # if ( @@crearSession == 8 || @@editarSession == 4 || @@leerSession == 2 || @@eliminarSession == 1 )
            ##Aquí va el código que leera el index
            #
            # @tx_sessions = TxSessions.all
            tx_sessions = TxSessions.all
            gon.sesiones = tx_sessions

            # Gon para Description
            desFiltro = TxFilters.all
            gon.desFiltros = desFiltro

            ## Gon para llenar selects Layout, Formato de ventana modal
            tFormatos = Tformatos.all
            gon.formatos = tFormatos

            tCampos = Tcamposformato.all
            gon.tCamposFormatos = tCampos

            tLays = Tcamposlay.all
            gon.tCamposLays = tLays
            gon.false = true


            # Paginado de kaminari
            @tx_sessions = TxSessions.order("Id").page(params[:page]).per(10)

          # else
          #   @Without_Permission = 100
          #   redirect_to home_index_path, :alert => t('all.not_access')
          # end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_enabled')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  # GET /tx_sessions/1
  # GET /tx_sessions/1.json
  def show
  end

  # GET /tx_sessions/new
  def new
    if current_user
      @cu = current_user.profile_id

      if @cu != 0
        @permisos = Permission.where("view_name = 'Sessions' and profile_id = ?", @cu)

        @permisos.each do |permisos|
          @crearSession = permisos.crear
          @editarSession = permisos.editar
          @leerSession = permisos.leer
          @eliminarSession = permisos.eliminar

          if permisos.view_name == @@session_name
            @@crearSession = permisos.crear # 8
            @@editarSession = permisos.editar # 4
            @@leerSession = permisos.leer # 2
            @@eliminarSession = permisos.eliminar # 1

            @crearSession = permisos.crear
            @editarSession = permisos.editar
            @leerSession = permisos.leer
            @eliminarSession = permisos.eliminar


          end

        end

        if current_user.habilitado == 0
          if ( @@crearSession == 8 || @@editarSession == 4 || @@leerSession == 2 || @@eliminarSession == 1 )
            ##Aquí va el código que leera el index
            #
            #
            @tx_session = TxSessions.new
            @edt = false;

            tx_sessions = TxSessions.all
            gon.sesiones = tx_sessions

            tFormatos = Tformatos.all
            gon.formatos = tFormatos

            tCampos = Tcamposformato.all
            gon.tCamposFormatos = tCampos

            tLays = Tcamposlay.all
            gon.tCamposLays = tLays
            gon.false = true

          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_enabled')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  # GET /tx_sessions/1/edit
  def edit
    if current_user
      @cu = current_user.profile_id

      if @cu != 0
        @permisos = Permission.where("view_name = 'Sessions' and profile_id = ?", @cu)

        @permisos.each do |permisos|
          @crearSession = permisos.crear
          @editarSession = permisos.editar
          @leerSession = permisos.leer
          @eliminarSession = permisos.eliminar

          if permisos.view_name == @@session_name
            @@crearSession = permisos.crear # 8
            @@editarSession = permisos.editar # 4
            @@leerSession = permisos.leer # 2
            @@eliminarSession = permisos.eliminar # 1

            @crearSession = permisos.crear
            @editarSession = permisos.editar
            @leerSession = permisos.leer
            @eliminarSession = permisos.eliminar


          end

        end

        if current_user.habilitado == 0
          if ( @@crearSession == 8 || @@editarSession == 4 || @@leerSession == 2 || @@eliminarSession == 1 )
            ##Aquí va el código que leera el index
            #
            @edt = true;
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_enabled')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  # POST /tx_sessions
  # POST /tx_sessions.json
  def create


    if (params[:_method].present? && params[:_method] == "update")
      @valorActive = params[:Inactive]
      @idSe = params[:Id]

      @tx_session = TxSessions.where(:Id => @idSe)
      @tx_session.each do |session|
        session.Inactive = @valorActive
        session.save
      end
    else
      ##### Vairables de AJAX #####
      # // Variables de la Sesión
      #
      @idLay = params[:selectLay]
      @idFormat = params[:selectFormato]

      @tx_session = TxSessions.new(tx_session_params)
      # @tx_session = TxSessions.new
      @tx_session.Lay_id = @idLay
      @tx_session.Format_id= @idFormat
      # @tx_session.Description = @desSession
      @tx_session.Inactive = 0
      @tx_session.Update = 0
      @tx_session.save

      # // Variables del Filtro
      @desFilter = params[:filter_name]
      @typeFilter = params[:type_filter]
      @background = params[:backgroundColor]

      # // Detalle del filtro
      @detalleFiltro = params[:condicionesFiltro].split(',')
      @idUser = params[:keyId]

      respond_to do |format|
        if @tx_session.save

          @tx_session_filter = TxFilters.new
          @tx_session_filter.Session_id = @tx_session.id
          @tx_session_filter.Description = @desFilter
          @tx_session_filter.Order_number = @typeFilter
          @tx_session_filter.Backgroundcolor = @background
          @tx_session_filter.save

          #// Detalle del Filtro
          @cont = 0
          @numCondi = 0
          @crearCondicion = false

          @conector
          @negado
          @operador
          @parentesis
          @valor
          @campoFormatoId
          @posision
          @length

          @detalleFiltro.each do | datoFiltro |
            @cont += 1

            if ( @cont === 1 )
              @conector = datoFiltro
            elsif ( @cont === 2 )
              @negado = datoFiltro
            elsif ( @cont === 3 )
              @operador = datoFiltro
            elsif ( @cont === 4 )
              @parentesis = datoFiltro
            elsif ( @cont === 5 )
              @valor = datoFiltro
            elsif ( @cont === 6 )
              @campoFormatoId = datoFiltro
            elsif ( @cont === 7 )
              @posision = datoFiltro
            elsif ( @cont === 8 )
              @length = datoFiltro
              @cont = 0
              @crearCondicion = true
            end

            if ( @crearCondicion  === true )
              @crearCondicion = false
              @numCondi += 1
              # @tx_filter_detail = TxFilterDetail.new(tx_filter_detail_params) marco error enviandolo desde el metodo new de session
              @tx_filter_detail = TxFilterDetail.new
              @tx_filter_detail.filter_id = @tx_session_filter.id
              @tx_filter_detail.conditionnumber = @numCondi
              @tx_filter_detail.connector = @conector
              @tx_filter_detail.denied = @negado
              @tx_filter_detail.operator = @operador
              @tx_filter_detail.parenthesis = @parentesis
              @tx_filter_detail.value = @valor
              @tx_filter_detail.fieldFormat_id = @campoFormatoId
              @tx_filter_detail.position = @posision
              @tx_filter_detail.lenght = @length
              @tx_filter_detail.key_id = @idUser
              @tx_filter_detail.save
            end
          end

          @id_Session = @tx_session.id

          ### Guardando campos de referencia
          @camposRefencia = params[:camposRef].split(',')
          @camposRefencia.each do | campoRef |
            @tx_schedules_reference = TxScheduleReference.new
            @tx_schedules_reference.field_formatId = campoRef
            @tx_schedules_reference.Filter_id = @id_Session
            @tx_schedules_reference.save
          end

          ## Datos para el horario de alertamiento con mensaje
          @hr_st = params[:Hr_Start].split(':')
          @hrS1 = @hr_st[0]
          @hrS2 = @hr_st[1]

          @hr_en = params[:Hr_End].split(':')
          @hrE1 = @hr_en[0]
          @hrE2 = @hr_en[1]

          @hora_start = "#{@hrS1}#{@hrS2}"
          @hora_end = "#{@hrE1}#{@hrE2}"

          @limite = params[:limit]
          @limiteTipo = params[:select_limit]

          if @limiteTipo == '1'
            @max_tran = @limite
            @min_tran = 0
          elsif @limiteTipo == '2'
            @max_tran = 0
            @min_tran = @limite
          end

          @interval = params[:Interval]
          @umInterval = params[:UMInterval]
          @intermediate = params[:Intermediate]
          @umIntermediate = params[:UMIntermediate]
          @timeOut = params[:TimeOut]
          @umTimeOut = params[:UMTimeout]

          @tx_filter_schedules = TxFilterSchedule.new
          @tx_filter_schedules.filter_id = @tx_session_filter.id
          @tx_filter_schedules.Hr_Start = @hora_start
          @tx_filter_schedules.Hr_End = @hora_end
          @tx_filter_schedules.Max_Tran = @max_tran
          @tx_filter_schedules.Min_Tran = @min_tran
          @tx_filter_schedules.Interval = @interval
          @tx_filter_schedules.UMInterval = @umInterval
          @tx_filter_schedules.Intermediate = @intermediate
          @tx_filter_schedules.UMIntermediate = @umIntermediate
          @tx_filter_schedules.TimeOut = @timeOut
          @tx_filter_schedules.UMTimeout = @umTimeOut
          @tx_filter_schedules.save


          #//-- Creacion del mensaje
          if params[:msn].present?
            params[:msn].each do |key,obj|
              @tx_schedule_message = TxScheduleMessage.new(obj.permit(:Num_Alarma, :message, :bit_date, :bit_description, :bit_escala, :bit_limit, :bit_reference))
              @tx_schedule_message.schedule_id = @tx_filter_schedules.id
              @tx_schedule_message.save
            end
          end


          # format.html { redirect_to action: :index, notice: 'Tx session was successfully created.' }
          format.html { redirect_to action: :index, notice: 'Tx session was successfully created.' }
          # @tx_session_filters_details = TxFilterDetail.where(:filter_id => nil).where(:key_id => current_user.id)
          # @tx_session_filters_details.each do | detalle |
          #   @cont += 1
          #   detalle.filter_id = @tx_session_filter.id
          #   detalle.conditionnumber = @cont
          #   detalle.key_id = nil
          #   detalle.save
          # end

          # format.html { redirect_to @tx_session, notice: 'Tx session was successfully created.' }
          # format.json { render :show, status: :created, location: @tx_session }
          format.json { render :index, location: @tx_session }
        else
          format.html { render :new }
          format.json { render json: @tx_session.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /tx_sessions/1
  # PATCH/PUT /tx_sessions/1.json
  def update
    respond_to do |format|
      if @tx_session.update(tx_session_params)
        format.html { redirect_to action: :index, notice: 'Tx session was successfully updated.' }
        format.json { render :show, status: :ok, location: @tx_session }
      else
        format.html { render :edit }
        format.json { render json: @tx_session.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tx_sessions/1
  # DELETE /tx_sessions/1.json
  def destroy

    @tx_session_filters = TxFilters.where(:Session_id => @tx_session.id)

    @tx_session_filters.each do |filter|

      ## Eliminar detalle de filtro
      @tx_session_filters_details = TxFilterDetail.where(:filter_id => filter.id)
      @tx_session_filters_details.each do | filterDeatil |
        filterDeatil.destroy
      end

      ## Elimina los campos de referencia
      @tx_schedules_reference = TxScheduleReference.where(:Filter_id => filter.id)
      @tx_schedules_reference.each do | camposRef |
        camposRef.destroy
      end

      ## Eliminar horarios de alertamiento y mensaje
      @tx_filter_schedules = TxFilterSchedule.where(:filter_id => filter.id)
      @tx_filter_schedules.each do | horarios |

        @tx_schedules_message = TxScheduleMessage.where(:schedule_id => horarios.id )
        @tx_schedules_message.each do | mensajes |
          mensajes.destroy
        end

        horarios.destroy
      end

      filter.destroy
    end

    @tx_session.destroy
    respond_to do |format|
      # format.html { redirect_to tx_sessions_url, notice: 'Tx session was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tx_session
      @tx_session = TxSessions.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tx_session_params
      params.require(:tx_sessions).permit(:Id, :Lay_id, :Format_id, :Description, :Sessioncolor, :Interval, :Inactive)
    end
end
