class CategoriFiltersController < ApplicationController
  before_action :set_categori_filter, only: [:show, :edit, :update, :destroy]

  # GET /categori_filters
  # GET /categori_filters.json
  def index
    @categori_filters = CategoriFilter.all
  end

  # GET /categori_filters/1
  # GET /categori_filters/1.json
  def show
  end

  # GET /categori_filters/new
  def new
    @categori_filter = CategoriFilter.new
  end

  # GET /categori_filters/1/edit
  def edit
  end

  # POST /categori_filters
  # POST /categori_filters.json
  def create
    @categori_filter = CategoriFilter.new(categori_filter_params)

    respond_to do |format|
      if @categori_filter.save
        format.html { redirect_to @categori_filter, notice: 'Categori filter was successfully created.' }
        format.json { render :show, status: :created, location: @categori_filter }
      else
        format.html { render :new }
        format.json { render json: @categori_filter.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /categori_filters/1
  # PATCH/PUT /categori_filters/1.json
  def update
    respond_to do |format|
      if @categori_filter.update(categori_filter_params)
        format.html { redirect_to @categori_filter, notice: 'Categori filter was successfully updated.' }
        format.json { render :show, status: :ok, location: @categori_filter }
      else
        format.html { render :edit }
        format.json { render json: @categori_filter.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /categori_filters/1
  # DELETE /categori_filters/1.json
  def destroy
    @categori_filter.destroy
    respond_to do |format|
      format.html { redirect_to categori_filters_url, notice: 'Categori filter was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_categori_filter
      @categori_filter = CategoriFilter.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def categori_filter_params
      params.require(:categori_filter).permit(:id_categori, :id_filter)
    end
end
