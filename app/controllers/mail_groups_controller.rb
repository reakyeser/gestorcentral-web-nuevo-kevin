class MailGroupsController < ApplicationController
  before_action :set_mail_group, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!

  @@mailGroup = "EmailGroup"

  # GET /mail_groups
  # GET /mail_groups.json
  def index
    # if current_user

    @id_usuario = current_user.id
    @id_aplicacion = 25
    @idTipoReg = 11 #Index
    @fecha = Time.now.strftime("%F")
    @hora = Time.now.strftime("%k:%M:%S.00000")
    @men = "Mails"


    @tbit = Tbitacora.new
    @tbit.IdUsuario = @id_usuario
    @tbit.IdAplicacion = @id_aplicacion
    @tbit.IdTipoReg = @idTipoReg #Index
    @tbit.Fecha = @fecha
    @tbit.Hora  = @hora
    @tbit.Mensaje = @men
    @tbit.save

    #   @cu = current_user.profile_id
    #   if @cu == 1
    #     @per = Permission.where("view_name = 'EmailGroup' and profile_id = ?", @cu)
    #     @per.each do |permisos|
    #       @uno = permisos.view_name
    #       @crearMail = permisos.crear
    #       @editarMail = permisos.editar
    #       @leerMail = permisos.leer
    #       @eliminarMail = permisos.eliminar
    #       if permisos.view_name == @@mailGroup
    #         @@crear = permisos.crear
    #         @@editar = permisos.editar
    #         @@leer = permisos.leer
    #         @@eliminar = permisos.eliminar
    #
    #         @crear = permisos.crear
    #         @editar = permisos.editar
    #         @leer = permisos.leer
    #         @eliminar = permisos.eliminar
    #       end
    #     end
    #     if ((@@crear == 8) || (@@leer == 2) || (@@editar == 4) || (@@eliminar == 1))
    if params[:id].present?
      @mail_groups = MailGroup.where('group_id = ?', params[:id])
    else
      @mail_groups = MailGroup.all
    end
    # else
    #   @Without_Permission = 100
    #   redirect_to home_index_path, :alert => t('all.not_access')
    # end
    # else
    # @Without_Permission = 100
    # redirect_to home_index_path, :alert => t('all.not_access')
    # end
    # else
    # @Without_Permission = 100
    # redirect_to new_user_session_path, :alert => t('all.please_continue')
    # end
  end

  # GET /mail_groups/1
  # GET /mail_groups/1.json
  def show
  end

  # GET /mail_groups/new
  def new
    # if current_user
    #   @cu = current_user.profile_id
    #   if @cu == 1
    #     @per = Permission.where("view_name = 'EmailGroup' and profile_id = ? ", @cu)
    #     @per.each do |permisos|
    #       @uno = permisos.view_name
    #       @crearMail = permisos.crear
    #       if permisos.view_name == @@mailGroup
    #         @@crear = permisos.crear
    #         @crear = permisos.crear
    #       end
    #     end
    #     if ((@@crear == 8))
    if (params.has_key?(:id))
      @id_gr = params[:id]
      @mail_group = MailGroup.new
      @mail_group.group_id = @id_gr
    else
      @mail_group = MailGroup.new
    end
    #     else
    #       @Without_Permission = 100
    #       redirect_to home_index_path, :alert => t('views.not_access')
    #     end
    #   else
    #     @Without_Permission = 100
    #     redirect_to home_index_path, :alert => t('views.not_access')
    #   end
    # else
    #   @Without_Permission = 100
    #   redirect_to new_user_session_path, :alert => t('views.please_continue')
    # end
  end

  # GET /mail_groups/1/edit
  def edit
    # if current_user
    #   @cu = current_user.profile_id
    #   if @cu == 1
    #     @per = Permission.where("view_name = 'EmailGroup' and profile_id = ? ", @cu)
    #     @per.each do |permisos|
    #       @uno = permisos.view_name
    #       @editarMail = permisos.editar
    #       if permisos.view_name == @@mailGroup
    #         @@editar = permisos.editar
    #         @editar = permisos.editar
    #       end
    #     end
    #     if ((@editar == 4))
    #
    #     else
    #       @Without_Permission = 100
    #       redirect_to new_user_session_path, :alert => t('views.not_access')
    #     end
    #   else
    #     @Without_Permission = 100
    #     redirect_to new_user_session_path, :alert => t('views.not_access')
    #   end
    # else
    #   @Without_Permission = 100
    #   redirect_to new_user_session_path, :alert => t('views.please_continue')
    # end
  end

  # POST /mail_groups
  # POST /mail_groups.json
  def create
    @id_usuario = current_user.id
    @id_aplicacion = 25
    @taplicacioneskm = "prueba"
    @idTipoReg = 9 #Create
    @fecha = Time.now.strftime("%F")
    @hora = Time.now.strftime("%k:%M:%S.00000")
    @men = "Mails New"
    # alertas = cuando se entre a editar alerta se va a guardar el id de tbitacora(alerta que se está atendiendo) y el id de producto, transaccional o perfiles <== aun no


    @tbit = Tbitacora.new
    @tbit.IdUsuario = @id_usuario
    @tbit.IdAplicacion = @id_aplicacion
    @tbit.IdTipoReg = @idTipoReg #Edit
    @tbit.Fecha = @fecha
    @tbit.Hora  = @hora
    @tbit.Mensaje = @men
    @tbit.save

    @mail_group = MailGroup.new(mail_group_params)
    @mail_group.group_id = params[:id_group]

    respond_to do |format|
      if MailGroup.where("correo like ?", @mail_group.correo).exists?
        format.html {
          @error = t('notice.blackup_Correo')
          render action: :new
        }
      else
        if @mail_group.save
          format.html {redirect_to groups_path, notice: 'Mail group was successfully created.'}
          format.json {render :show, status: :created, location: @mail_group}
        else
          format.html {render :new}
          format.json {render json: @mail_group.errors, status: :unprocessable_entity}
        end
      end
    end
  end

  # PATCH/PUT /mail_groups/1
  # PATCH/PUT /mail_groups/1.json
  def update

    @id_usuario = current_user.id
    @id_aplicacion = 25
    @idTipoReg = 10 #Edit
    @fecha = Time.now.strftime("%F")
    @hora = Time.now.strftime("%k:%M:%S.00000")
    @men = "Mails Edit"
    # alertas = cuando se entre a editar alerta se va a guardar el id de tbitacora(alerta que se está atendiendo) y el id de producto, transaccional o perfiles <== aun no


    @tbit = Tbitacora.new
    @tbit.IdUsuario = @id_usuario
    @tbit.IdAplicacion = @id_aplicacion
    @tbit.IdTipoReg = @idTipoReg #Edit
    @tbit.Fecha = @fecha
    @tbit.Hora  = @hora
    @tbit.Mensaje = @men
    @tbit.save

    respond_to do |format|
      if @mail_group.update(mail_group_params)
        format.html {redirect_to groups_path, notice: 'Mail group was successfully updated.'}
        format.json {render :show, status: :ok, location: @mail_group}
      else
        format.html {render :edit}
        format.json {render json: @mail_group.errors, status: :unprocessable_entity}
      end
    end
  end

  # DELETE /mail_groups/1
  # DELETE /mail_groups/1.json
  def destroy
    # if current_user
    #   @cu = current_user.profile_id
    #   if @cu == 1
    #     @per = Permission.where("view_name = 'EmailGroup' and profile_id = ?", @cu)
    #     @per.each do |permisos|
    #       @uno = permisos.view_name
    #       @eliminarMail = permisos.eliminar
    #       if permisos.view_name == @@mailGroup
    #         @@eliminar = permisos.eliminar
    #         @eliminar = permisos.eliminar
    #       end
    #     end
    #     if ((@@eliminar == 1))
    @mail_group.destroy
    respond_to do |format|
      format.js
      format.html {redirect_to posts_url, notice: 'Mail group was successfully destroyed.'}
      format.json {head :no_content}
    end
    #     else
    #       @Without_Permission = 100
    #       redirect_to home_index_path, :alert => t('views.not_access')
    #     end
    #   else
    #     @Without_Permission = 100
    #     redirect_to home_index_path, :alert => t('views.not_access')
    #   end
    # else
    #   @Without_Permission = 100
    #   redirect_to new_user_session_path, :alert => t('views.please_continue')
    # end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_mail_group
    @mail_group = MailGroup.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def mail_group_params
    params.require(:mail_group).permit(:group_id, :correo)
  end

end
