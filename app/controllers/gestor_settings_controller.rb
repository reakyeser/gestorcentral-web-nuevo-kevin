class GestorSettingsController < ApplicationController
  before_action :set_gestor_setting, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  respond_to :html, :js

  # @@byhorario = "Horario"

  # GET /gestor_settings
  # GET /gestor_settings.json
  def index
    # if curent_user
    #   @cu = current_user.profile_id
    #   if @cu == 1
    #     @per = Permission.where("view_name = 'Horario' and profile_id = ?", @cu)
    #     @per.each do |permisos|
    #       @uno = permisos.view_name
    #       @crearHorario = permisos.crear
    #       @editarHorario = permisos.editar
    #       @leerHorario = permisos.leer
    #
    #       if permisos.view_name == @@byhorario
    #         @@crear = permisos.crear
    #         @@editar = permisos.editar
    #         @@leer = permisos.leer
    #
    #         @crear = permisos.crear
    #         @editar = permisos.editar
    #         @leer = permisos.leer
    #       end
    #     end
    #     if ((@@leer == 2) || (@@crear == 8) || (@@editar == 4))
          @gestor_settings = GestorSetting.last
          @settings = GestorSetting.all.count
    #     else
    #       @Without_Permission = 100
    #       redirect_to home_index_path, :alert => t('all.not_access')
    #     end
    #   else
    #     @Without_Permission = 100
    #     redirect_to home_index_path, :alert => t('all.not_access')
    #   end
    # else
    #   @Without_Permission = 100
    #   redirect_to new_user_session_path, :alert => t('all.please_continue')
    # end

  end

  # GET /gestor_settings/1
  # GET /gestor_settings/1.json
  def show
  end

  # GET /gestor_settings/new
  def new
    # if current_user
    #   @cu = current_user.profile_id
    #   if @cu == 1
    #     @per = Permission.where("view_name = 'Horario' and profile_id = ?", @cu)
    #     @per.each do |permisos|
    #       @uno = permisos.view_name
    #       @crearHorario = permisos.crear
    #       if permisos.view_name == @@byhorario
    #         @@crear = permisos.crear
    #         @crear = permisos.crear
    #       end
    #     end
    #     if ((@@crear == 8))
    @setting = GestorSetting.last
    @gestor_setting = GestorSetting.new
    #     else
    #       @Without_Permission = 100
    #       redirect_to home_index_path, :alert => t('all.not_access')
    #     end
    #   else
    #     @Without_Permission = 100
    #     redirect_to home_index_path, :alert => t('all.not_access')
    #   end
    # else
    #   @Without_Permission = 100
    #   redirect_to new_user_session_path, :alert => t('all.please_continue')
    # end
  end

  # GET /gestor_settings/1/edit
  def edit
    # if current_user
    #   @cu = current_user.profile_id
    #
    #   if @cu == 1
    #     @per = Permission.where("view_name = 'Horario' and profile_id = ?", @cu)
    #     @per.each do |permisos|
    #       @uno = permisos.view_name
    #       @@editarHorario = permisos.editar
    #       if permisos.view_name == @@group
    #         @@editar = permisos.editar
    #         @editar = permisos.editar
    #       end
    #     end
    #     if ((@@editar == 4))
    #
    #     else
    #       @Without_Permission = 100
    #       redirect_to home_index_path, :alert => t('all.not_access')
    #     end
    #   else
    #     @Without_Permission = 100
    #     redirect_to home_index_path, :alert => t('all.not_access')
    #   end
    #
    # else
    #   @Without_Permission = 100
    #   redirect_to new_user_session_path, :alert => t('all.please_continue')
    # end
  end

  # POST /gestor_settings
  # POST /gestor_settings.json
  def create
    @day_start = params[:gestor_setting][:day_start]
    @day_end= params[:gestor_setting][:day_end]
    @gestor_setting = GestorSetting.new(gestor_setting_params)

    respond_to do |format|
      if @gestor_setting.save
        format.html {redirect_to gestor_settings_path, notice: t('notice.succes_Schedule') }
        format.json {render :show, status: :created, location: @gestor_setting}
      else
        format.html {render :new}
        format.json {render json: @gestor_setting.errors, status: :unprocessable_entity}
      end
    end
  end

  # PATCH/PUT /gestor_settings/1
  # PATCH/PUT /gestor_settings/1.json
  def update
    respond_to do |format|
      if @gestor_setting.update(gestor_setting_params)
        format.html {redirect_to gestor_settings_path, notice: t('notice.update_Schedule') }
        format.json {render :show, status: :ok, location: @gestor_setting}
      else
        format.html {render :edit}
        format.json {render json: @gestor_setting.errors, status: :unprocessable_entity}
      end
    end
  end

  # DELETE /gestor_settings/1
  # DELETE /gestor_settings/1.json
  def destroy
    @gestor_setting.destroy
    respond_to do |format|
      format.html {redirect_to gestor_settings_url, notice: t('notice.destroy_Schedule')}
      format.json {head :no_content}
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_gestor_setting
    @gestor_setting = GestorSetting.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def gestor_setting_params
    params.require(:gestor_setting).permit(:blue_day, :blue_night, :yellow_day, :yellow_night, :red_day, :red_night, :blue_day_end, :blue_night_end, :yellow_day_end, :yellow_night_end, :red_day_end, :red_night_end, :day_start, :day_end, :night_start, :night_end)
  end
end
