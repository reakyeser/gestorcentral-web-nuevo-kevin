class TxScheduleMessagesController < ApplicationController
  before_action :set_tx_schedule_message, only: [:show, :edit, :update, :destroy]

  # GET /tx_schedule_messages
  # GET /tx_schedule_messages.json
  def index
    @tx_schedule_messages = TxScheduleMessage.all
  end

  # GET /tx_schedule_messages/1
  # GET /tx_schedule_messages/1.json
  def show
  end

  # GET /tx_schedule_messages/new
  def new
    @tx_schedule_message = TxScheduleMessage.new
  end

  # GET /tx_schedule_messages/1/edit
  def edit
  end

  # POST /tx_schedule_messages
  # POST /tx_schedule_messages.json
  def create
    @tx_schedule_message = TxScheduleMessage.new(tx_schedule_message_params)

    respond_to do |format|
      if @tx_schedule_message.save
        format.html { redirect_to @tx_schedule_message, notice: 'Tx schedule message was successfully created.' }
        format.json { render :show, status: :created, location: @tx_schedule_message }
      else
        format.html { render :new }
        format.json { render json: @tx_schedule_message.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tx_schedule_messages/1
  # PATCH/PUT /tx_schedule_messages/1.json
  def update
    respond_to do |format|
      if @tx_schedule_message.update(tx_schedule_message_params)
        format.html { redirect_to @tx_schedule_message, notice: 'Tx schedule message was successfully updated.' }
        format.json { render :show, status: :ok, location: @tx_schedule_message }
      else
        format.html { render :edit }
        format.json { render json: @tx_schedule_message.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tx_schedule_messages/1
  # DELETE /tx_schedule_messages/1.json
  def destroy
    @tx_schedule_message.destroy
    respond_to do |format|
      format.html { redirect_to tx_schedule_messages_url, notice: 'Tx schedule message was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tx_schedule_message
      @tx_schedule_message = TxScheduleMessage.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tx_schedule_message_params
      params.require(:tx_schedule_message).permit(:schedule_id, :Num_Alarma, :letter_color, :background_color, :message, :bit_date, :bit_description, :bit_escala, :bit_limit, :bit_reference)
    end
end
