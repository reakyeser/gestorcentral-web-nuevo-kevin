class AlertnController < ApplicationController
  before_action :set_alertn, only: [:show, :edit, :update, :destroy]

  # GET /alertn
  # GET /alertn.json
  @@byNewTran = "NewAlertTransaccional"
  @@byNewPer = "NewAlertPerfiles"

  def index
    if current_user
      @cu = current_user.profile_id

      if @cu != 0
        @per_n_tran = Permission.where("view_name = 'NewAlertTransaccional' and profile_id = ?", @cu)

        @per_n_tran.each do |permisos|
          @leerNewTran = permisos.leer

          if permisos.view_name == @@byNewTran

            @@crearNewTran = permisos.crear
            @@editarNewTran = permisos.editar
            @@leerNewTran = permisos.leer
            @@eliminarNewTran = permisos.eliminar

            @crearNewTran = permisos.crear
            @editarNewTran = permisos.editar
            @leerNewTran = permisos.leer
            @eliminarNewTran = permisos.eliminar
          end
        end

        @per_n_per = Permission.where("view_name = 'NewAlertPerfiles' and profile_id = ?", @cu)

        @per_n_per.each do |permisos|
          @leerNewPer = permisos.leer

          if permisos.view_name == @@byNewPer

            @@crearNewPer = permisos.crear
            @@editarNewPer = permisos.editar
            @@leerNewPer = permisos.leer
            @@eliminarNewPer = permisos.eliminar

            @crearNewPer = permisos.crear
            @editarNewPer = permisos.editar
            @leerNewPer = permisos.leer
            @eliminarNewPer = permisos.eliminar
          end

        end

        if current_user.habilitado == 0

          # if (@@leerNewPer == 2 || @@leerNewTran == 2)

            # tbitacora = @tbitacora.new


            @bitaAlertasxAtender = Tbitacora.where(:IdEstado => 1).where(:canceladas => 0).where(:IdTipoReg => 3).where(:IdProducto => 1).count
            @bitaAlertasxAtender = Tbitacora.where(:IdUsuario => 'N/A')

            @alert_Tran = Tbitacora.where(:IdEstado => 1).where(:canceladas => 0).where(:IdTipoReg => 3).where(:IdProducto => 1).count
            @alert_Per = Tbitacora.where(:IdEstado => 1).where(:canceladas => 0).where(:IdTipoReg => 3).where(:IdProducto => 2).count
          # else
          #   @Without_Permission = 100
            # redirect_to home_index_path, :alert => t('all.not_access')
          # end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_enabled')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  # GET /alertn/1
  # GET /alertn/1.json
  def show
    if current_user.habilitado == 0
    else
      @Without_Permission = 100
      redirect_to home_index_path, :alert => t('all.not_enabled')
    end
  end

  # GET /alertn/new
  def new
    if current_user.habilitado == 0
      @alertn = Alertn.new
    else
      @Without_Permission = 100
      redirect_to home_index_path, :alert => t('all.not_enabled')
    end
  end

  # GET /alertn/1/edit
  def edit
    if current_user.habilitado == 0
    else
      @Without_Permission = 100
      redirect_to home_index_path, :alert => t('all.not_enabled')
    end

  end

  # POST /alertn
  # POST /alertn.json
  def create
    @alertn = Alertn.new(alertn_params)

    respond_to do |format|
      if @alertn.save
        format.html {redirect_to @alertn, notice: 'Alertn was successfully created.'}
        format.json {render :show, status: :created, location: @alertn}
      else
        format.html {render :new}
        format.json {render json: @alertn.errors, status: :unprocessable_entity}
      end
    end
  end

  # PATCH/PUT /alertn/1
  # PATCH/PUT /alertn/1.json
  def update
    respond_to do |format|
      if @alertn.update(alertn_params)
        format.html {redirect_to @alertn, notice: 'Alertn was successfully updated.'}
        format.json {render :show, status: :ok, location: @alertn}
      else
        format.html {render :edit}
        format.json {render json: @alertn.errors, status: :unprocessable_entity}
      end
    end
  end

  # DELETE /alertn/1
  # DELETE /alertn/1.json
  def destroy

    if current_user.habilitado == 0
      @alertn.destroy
      respond_to do |format|
        format.html {redirect_to alertns_url, notice: 'Alertn was successfully destroyed.'}
        format.json {head :no_content}
      end
    else
      @Without_Permission = 100
      redirect_to home_index_path, :alert => t('all.not_enabled')
    end
  end

  def alertTran

    if current_user
      @cu = current_user.profile_id

      if @cu != 0
        @per_n_tran = Permission.where("view_name = 'NewAlertTransaccional' and profile_id = ?", @cu)

        @per_n_tran.each do |permisos|
          @crearNewTran = permisos.crear
          @editarNewTran = permisos.editar
          @leerNewTran = permisos.leer

          if permisos.view_name == @@byNewTran

            @@crearNewTran = permisos.crear
            @@editarNewTran = permisos.editar
            @@leerNewTran = permisos.leer
            @@eliminarNewTran = permisos.eliminar

            @crearNewTran = permisos.crear
            @editarNewTran = permisos.editar
            @leerNewTran = permisos.leer
            @eliminarNewTran = permisos.eliminar
          end
        end

        if current_user.habilitado == 0
          @alert_Tran = Tbitacora.where(:IdEstado => 1).where(:canceladas => 0).where(:IdTipoReg => 3).where(:IdProducto => 1).count

          if (@@leerNewTran == 2 && @alert_Tran > 0)

            st = Testadosxalerta.select(:IdEstado, :falso_positivo, :mensajeDefault).all

            gon.estados = st
            gon.false = true

            if params[:id] #---------------------------------------------------------------------------SEGUIMIENTO DE ALERTA
              @id_consulta = params[:id]
              @alerta_Tran = Tbitacora.where(:Id => @id_consulta).first
              @nuevo = true
              @seguimiento = true

              # Codigo Alfredo Junio282018
              puts "RefeAlfredo"
              @referencia = Referenciadealerta.where("idAlerta = #{@id_consulta}")

              @observaciones = Tbitaobse.where(:IdAlerta => @id_consulta).order("Id Desc").page(params[:page]).per(10)
              @observaciones_xls = Tbitaobse.where(:IdAlerta => @id_consulta).order("Id Desc")

              #Verificar que esa cuenta no se esté atendiendo por alguien más
              @atendiendo_cta = Tatendiendo.where(:IdTran => @alerta_Tran.IdTran).where.not(:IdUsuario => current_user.id)

              if @atendiendo_cta.present? && !@atendiendo_cta.nil?
                redirect_to alertc_index_path, :alert => t('all.alert_atn')
              else
                #Agregar el registro de que el usuario está atendiendo una cuenta
                @atendiendo = Tatendiendo.new
                @atendiendo.IdUsuario = current_user.id
                @atendiendo.IdTran = @alerta_Tran.IdTran
                @atendiendo.save
              end

              #Transacciones que ya fueron asociadas
              @fraud_s = TransaccionesDeAlerta.where(:idAlerta => @IdAlerta).where(:transaction_type => 1)
              @pending_s = TransaccionesDeAlerta.where(:idAlerta => @IdAlerta).where(:transaction_type => 2)
              frauds = ""
              pends = ""

              if @fraud_s.present? && !@fraud_s.nil?
                @fraud_s.each do |fr|
                  frauds = frauds + fr.ttTableName.to_s + ',' + fr.idTran.to_s + '|'
                end
              end

              if @pending_s.present? && !@pending_s.nil?
                @pending_s.each do |pen|
                  pends = pends + pen.ttTableName.to_s + ',' + pen.idTran.to_s + '|'
                end
              end

              @fraud_s = frauds
              @pending_s = pends
            elsif params[:observaciones] #----------------------------------------------------------- #ALERTA A LA QUE SE LE AGREGO OBSERVACIÓN
              @id_tran = params[:id_tran]
              @id_consulta = @id_tran
              @alerta_Tran = Tbitacora.where(:Id => @id_tran).first
              @referencia = Referenciadealerta.where("idAlerta = #{@alerta_Tran.Id}")
              @nuevo = true
              @seguimiento = true

              @observaciones = Tbitaobse.where(:IdAlerta => @id_consulta).order("Id Desc").page(params[:page]).per(10)
              @observaciones_xls = Tbitaobse.where(:IdAlerta => @id_consulta).order("Id Desc")

              #Verificar que esa cuenta no se esté atendiendo por alguien más
              @atendiendo_cta = Tatendiendo.where(:IdTran => @alerta_Tran.IdTran).where.not(:IdUsuario => current_user.id)

              if @atendiendo_cta.present? && !@atendiendo_cta.nil?
                redirect_to alertc_index_path, :alert => t('all.alert_atn')
              else
                #Agregar el registro de que el usuario está atendiendo una cuenta
                @atendiendo = Tatendiendo.new
                @atendiendo.IdUsuario = current_user.id
                @atendiendo.IdTran = @alerta_Tran.IdTran
                @atendiendo.save
              end

            else #-------------------------------------------------------------------------------ASIGNA NUEVA ALERTA
              #Verificar si el usuario ya tenía cuentas atendiendo actualmente
              @cuentas_ocupando = Tatendiendo.where(:IdUsuario => current_user.id)
              if @cuentas_ocupando.present? && !@cuentas_ocupando.nil?
                @cuentas_ocupando.each do |cta|
                  cta.destroy
                end
              end

              #Verificar si hay cuentas que se están atendiendo de otros usuarios
              @cuentas_ocupadas = Tatendiendo.all
              query = ""

              if @cuentas_ocupadas.present? && !@cuentas_ocupadas.nil?
                @cuentas_ocupadas.each do |cta|
                  if query == ""
                    # query = query + "#{t('config.main')} != '#{cta.IdTran}'"
                    query = query + "IdTran != '#{cta.IdTran}'"
                  else
                    # query = query + " and #{t('config.main')} != '#{cta.IdTran}'"
                    query = query + " and IdTran != '#{cta.IdTran}'"
                  end
                end
              end

              # Verificar las prioridades del usuario
              @prioridades_usuario = Txfilterxusr.where(:IdUsuario => current_user.id)
              if @prioridades_usuario.present? && !@prioridades_usuario.nil?
                @prioridades_usuario.each do |prio|
                  if query == ""
                    query = query + "IdFiltro = #{prio.Txfiltro_id}"
                  else
                    query = query + " or IdFiltro = #{prio.Txfiltro_id}"
                  end
                end
              end

              # Asignación de alerta de acuerdo a atencion y a prioridad
              if query != ""
                @alerta_Tran = Tbitacora.where(:IdEstado => 1).where(:canceladas => 0).where(:IdTipoReg => 3).where(:IdProducto => 1).where(:IdUsuario => 'N/A').where(query).last
                if @alerta_Tran.present? && !@alerta_Tran.nil?
                  @alerta_Tran = Tbitacora.where(:IdEstado => 1).where(:canceladas => 0).where(:IdTipoReg => 3).where(:IdProducto => 1).where(:IdUsuario => 'N/A').last
                  @referencia = Referenciadealerta.where("idAlerta = #{@alerta_Tran.Id}")
                end
              else
                @alerta_Tran = Tbitacora.where(:IdEstado => 1).where(:canceladas => 0).where(:IdTipoReg => 3).where(:IdProducto => 1).where(:IdUsuario => 'N/A').last
                @referencia = Referenciadealerta.where("idAlerta = #{@alerta_Tran.Id}")
              end

              #Agregar el registro de que el usuario está atendiendo una cuenta
              @atendiendo = Tatendiendo.new
              @atendiendo.IdUsuario = current_user.id
              @atendiendo.IdTran = @alerta_Tran.IdTran
              @atendiendo.save

              @nuevo = true
            end

            #-------------------------------------------------------------------------------------- CODIGO PARA CUALQUIER ALERTA
            mail_groups = MailGroup.all

            gon.emails = mail_groups
            gon.false = true

            @IdAlerta = @alerta_Tran.Id
            @alerta_Tran.IdUsuario = current_user.id
            @alerta_Tran.save


            @IdTran = @alerta_Tran.IdTran

            @ConsultaHistTbit = Tbitacora.where(:IdTran => @IdTran).where.not(:Id => @IdAlerta).where(:IdProducto => 1).order("Fecha Desc").order("Hora Desc").page(params[:page]).per(10)
            @ConsultaHistTbit_xls = Tbitacora.where(:IdTran => @IdTran).where.not(:Id => @IdAlerta).where(:IdProducto => 1).order("Fecha Desc").order("Hora Desc")

            #Se inserta referencia en base y se almacena columnas para TTs
            base_utilizada = ActiveRecord::Base.connection.adapter_name.downcase

            base_utilizada = base_utilizada.to_sym

            case base_utilizada
              when :mysql
                sql_exist = 'SELECT * FROM %{reference_table} WHERE idAlerta = %{idAlerta} LIMIT 1'
                sql_insert = 'INSERT INTO %{reference_table} (idAlerta, idCampo, idProducto, campo, valor, created_at, updated_at) VALUES (%{idAlerta}, %{idCampo}, %{idProducto}, %{campo}, %{valor}, %{created_at}, %{updated_at})'
              when :mysql2
                sql_exist = 'SELECT * FROM %{reference_table} WHERE idAlerta = %{idAlerta} LIMIT 1'
                sql_insert = 'INSERT INTO %{reference_table} (idAlerta, idCampo, idProducto, campo, valor, created_at, updated_at) VALUES (%{idAlerta}, %{idCampo}, %{idProducto}, %{campo}, %{valor}, %{created_at}, %{updated_at})'
              when :sqlite
                sql_exist = 'SELECT * FROM %{reference_table} WHERE idAlerta = %{idAlerta} LIMIT 1'
                sql_insert = 'INSERT INTO %{reference_table} (idAlerta, idCampo, idProducto, campo, valor, created_at, updated_at) VALUES (%{idAlerta}, %{idCampo}, %{idProducto}, %{campo}, %{valor}, %{created_at}, %{updated_at})'
              when :sqlserver
                sql_exist = 'SELECT TOP(1) * FROM %{reference_table} WHERE idAlerta = %{idAlerta}'
                sql_insert = 'INSERT INTO [%{reference_table}] ([idAlerta], [idCampo], [idProducto], [campo], [valor], [created_at], [updated_at]) VALUES (%{idAlerta}, %{idCampo}, %{idProducto}, %{campo}, %{valor}, %{created_at}, %{updated_at})'
            end

            reference_table = 'referenciadealerta'
            @cols_tt = ""

            sql_exist_q = sql_exist % {reference_table: reference_table, idAlerta: @IdAlerta}
            # puts (sql_exist_q)
            Timeout::timeout(0) {@alerta_existente = ActiveRecord::Base::connection.exec_query(sql_exist_q)}

            if @alerta_existente.length == 0 # No existe la referencia de la alerta
              @camposref = @alerta_Tran.Referencia.split(">")

              @camposref.each do |ref|
                @encabezado = ref.split("=").first
                @idCampo = @encabezado[1..3]
                @campo = "'#{@encabezado.from(5)}'"
                @campott = @encabezado.from(5)
                @valor = "'#{ref.split("=").second}'"
                @createDate = "'#{Time.now.to_datetime.strftime("%F %T")}'"

                #Insertar registro
                sql_insert_q = sql_insert % {reference_table: reference_table, idAlerta: @IdAlerta, idCampo: @idCampo, idProducto: 1, campo: "#{@campo}", valor: "#{@valor}", created_at: "#{@createDate}", updated_at: "#{@createDate}"}
                # puts(sql_insert_q)
                Timeout::timeout(0) {@inserted = ActiveRecord::Base::connection.exec_query(sql_insert_q)}

                if @cols_tt == ""
                  @cols_tt = @cols_tt + @campott
                else
                  @cols_tt = @cols_tt + ", " + @campott
                end
              end
            else
              @cols_tt = ""

              @campos_ref = Referenciadealerta.select(:campo).where(:idAlerta => @IdAlerta).group(:campo)

              @campos_ref.each do |cr|
                @campott = cr.campo.remove("'")
                if @cols_tt == ""
                  @cols_tt = @cols_tt + @campott
                else
                  @cols_tt = @cols_tt + ", " + @campott
                end
              end
            end

            # ---------- Observaciones

            @atn_usr = Tatendiendo.find_by(:IdUsuario => current_user.id, :IdTran => @alerta_Tran.IdTran)

            if @atn_usr.present? && !@atn_usr.nil?
              if params[:observaciones]

                if params[:observaciones] != "" && params[:estado] != ""
                  @ne = Tbitaobse.new
                  @ne.IdAlerta = @IdAlerta
                  @ne.Fecha = Time.now.to_date
                  tiempo = Time.now
                  @tiempo = tiempo.strftime("%I:%M:%S")
                  @ne.Hora = @tiempo
                  @ne.IdEstado = params[:estado]
                  @ne.IdUsuarioAtencion = current_user.id
                  @ne.Observaciones = params[:observaciones]
                  @ne.falso_positivo = params[:fraud]
                  @ne.save

                  @estado_ant = @alerta_Tran.IdEstado

                  @alerta_Tran.IdEstado = params[:estado]
                  @alerta_Tran.FalsoPositivo = params[:fraud]
                  @alerta_Tran.save

                  @estado_act = @alerta_Tran.IdEstado

                  if (@estado_ant == 1 || @estado_ant == "1") && (@estado_act != 1 || @estado_act != "1")
                    @atendidasxhoy = Tatendidasxhoy.find_by(:idProducto => 1, :fecha => Time.now.strftime("%Y-%m-%d"))

                    if @atendidasxhoy.present? && !@atendidasxhoy.nil?
                      @cantidad = @atendidasxhoy.cantidad
                      @atendidasxhoy.cantidad = @cantidad + 1
                      @atendidasxhoy.save
                    else
                      @nueva_atendida = Tatendidasxhoy.new
                      @nueva_atendida.fecha = Time.now.strftime("%Y-%m-%d")
                      @nueva_atendida.idProducto = 1
                      @nueva_atendida.cantidad = 1
                      @nueva_atendida.save
                    end

                  end

                  @apply = params[:apply]

                  if @apply == "true"
                    @historico_alert = params[:historico]

                    if !@historico_alert.nil? && @historico_alert != ""
                      historico_al = @historico_alert.split(',')

                      loop do

                        @ne = Tbitaobse.new
                        @ne.IdAlerta = historico_al[0]
                        @ne.Fecha = Time.now.to_date
                        tiempo = Time.now
                        @tiempo = tiempo.strftime("%I:%M:%S")
                        @ne.Hora = @tiempo
                        @ne.IdEstado = params[:estado]
                        @ne.IdUsuarioAtencion = current_user.id
                        @ne.Observaciones = params[:observaciones]
                        @ne.falso_positivo = params[:fraud]
                        @ne.save

                        @alerta_observacion = Tbitacora.find(historico_al[0])

                        @estado_ant = @alerta_observacion.IdEstado

                        if @alerta_observacion
                          @alerta_observacion.IdEstado = params[:estado]
                          @alerta_observacion.IdUsuario = current_user.id
                          @alerta_observacion.FalsoPositivo = params[:fraud]
                          @alerta_observacion.save
                        end

                        @estado_act = @alerta_observacion.IdEstado

                        if (@estado_ant == 1 || @estado_ant == "1") && (@estado_act != 1 || @estado_act != "1")
                          @atendidasxhoy = Tatendidasxhoy.find_by(:idProducto => 1, :fecha => Time.now.strftime("%Y-%m-%d"))

                          if @atendidasxhoy.present? && !@atendidasxhoy.nil?
                            @cantidad = @atendidasxhoy.cantidad
                            @atendidasxhoy.cantidad = @cantidad + 1
                            @atendidasxhoy.save
                          else
                            @nueva_atendida = Tatendidasxhoy.new
                            @nueva_atendida.fecha = Time.now.strftime("%Y-%m-%d")
                            @nueva_atendida.idProducto = 1
                            @nueva_atendida.cantidad = 1
                            @nueva_atendida.save
                          end

                        end

                        historico_al.delete_at(0)
                        break if historico_al.length == 0
                      end
                    end
                  end
                end


                #----------------------------------------------------------- Transacciones asociadas
                @trans = TransaccionesDeAlerta.where(:idAlerta => @IdAlerta)

                if @trans.present? && !@trans.nil?
                  @trans.each do |trans|
                    trans.destroy
                  end
                end

                @fraudes = params[:fraud_s]
                @pendientes = params[:pending]

                if params[:fraud_s].present? && params[:fraud] != ""
                  @fraud = params[:fraud_s].split("|")

                  @fraud.each do |f|
                    @fra = f.split(",")

                    @tran_fraud = TransaccionesDeAlerta.new
                    @tran_fraud.idAlerta = @alerta_Tran.Id
                    @tran_fraud.idTran = @fra[1]
                    @tran_fraud.ttTableName = @fra[0]
                    @tran_fraud.transaction_type = 1
                    @tran_fraud.save
                  end
                end

                if params[:pending].present? && params[:pending] != ""
                  @pending = params[:pending].split("|")

                  @pending.each do |p|
                    @pen = p.split(",")

                    @tran_pending = TransaccionesDeAlerta.new
                    @tran_pending.idAlerta = @alerta_Tran.Id
                    @tran_pending.idTran = @pen[1]
                    @tran_pending.ttTableName = @pen[0]
                    @tran_pending.transaction_type = 2
                    @tran_pending.save
                  end
                end

                #---------------------------------------------------------- FIN Transacciones asociadas



                # -------------- Envio de correo a grupos
                @groups_mail = params[:radioMails]
                @emails = MailGroup.where(:group_id => @groups_mail.to_i)

                if @emails.length != 0
                  @groups_name = Group.where(:id => @groups_mail.to_i)
                  @obse_mail = params[:observaciones]
                  @state_mail = params[:estado]
                  @name_state = Testadosxalerta.where(:IdEstado => @state_mail.to_i)
                  @fraud_mail = params[:fraud]

                  if @fraud_mail == "1"

                    @fraud_mail = "Si"

                  elsif @fraud_mail == "0"
                    @fraud_mail = "No"
                  end
                  @fecha_mail = Time.now.to_date
                  @tiempo_mail = Time.now.strftime("%I:%M:%S")
                  @user = current_user

                  @gr = Group.find(@groups_mail.to_i)
                  @detail = @gr.detail.split(',')
                  @reference = @gr.reference.split(',')
                  @area = @gr.area.split(',')
                  @hist_alert = @gr.hist_alert.split(',')
                  @hist_trans = @gr.hist_trans.split(',')

                  @alerta_Tran = Tbitacora.where(:Id => @id_consulta).first
                  @alerta_TranHist = Tbitacora.where(:Id => @id_consulta).first
                  # @referencia = Referenciadealerta.where("idAlerta = #{@id_consulta}")
                  @camposRef = @alerta_Tran.Referencia.split(">")

                  @valoresDetail = {}
                  @detail.each do |detail|
                    if detail == 'Ticket'
                      @valoresDetail.store(detail, @alerta_Tran.Id)
                    elsif detail == 'Fecha/Hora'
                      @valoresDetail.store(detail, "#{@alerta_Tran.Fecha} #{@alerta_Tran.Hora}")
                    elsif detail == 'Origen'
                      if @alerta_Tran.IdProducto == 1
                        @valoresDetail.store(detail, "Transaccional")
                      else
                        @valoresDetail.store(detail, "Perfiles")
                      end
                    elsif detail == 'Mensaje'
                      @valoresDetail.store(detail, @alerta_Tran.Mensaje)
                    elsif detail == 'Filtro'
                      if @alerta_Tran.tx_filters.present?
                        @valoresDetail.store(detail, "#{@alerta_Tran.tx_filters.Id} - #{@alerta_Tran.tx_filters.Description}")
                      else
                        @valoresDetail.store(detail, "N/A")
                      end
                    end
                  end

                  @word1 = Referenciadealerta.where("idAlerta = #{@id_consulta}")

                  @valoresReference = {}
                  @reference.each do |ref|
                    @word1.each do |wol|
                      if ref == wol.campo
                        @valoresReference.store(ref, wol.valor)
                      end
                    end
                  end

                  @valoresArea = {}
                  @area.each do |area|
                    if area == 'Fecha/Hora'
                      @valoresArea.store(area, "#{@ne.Fecha} #{@ne.Hora}")
                    elsif area == 'Observaciones'
                      @valoresArea.store(area, @ne.Observaciones)
                    elsif area == 'Estado'
                      if @ne.IdEstado == 1
                        @valoresArea.store(area, "#{@ne.IdEstado} - #{"No Atendido"}")
                      elsif @ne.IdEstado == 2
                        @valoresArea.store(area, "#{@ne.IdEstado} - #{"Atendido"}")
                      elsif @ne.IdEstado == 3
                        @valoresArea.store(area, "#{@ne.IdEstado} - #{"En Proceso"}")
                      end
                    elsif area == 'Usuario'
                      @valoresArea.store(area, "#{@ne.IdUsuarioAtencion} - #{current_user.name} #{current_user.last_name}")
                    end
                  end

                  @valoresHist = {}



                  @hist_alert.each do |alert|
                    @ConsultaHistTbit.each do |hist|
                      if alert == 'Ticket'
                        @valoresHist.store(alert, hist.Id)
                      elsif alert == 'Fecha/Hora'
                        @valoresHist.store(alert, "#{hist.Fecha} #{hist.Hora}")
                      elsif alert == 'Origen'
                        if hist.IdProducto == 1
                          @valoresHist.store(alert, "Transaccional")
                        else
                          @valoresHist.store(alert, "Perfiles")
                        end
                      elsif alert == 'Filtro'
                        if hist.tx_filters.present?
                          @valoresHist.store(alert, "#{hist.tx_filters.Id} - #{hist.tx_filters.Description}")
                        else
                          @valoresHist.store(alert, "N/A")
                        end
                      elsif alert == 'Estado'
                        if hist.testadosxalerta.present?
                          @valoresHist.store(alert, "#{hist.testadosxalerta.IdEstado} - #{hist.testadosxalerta.Descripcion}")
                        else
                          @valoresHist.store(alert, "N/A")
                        end
                      elsif alert == 'Analista'
                        if hist.user.present?
                          @valoresHist.store(alert, "#{hist.user.name} #{hist.user.last_name}")
                        else
                          @valoresHist.store(alert, "N/A")
                        end
                      end
                    end
                  end

                  @valoresTrans = {}
                  @hist_trans.each do |trans|
                    @word1.each do |wo1|
                      if trans == wo1.campo
                        @valoresTrans.store(trans, wo1.valor)
                      end
                    end
                  end

                  #Josue
                  CorreoGrupoMailer.mandar_area_trabajo_group(@name_state, @obse_mail, @fraud_mail, @fecha_mail, @tiempo_mail, @emails, @user, @IdTran, @valoresDetail, @valoresReference, @valoresArea, @valoresHist, @valoresTrans).deliver
                  CorreoGrupoMailer.mandar_area_trabajo_user(@name_state, @obse_mail, @fraud_mail, @fecha_mail, @tiempo_mail, @groups_name, @emails, @user, @IdTran, @valoresDetail, @valoresReference, @valoresArea, @valoresHist, @valoresTrans).deliver
                end
                # -------------- Fin envio de correo a grupos
                #
                #Transacciones que ya fueron asociadas
                @fraud_s = TransaccionesDeAlerta.where(:idAlerta => @IdAlerta).where(:transaction_type => 1)
                @pending_s = TransaccionesDeAlerta.where(:idAlerta => @IdAlerta).where(:transaction_type => 2)
                frauds = ""
                pends = ""

                if @fraud_s.present? && !@fraud_s.nil?
                  @fraud_s.each do |fr|
                    frauds = frauds + fr.ttTableName.to_s + ',' + fr.idTran.to_s + '|'
                  end
                end

                if @pending_s.present? && !@pending_s.nil?
                  @pending_s.each do |pen|
                    pends = pends + pen.ttTableName.to_s + ',' + pen.idTran.to_s + '|'
                  end
                end

                @fraud_s = frauds
                @pending_s = pends
              end
            end

            # ----------  Observaciones

            # ----------  Historial transaccional
            @nuevo_hist = true
            @fecha_consulta = 10.days.ago
            tts = Array.new

            while (@fecha_consulta <= Time.now)
              valor = {tt: ''}
              valor[:tt] = 'TT' + @fecha_consulta.strftime('%Y%m%d') + '0101'

              tts.push(valor)
              @fecha_consulta = @fecha_consulta + 1.day
            end

            puts 'TT a consultar: ' + tts.to_s

            transacciones = Array.new
            loop do
              if (ActiveRecord::Base.connection.table_exists? tts[0][:tt].to_s)
                # @consulta = ActiveRecord::Base::connection.exec_query("Select #{@cols_tt} from #{tts[0][:tt].to_s} where Cuenta_CI = #{@IdTran}")
                # puts "Select #{@cols_tt} from #{tts[0][:tt].to_s} where Cuenta_CI = #{@IdTran}"
                @cols_tt = @cols_tt.remove("'")
                puts "************************** #{@cols_tt}"
                @consulta = ActiveRecord::Base::connection.exec_query("Select #{@cols_tt}, ID_TRAN from #{tts[0][:tt].to_s} where #{t('config.main')} = '#{@IdTran}'")

                puts "Select #{@cols_tt}, ID_TRAN from #{tts[0][:tt].to_s} where #{t('config.main')} = '#{@IdTran}'"

                @cols_ref = Referenciadealerta.select(:campo).where(:idAlerta => @IdAlerta).group(:campo, :idCampo).order(:idCampo)

                tt = tts[0][:tt].to_s.remove('TT').chomp('0101').to_date.strftime('%Y-%m-%d')

                if @consulta.present? && !@consulta.nil?

                  @consulta.each do |con|
                    registro = {}
                    registro[:fecha] = tt

                    @cols_ref.each do |cr|
                      @campor = cr.campo.remove("'")
                      registro[@campor] = con[@campor]
                    end

                    registro[:id_tran] = con['ID_TRAN'].to_i
                    registro[:tt] = tts[0][:tt].to_s
                    transacciones.push(registro)
                  end
                end
              end

              tts.delete_at(0)
              break if tts.length == 0
            end

            @transacciones = transacciones
            puts transacciones.to_s

            @states = Testadosxalerta.all.where.not(:IdEstado => '1')

            if params[:rangos_fecha]
              # @transacciones = [{:Ln_Tarj => "PRO1", :Numero_de_Tarjeta => "1000001754085501   ", :Ln_Comer => "PRO1", :Fiid_Comer => "LIVP", :Region => "0000", :ID_Comer => "126258735          ", :Fiid_Term => "LIVP", :Term_ID_Term => "L3060045        ", :Tipo_Tarjeta => "C0", :Codigo_Respuesta => "000", :Monto1 => 5165165, :Monto2 => 51513521}]
              # @hola = "hola"
              # @exito = true
              render :partial => "alertn/histTran"
            end
            # ----------  Historial transaccional

            # ----------  Creación de excel
            if params[:xls_g]
              Thread.new do
                puts 'comienza hilo'
                create_xls = CreateXls.new
                create_xls.setInfoAlertXls(params[:id], params[:user_id], params[:idioma], @observaciones_xls, @ConsultaHistTbit_xls, @transacciones)
                create_xls.alertXls
              end
            end
            # ----------  Creación de excel
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_enabled')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  def alertPer
    if current_user
      @cu = current_user.profile_id

      if @cu != 0
        @per_n_per = Permission.where("view_name = 'NewAlertPerfiles' and profile_id = ?", @cu)

        @per_n_per.each do |permisos|
          @crearNewPer = permisos.crear
          @editarNewPer = permisos.editar
          @leerNewPer = permisos.leer

          if permisos.view_name == @@byNewPer

            @@crearNewPer = permisos.crear
            @@editarNewPer = permisos.editar
            @@leerNewPer = permisos.leer
            @@eliminarNewPer = permisos.eliminar

            @crearNewPer = permisos.crear
            @editarNewPer = permisos.editar
            @leerNewPer = permisos.leer
            @eliminarNewPer = permisos.eliminar
          end

        end

        if current_user.habilitado == 0
          @alert_Per = Tbitacora.where(:IdEstado => 1).where(:canceladas => 0).where(:IdTipoReg => 3).where(:IdProducto => 2).count

          if (@@leerNewPer == 2 && @alert_Per > 0)
            st = Testadosxalerta.select(:IdEstado, :falso_positivo).all

            gon.estados = st
            gon.false = true

            if params[:id]
              @id_consulta = params[:id]
              @alerta_Tran = Tbitacora.where(:Id => @id_consulta).first
              @observaciones = Tbitaobse.where(:IdAlerta => @id_consulta).order("Id Desc").page(params[:page]).per(10)
              @observaciones_xls = Tbitaobse.where(:IdAlerta => @id_consulta).order("Id Desc")
              @nuevo = true

              #Verificar que esa cuenta no se esté atendiendo por alguien más
              @atendiendo_cta = Tatendiendo.where(:IdTran => @alerta_Tran.IdTran).where.not(:IdUsuario => current_user.id)

              if @atendiendo_cta.present? && !@atendiendo_cta.nil?
                redirect_to alertc_index_path, :alert => t('all.alert_atn')
              else
                #Agregar el registro de que el usuario está atendiendo una cuenta
                @atendiendo = Tatendiendo.new
                @atendiendo.IdUsuario = current_user.id
                @atendiendo.IdTran = @alerta_Tran.IdTran
                @atendiendo.save
              end
            elsif params[:observaciones]
              @id_tran = params[:id_tran]
              @id_consulta = @id_tran
              @alerta_Tran = Tbitacora.where(:Id => @id_tran).first
              @nuevo = true

              @observaciones = Tbitaobse.where(:IdAlerta => @id_consulta).order("Id Desc").page(params[:page]).per(10)
              @observaciones_xls = Tbitaobse.where(:IdAlerta => @id_consulta).order("Id Desc")

              #Verificar que esa cuenta no se esté atendiendo por alguien más
              @atendiendo_cta = Tatendiendo.where(:IdTran => @alerta_Tran.IdTran).where.not(:IdUsuario => current_user.id)

              if @atendiendo_cta.present? && !@atendiendo_cta.nil?
                redirect_to alertc_index_path, :alert => t('all.alert_atn')
              else
                #Agregar el registro de que el usuario está atendiendo una cuenta
                @atendiendo = Tatendiendo.new
                @atendiendo.IdUsuario = current_user.id
                @atendiendo.IdTran = @alerta_Tran.IdTran
                @atendiendo.save
              end
            else
              #Verificar si el usuario ya tenía cuentas atendiendo actualmente
              @cuentas_ocupando = Tatendiendo.where(:IdUsuario => current_user.id)
              if @cuentas_ocupando.present? && !@cuentas_ocupando.nil?
                @cuentas_ocupando.each do |cta|
                  cta.destroy
                end
              end

              #Verificar si hay cuentas que se están atendiendo de otros usuarios
              @cuentas_ocupadas = Tatendiendo.all
              query = ""

              if @cuentas_ocupadas.present? && !@cuentas_ocupadas.nil?
                @cuentas_ocupadas.each do |cta|
                  if query == ""
                    query = query + "IdTran != '#{cta.IdTran}'"
                  else
                    query = query + " and IdTran != '#{cta.IdTran}'"
                  end
                end
              end

              # Verificar las prioridades del usuario
              @prioridades_usuario = Talertaxusr.where(:IdUsuario => current_user.id)
              if @prioridades_usuario.present? && !@prioridades_usuario.nil?
                @prioridades_usuario.each do |prio|
                  if query == ""
                    query = query + "IdGrupo = #{prio.IdGrupo}"
                  else
                    query = query + " or IdPerfil = #{prio.IdPerfil}"
                  end
                end
              end

              # Asignación de alerta de acuerdo a atencion y a prioridad
              if query != ""
                @alerta_Tran = Tbitacora.where(:IdEstado => 1).where(:canceladas => 0).where(:IdTipoReg => 3).where(:IdProducto => 2).where(:IdUsuario => 'N/A').where(query).last
                if !@alerta_Tran.present? && @alerta_Tran.nil?
                  @alerta_Tran = Tbitacora.where(:IdEstado => 1).where(:canceladas => 0).where(:IdTipoReg => 3).where(:IdProducto => 2).where(:IdUsuario => 'N/A').last
                end
              else
                @alerta_Tran = Tbitacora.where(:IdEstado => 1).where(:canceladas => 0).where(:IdTipoReg => 3).where(:IdProducto => 2).where(:IdUsuario => 'N/A').last
              end

              #Agregar el registro de que el usuario está atendiendo una cuenta
              @atendiendo = Tatendiendo.new
              @atendiendo.IdUsuario = current_user.id
              @atendiendo.IdTran = @alerta_Tran.IdTran
              @atendiendo.save

              @nuevo = true
            end

            @IdAlerta = @alerta_Tran.Id
            @alerta_Tran.IdUsuario = current_user.id
            @alerta_Tran.save

            @IdTran = @alerta_Tran.IdTran

            @ConsultaHistTbit = Tbitacora.where(:IdTran => @IdTran).where.not(:Id => @IdAlerta).where(:IdProducto => 2).order("Fecha Desc").order("Hora Desc").page(params[:page]).per(10)
            @ConsultaHistTbit_xls = Tbitacora.where(:IdTran => @IdTran).where.not(:Id => @IdAlerta).where(:IdProducto => 2).order("Fecha Desc").order("Hora Desc")

            #Se inserta referencia en base y se almacena columnas para TTs
            base_utilizada = ActiveRecord::Base.connection.adapter_name.downcase

            base_utilizada = base_utilizada.to_sym

            case base_utilizada
              when :mysql
                sql_exist = 'SELECT * FROM %{reference_table} WHERE idAlerta = %{idAlerta} LIMIT 1'
                sql_insert = 'INSERT INTO %{reference_table} (idAlerta, idCampo, idProducto, campo, valor, created_at, updated_at) VALUES (%{idAlerta}, %{idCampo}, %{idProducto}, %{campo}, %{valor}, %{created_at}, %{updated_at})'
              when :mysql2
                sql_exist = 'SELECT * FROM %{reference_table} WHERE idAlerta = %{idAlerta} LIMIT 1'
                sql_insert = 'INSERT INTO %{reference_table} (idAlerta, idCampo, idProducto, campo, valor, created_at, updated_at) VALUES (%{idAlerta}, %{idCampo}, %{idProducto}, %{campo}, %{valor}, %{created_at}, %{updated_at})'
              when :sqlite
                sql_exist = 'SELECT * FROM %{reference_table} WHERE idAlerta = %{idAlerta} LIMIT 1'
                sql_insert = 'INSERT INTO %{reference_table} (idAlerta, idCampo, idProducto, campo, valor, created_at, updated_at) VALUES (%{idAlerta}, %{idCampo}, %{idProducto}, %{campo}, %{valor}, %{created_at}, %{updated_at})'
              when :sqlserver
                sql_exist = 'SELECT TOP(1) * FROM %{reference_table} WHERE idAlerta = %{idAlerta}'
                sql_insert = 'INSERT INTO [%{reference_table}] ([idAlerta], [idCampo], [idProducto], [campo], [valor], [created_at], [updated_at]) VALUES (%{idAlerta}, %{idCampo}, %{idProducto}, %{campo}, %{valor}, %{created_at}, %{updated_at})'
            end

            reference_table = 'referenciadealerta'
            @cols_tt = ""

            sql_exist_q = sql_exist % {reference_table: reference_table, idAlerta: @IdAlerta}
            # puts (sql_exist_q)
            Timeout::timeout(0) {@alerta_existente = ActiveRecord::Base::connection.exec_query(sql_exist_q)}

            if @alerta_existente.length == 0 # No existe la referencia de la alerta
              @camposref = @alerta_Tran.Referencia.split(">")

              @camposref.each do |ref|
                @encabezado = ref.split("=").first
                @idCampo = @encabezado[1..3]
                @campo = "'#{@encabezado.from(5)}'"
                @campott = @encabezado.from(5)
                @valor = "'#{ref.split("=").second}'"
                @createDate = "'#{Time.now.to_datetime.strftime("%F %T")}'"

                #Insertar registro
                sql_insert_q = sql_insert % {reference_table: reference_table, idAlerta: @IdAlerta, idCampo: @idCampo, idProducto: 1, campo: "#{@campo}", valor: "#{@valor}", created_at: "#{@createDate}", updated_at: "#{@createDate}"}
                # puts(sql_insert_q)
                Timeout::timeout(0) {@inserted = ActiveRecord::Base::connection.exec_query(sql_insert_q)}

                if @cols_tt == ""
                  @cols_tt = @cols_tt + @campott
                else
                  @cols_tt = @cols_tt + ", " + @campott
                end
              end
            else
              @cols_tt = ""

              @campos_ref = Referenciadealerta.select(:campo).where(:idAlerta => @IdAlerta).group(:campo)

              @campos_ref.each do |cr|
                @campott = cr.campo.remove("'")
                if @cols_tt == ""
                  @cols_tt = @cols_tt + @campott
                else
                  @cols_tt = @cols_tt + ", " + @campott
                end
              end
            end
            # ---------- Observaciones

            # Verificar que aún sea el usuario quien atiende la alerta y alguien más no la liberó

            @atn_usr = Tatendiendo.find_by(:IdUsuario => current_user.id, :IdTran => @alerta_Tran.IdTran)

            if @atn_usr.present? && !@atn_usr.nil?
              if params[:observaciones]

                @ne = Tbitaobse.new
                @ne.IdAlerta = @IdAlerta
                @ne.Fecha = Time.now.to_date
                tiempo = Time.now
                @tiempo = tiempo.strftime("%I:%M:%S")
                @ne.Hora = @tiempo
                @ne.IdEstado = params[:estado]
                @ne.IdUsuarioAtencion = current_user.id
                @ne.Observaciones = params[:observaciones]
                @ne.falso_positivo = params[:fraud]
                @ne.save

                @estado_ant = @alerta_Tran.IdEstado

                @alerta_Tran.IdEstado = params[:estado]
                @alerta_Tran.FalsoPositivo = params[:fraud]
                @alerta_Tran.save

                @estado_act = @alerta_Tran.IdEstado

                if (@estado_ant == 1 || @estado_ant == "1") && (@estado_act != 1 || @estado_act != "1")
                  @atendidasxhoy = Tatendidasxhoy.find_by(:idProducto => 2, :fecha => Time.now.strftime("%Y-%m-%d"))

                  if @atendidasxhoy.present? && !@atendidasxhoy.nil?
                    @cantidad = @atendidasxhoy.cantidad
                    @atendidasxhoy.cantidad = @cantidad + 1
                    @atendidasxhoy.save
                  else
                    @nueva_atendida = Tatendidasxhoy.new
                    @nueva_atendida.fecha = Time.now.strftime("%Y-%m-%d")
                    @nueva_atendida.idProducto = 2
                    @nueva_atendida.cantidad = 1
                    @nueva_atendida.save
                  end

                end

                @apply = params[:apply]

                if @apply == "true"
                  @historico_alert = params[:historico]

                  if !@historico_alert.nil? && @historico_alert != ""
                    historico_al = @historico_alert.split(',')

                    loop do

                      @ne = Tbitaobse.new
                      @ne.IdAlerta = historico_al[0]
                      @ne.Fecha = Time.now.to_date
                      tiempo = Time.now
                      @tiempo = tiempo.strftime("%I:%M:%S")
                      @ne.Hora = @tiempo
                      @ne.IdEstado = params[:estado]
                      @ne.IdUsuarioAtencion = current_user.id
                      @ne.Observaciones = params[:observaciones]
                      @ne.falso_positivo = params[:fraud]
                      @ne.save

                      @alerta_observacion = Tbitacora.find(historico_al[0])

                      @estado_ant = @alerta_observacion.IdEstado
                      if @alerta_observacion
                        @alerta_observacion.IdEstado = params[:estado]
                        @alerta_observacion.IdUsuario = current_user.id
                        @alerta_observacion.FalsoPositivo = params[:fraud]
                        @alerta_observacion.save
                      end

                      @estado_act = @alerta_observacion.IdEstado

                      if (@estado_ant == 1 || @estado_ant == "1") && (@estado_act != 1 || @estado_act != "1")
                        @atendidasxhoy = Tatendidasxhoy.find_by(:idProducto => 2, :fecha => Time.now.strftime("%Y-%m-%d"))

                        if @atendidasxhoy.present? && !@atendidasxhoy.nil?
                          @cantidad = @atendidasxhoy.cantidad
                          @atendidasxhoy.cantidad = @cantidad + 1
                          @atendidasxhoy.save
                        else
                          @nueva_atendida = Tatendidasxhoy.new
                          @nueva_atendida.fecha = Time.now.strftime("%Y-%m-%d")
                          @nueva_atendida.idProducto = 2
                          @nueva_atendida.cantidad = 1
                          @nueva_atendida.save
                        end

                      end

                      historico_al.delete_at(0)
                      break if historico_al.length == 0
                    end
                  end
                end

              end
            end

            # ----------  Observaciones

            # ----------  Historial transaccional

            @fecha_consulta = 10.days.ago
            tts = Array.new

            while (@fecha_consulta <= Time.now)
              valor = {tt: ''}
              valor[:tt] = 'TT' + @fecha_consulta.strftime('%Y%m%d') + '0101'

              tts.push(valor)
              @fecha_consulta = @fecha_consulta + 1.day
            end

            transacciones = Array.new
            loop do
              if (ActiveRecord::Base.connection.table_exists? tts[0][:tt].to_s)
                # @consulta = ActiveRecord::Base::connection.exec_query("Select #{@cols_tt} from #{tts[0][:tt].to_s} where Cuenta_CI = #{@IdTran}")
                # puts "Select #{@cols_tt} from #{tts[0][:tt].to_s} where Cuenta_CI = #{@IdTran}"
                @cols_tt = @cols_tt.remove("'")
                puts "************************** #{@cols_tt}"
                @consulta = ActiveRecord::Base::connection.exec_query("Select #{@cols_tt} from #{tts[0][:tt].to_s} where #{t('config.main')} = '#{@IdTran}'")

                puts "Select #{@cols_tt} from #{tts[0][:tt].to_s} where #{t('config.main')} = '#{@IdTran}'"

                @cols_ref = Referenciadealerta.select(:campo).where(:idAlerta => @IdAlerta).group(:campo, :idCampo).order(:idCampo)

                if @consulta.present? && !@consulta.nil?
                  @consulta.each do |con|
                    registro = {}
                    @cols_ref.each do |cr|
                      @campor = cr.campo.remove("'")
                      registro[@campor] = con[@campor]
                    end
                    transacciones.push(registro)
                  end
                end
              end
              tts.delete_at(0)
              break if tts.length == 0
            end

            @transacciones = transacciones
            @@transacciones = transacciones
            @states = Testadosxalerta.all.where.not(:IdEstado => '1')

            # ----------  Historial transaccional

            # ----------  Generación de excel
            if params[:xls_g]
              Thread.new do
                puts 'comienza hilo'
                create_xls = CreateXls.new
                create_xls.setInfoAlertXls(params[:id], params[:user_id], params[:idioma], @observaciones_xls, @ConsultaHistTbit_xls, @transacciones)
                create_xls.alertXls
              end
            end
            # ----------  Generación de excel
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_enabled')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  def histTran
    @nuevo_hist = true

    if params[:rangos].present?
      mail_groups = MailGroup.all

      gon.emails = mail_groups
      gon.false = true
      @nuevo_hist = false
      @exito = true
      if @exito.present?
        @IdAlerta = params[:idTran]
        @al = Tbitacora.find_by_Id(@IdAlerta.to_i)
        @cols_tt = ""

        @campos_ref = Referenciadealerta.select(:campo).where(:idAlerta => @IdAlerta).group(:campo)

        @campos_ref.each do |cr|
          @campott = cr.campo.remove("'")
          if @cols_tt == ""
            @cols_tt = @cols_tt + @campott
          else
            @cols_tt = @cols_tt + ", " + @campott
          end
        end

        @fecha_consulta = params[:FechaIni]
        @fecha_consulta_fin = params[:FechaFin]

        @fecha_consulta = @fecha_consulta.to_date
        @fecha_consulta_fin = @fecha_consulta_fin.to_date
        tts = Array.new

        while (@fecha_consulta <= @fecha_consulta_fin)
          valor = {tt: ''}
          valor[:tt] = 'TT' + @fecha_consulta.strftime('%Y%m%d') + '0101'

          tts.push(valor)
          @fecha_consulta = @fecha_consulta + 1.day
        end

        puts 'TT a consultar: ' + tts.to_s

        transacciones = Array.new
        loop do
          if (ActiveRecord::Base.connection.data_source_exists? tts[0][:tt].to_s)
            # @consulta = ActiveRecord::Base::connection.exec_query("Select #{@cols_tt} from #{tts[0][:tt].to_s} where Cuenta_CI = #{@al.IdTran}")
            # puts "Select #{@cols_tt} from #{tts[0][:tt].to_s} where Cuenta_CI = #{@al.IdTran}"
            @cols_tt = @cols_tt.remove("'")
            puts "************************** #{@cols_tt}"
            @consulta = ActiveRecord::Base::connection.exec_query("Select #{@cols_tt}, ID_TRAN, FECHA_HORA_KM from #{tts[0][:tt].to_s} where #{t('config.main')} = '#{@al.IdTran}'")

            puts "Select #{@cols_tt}, ID_TRAN from #{tts[0][:tt].to_s} where #{t('config.main')} = '#{@al.IdTran}'"

            @cols_ref = Referenciadealerta.select(:campo).where(:idAlerta => @IdAlerta).group(:campo, :idCampo).order(:idCampo)

            tt = tts[0][:tt].to_s.remove('TT').chomp('0101').to_date.strftime('%Y-%m-%d')

            if @consulta.present? && !@consulta.nil?

              @consulta.each do |con|
                registro = {}
                registro[:fecha] = tt

                @cols_ref.each do |cr|
                  @campor = cr.campo.remove("'")
                  registro[@campor] = con[@campor]
                end

                registro[:id_tran] = con['ID_TRAN'].to_i
                registro[:tt] = tts[0][:tt].to_s
                transacciones.push(registro)
              end
            end
          end

          tts.delete_at(0)
          break if tts.length == 0
        end

        if transacciones.length > 0
          @transacciones = transacciones
          puts 'Transacciones encontradas: ' + transacciones.length.to_s
        else
          @transacciones = nil
          puts 'Transacciones encontradas: 0'
        end


        # @transacciones = [{:Ln_Tarj =>"PRO1", :Numero_de_Tarjeta=>"1000001754085501   ", :Ln_Comer=>"PRO1", :Fiid_Comer=>"LIVP", :Region=>"0000", :ID_Comer=>"126258735          ", :Fiid_Term=>"LIVP", :Term_ID_Term=>"L3060045        ", :Tipo_Tarjeta=>"C0", :Codigo_Respuesta =>"000", :Monto1=> 5165165, :Monto2=> 51513521}]
      end
    end


    render :partial => "alertn/histTran"
  end

  def transacciones_alerta
    if params[:fraud_tt].present? || params[:pending_tt].present?
      puts 'entre a agregar transacciones de alerta'
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_alertn
    @alertn = Alertn.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def alertn_params
    params.fetch(:alertn, {})
  end
end
