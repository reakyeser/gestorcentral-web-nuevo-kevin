class HomeController < ApplicationController

  @@byHome = "Home"
  respond_to :html, :js

  def index
    if current_user

      # @id_usuario = current_user.id
      # @id_aplicacion = 25
      # @idTipoReg = 11 #Index
      # @fecha = Time.now.strftime("%F")
      # @hora = Time.now.strftime("%k:%M:%S.00000")
      # @men = "Home Index"
      #
      #
      # @tbit = Tbitacora.new
      # @tbit.IdUsuario = @id_usuario
      # @tbit.IdAplicacion = @id_aplicacion
      # @tbit.IdTipoReg = @idTipoReg #Index
      # @tbit.Fecha = @fecha
      # @tbit.Hora  = @hora
      # @tbit.Mensaje = @men
      # @tbit.save

      @cu = current_user.profile_id

      if @cu != 0
        @per = Permission.where("view_name = 'Home' and profile_id = ?", @cu)

        @per.each do |permisos|
          @uno = permisos.view_name
          @crearView = permisos.crear
          @editarView = permisos.editar
          @leerView = permisos.leer
          @eliminarView = permisos.eliminar

          if permisos.view_name == @@byHome

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        @per_n_tran = Permission.where("view_name = 'NewAlertTransaccional' and profile_id = ?", @cu)

        @per_n_tran.each do |permisos|
          @leerNewTran = permisos.leer
        end

        @per_n_per = Permission.where("view_name = 'NewAlertPerfiles' and profile_id = ?", @cu)

        @per_n_per.each do |permisos|
          @leerNewPer = permisos.leer
        end

        @per_c_tran = Permission.where("view_name = 'ConsultAlertTransaccional' and profile_id = ?", @cu)

        @per_c_tran.each do |permisos|
          @leerConTran = permisos.leer
        end

        @per_c_per = Permission.where("view_name = 'ConsultAlertPerfiles' and profile_id = ?", @cu)

        @per_c_per.each do |permisos|
          @leerConPer = permisos.leer
        end


        if ((@@leer == 2))
          # Pruebas
          @countTrans = Tbitacora.where(:canceladas => 0).where(:IdTipoReg => 3).where(:IdProducto => 1).count
          @countPerf = Tbitacora.where(:canceladas => 0).where(:IdTipoReg => 3).where(:IdProducto => 2).count

          @Trans = Tbitacora.where(:canceladas => 0).where(:IdTipoReg => 3).where(:IdProducto => 1).count
          @Perf = Tbitacora.where(:canceladas => 0).where(:IdTipoReg => 3).where(:IdProducto => 2).count

          @@countTrans = @countTrans
          @@countPerf = @countPerf

          @todas = @countTrans.to_i + @countPerf.to_i
        else
          @sin_permisos = true
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end

    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end

  end

  def mensajeAlerta

    # @hora_ant = Time.now - 30
    # @fecha = @hora_ant.strftime("%Y-%m-%d")
    # @hora_ant = @hora_ant.strftime("%k:%M:%S.00000")
    #
    # @hora_act = Time.now
    # @hora_act = @hora_act.strftime("%k:%M:%S.00000")
    #
    # @transAnt = Tbitacora.where(:IdTipoReg => 3).where(:IdProducto => 1).where("Hora <= ?", @hora_ant).where(:fecha => @fecha).count
    # @transAct = Tbitacora.where(:IdTipoReg => 3).where(:IdProducto => 1).where("Hora <= ?", @hora_act).where(:fecha => @fecha).count
    #
    # @perAnt = Tbitacora.where(:IdTipoReg => 3).where(:IdProducto => 2).where("Hora <= ?", @hora_ant).where(:fecha => @fecha).count
    # @perAct = Tbitacora.where(:IdTipoReg => 3).where(:IdProducto => 2).where("Hora <= ?", @hora_act).where(:fecha => @fecha).count
    #
    # if @transAct > @transAnt
    #
    #   @mensajeTran = true
    #   @producto = 1
    #
    #   @cantAlertTran = @transAct - @transAnt
    #   @cantAlertTran = @cantAlertTran.to_s
    # else
    #   @mensaje = false
    # end
    #
    # if @perAct > @perAnt
    #
    #   @mensajePer = true
    #   @producto = 2
    #
    #   @cantAlertPer = @perAct - @perAnt
    #   @cantAlertPer = @cantAlertPer.to_s
    # else
    #   @mensaje = false
    # end
    #
    render :partial => "mensajeAlerta"

  end

  def contadorSuperior

    @countTrans = Tbitacora.where(:IdEstado => 1).where(:canceladas => 0).where(:IdTipoReg => 3).where(:IdProducto => 1).count
    @countPerf = Tbitacora.where(:IdEstado => 1).where(:canceladas => 0).where(:IdTipoReg => 3).where(:IdProducto => 2).count

    @todas = @countTrans.to_i + @countPerf.to_i

    render :partial => "contadorSuperior"

  end

  def totalPoratender

    @porAtender = Tbitacora.where(:IdEstado => 1).where(:canceladas => 0).where(:IdTipoReg => 3).where(:IdUsuario => "N/A").count

    render :partial => "totalPoratender"
  end

  def totalAtendidas

    @atendidas = Tbitacora.where(:canceladas => 0).where(:IdTipoReg => 3).where.not(:IdUsuario => "N/A").count

    render :partial => "totalAtendidas"
  end

  def totalAlerts
    @countTrans = Tbitacora.where(:IdEstado => 1).where(:canceladas => 0).where(:IdTipoReg => 3).where(:IdProducto => 1).count
    @countPerf = Tbitacora.where(:IdEstado => 1).where(:canceladas => 0).where(:IdTipoReg => 3).where(:IdProducto => 2).count

    @Trans = Tbitacora.where(:canceladas => 0).where(:IdTipoReg => 3).where(:IdProducto => 1).count
    @Perf = Tbitacora.where(:canceladas => 0).where(:IdTipoReg => 3).where(:IdProducto => 2).count
    render :partial => "totalAlerts"

  end

  def alertaTransaccional

    render :partial => "alertaTransaccional"
  end

  def alertaPerfiles

    render :partial => "alertaPerfiles"
  end

  def pie1
    @cu = current_user.profile_id
    @per = Permission.where("view_name = 'Home' and profile_id = ?", @cu)

    @per.each do |permisos|
      @uno = permisos.view_name
      @crearView = permisos.crear
      @editarView = permisos.editar
      @leerView = permisos.leer
      @eliminarView = permisos.eliminar

      if permisos.view_name == @@byHome

        @@crear = permisos.crear
        @@editar = permisos.editar
        @@leer = permisos.leer
        @@eliminar = permisos.eliminar

        @crear = permisos.crear
        @editar = permisos.editar
        @leer = permisos.leer
        @eliminar = permisos.eliminar
      end

    end

    @per_n_tran = Permission.where("view_name = 'NewAlertTransaccional' and profile_id = ?", @cu)

    @per_n_tran.each do |permisos|
      @leerNewTran = permisos.leer
    end

    @per_n_per = Permission.where("view_name = 'NewAlertPerfiles' and profile_id = ?", @cu)

    @per_n_per.each do |permisos|
      @leerNewPer = permisos.leer
    end

    @per_c_tran = Permission.where("view_name = 'ConsultAlertTransaccional' and profile_id = ?", @cu)

    @per_c_tran.each do |permisos|
      @leerConTran = permisos.leer
    end

    @per_c_per = Permission.where("view_name = 'ConsultAlertPerfiles' and profile_id = ?", @cu)

    @per_c_per.each do |permisos|
      @leerConPer = permisos.leer
    end

    @countTrans = Tbitacora.where(:IdEstado => 1).where(:canceladas => 0).where(:IdTipoReg => 3).where(:IdProducto => 1).count
    @countPerf = Tbitacora.where(:IdEstado => 1).where(:canceladas => 0).where(:IdTipoReg => 3).where(:IdProducto => 2).count

    render :partial => "pie1"
  end

  def pie2
    @cu = current_user.profile_id
    @per = Permission.where("view_name = 'Home' and profile_id = ?", @cu)

    @per.each do |permisos|
      @uno = permisos.view_name
      @crearView = permisos.crear
      @editarView = permisos.editar
      @leerView = permisos.leer
      @eliminarView = permisos.eliminar

      if permisos.view_name == @@byHome

        @@crear = permisos.crear
        @@editar = permisos.editar
        @@leer = permisos.leer
        @@eliminar = permisos.eliminar

        @crear = permisos.crear
        @editar = permisos.editar
        @leer = permisos.leer
        @eliminar = permisos.eliminar
      end

    end

    @per_n_tran = Permission.where("view_name = 'NewAlertTransaccional' and profile_id = ?", @cu)

    @per_n_tran.each do |permisos|
      @leerNewTran = permisos.leer
    end

    @per_n_per = Permission.where("view_name = 'NewAlertPerfiles' and profile_id = ?", @cu)

    @per_n_per.each do |permisos|
      @leerNewPer = permisos.leer
    end

    @per_c_tran = Permission.where("view_name = 'ConsultAlertTransaccional' and profile_id = ?", @cu)

    @per_c_tran.each do |permisos|
      @leerConTran = permisos.leer
    end

    @per_c_per = Permission.where("view_name = 'ConsultAlertPerfiles' and profile_id = ?", @cu)

    @per_c_per.each do |permisos|
      @leerConPer = permisos.leer
    end

    render :partial => "pie2"

  end

  def pie3
    @cu = current_user.profile_id
    @per = Permission.where("view_name = 'Home' and profile_id = ?", @cu)

    @per.each do |permisos|
      @uno = permisos.view_name
      @crearView = permisos.crear
      @editarView = permisos.editar
      @leerView = permisos.leer
      @eliminarView = permisos.eliminar

      if permisos.view_name == @@byHome

        @@crear = permisos.crear
        @@editar = permisos.editar
        @@leer = permisos.leer
        @@eliminar = permisos.eliminar

        @crear = permisos.crear
        @editar = permisos.editar
        @leer = permisos.leer
        @eliminar = permisos.eliminar
      end

    end

    @per_n_tran = Permission.where("view_name = 'NewAlertTransaccional' and profile_id = ?", @cu)

    @per_n_tran.each do |permisos|
      @leerNewTran = permisos.leer
    end

    @per_n_per = Permission.where("view_name = 'NewAlertPerfiles' and profile_id = ?", @cu)

    @per_n_per.each do |permisos|
      @leerNewPer = permisos.leer
    end

    @per_c_tran = Permission.where("view_name = 'ConsultAlertTransaccional' and profile_id = ?", @cu)

    @per_c_tran.each do |permisos|
      @leerConTran = permisos.leer
    end

    @per_c_per = Permission.where("view_name = 'ConsultAlertPerfiles' and profile_id = ?", @cu)

    @per_c_per.each do |permisos|
      @leerConPer = permisos.leer
    end

    render :partial => "pie3"

  end

  def alertaBoton
    render :partial => "alertaBoton"
  end
end