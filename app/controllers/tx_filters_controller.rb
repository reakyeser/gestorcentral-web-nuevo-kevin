class TxFiltersController < ApplicationController
  before_action :set_tx_filters, only: [:show, :edit, :update, :destroy]

  # GET /tx_filters
  # GET /tx_filters.json
  def index
    @tx_filters = TxFilters.all
  end

  # GET /tx_filters/1
  # GET /tx_filters/1.json
  def show
  end

  # GET /tx_filters/new
  def new
    @tx_filter = TxFilters.new

    tFormatos = Tformatos.all
    gon.formatos = tFormatos

    tCampos = Tcamposformato.all
    gon.tCamposFormatos = tCampos

    tLays = Tcamposlay.all
    gon.tCamposLays = tLays
    gon.false = true

    @@idUser = current_user.id
  end

  # GET /tx_filters/1/edit
  def edit

    # Gon para traes los campos del formato y layout correspondiente
    @tx_sessions = TxSessions.all
    tx_sessions = TxSessions.all
    gon.sesiones = tx_sessions

    ## Gon para llenar selects Layout, Formato de ventana modal

    tCampos = Tcamposformato.all
    gon.tCamposFormatos = tCampos

    tLays = Tcamposlay.all
    gon.tCamposLays = tLays
    gon.false = true

  end

  # POST /tx_filters
  # POST /tx_filters.json
  def create

    @tx_filter = TxFilter.create!(tx_filters_params)
    @tx_filter.save

    @detalleFiltro = params[:condiFil].split(',')

    #-- Creando detalle de filtro
    @cont = 0
    @numCondi = 0
    @crearCondicion = false

    @conector
    @negado
    @operador
    @parentesis
    @valor
    @campoFormatoId
    @posision
    @length

    @detalleFiltro.each do | datoFiltro |
      @cont += 1

      if ( @cont === 1 )
        @conector = datoFiltro
      elsif ( @cont === 2 )
        @negado = datoFiltro
      elsif ( @cont === 3 )
        @operador = datoFiltro
      elsif ( @cont === 4 )
        @parentesis = datoFiltro
      elsif ( @cont === 5 )
        @valor = datoFiltro
      elsif ( @cont === 6 )
        @campoFormatoId = datoFiltro
      elsif ( @cont === 7 )
        @posision = datoFiltro
      elsif ( @cont === 8 )
        @length = datoFiltro
        @cont = 0
        @crearCondicion = true
      end

      if ( @crearCondicion  === true )
        @crearCondicion = false
        @numCondi += 1
        # @tx_filter_detail = TxFilterDetail.new(tx_filter_detail_params) marco error enviandolo desde el metodo new de session
        @tx_filter_detail = TxFilterDetail.new
        @tx_filter_detail.filter_id = @tx_filter.id
        @tx_filter_detail.conditionnumber = @numCondi
        @tx_filter_detail.connector = @conector
        @tx_filter_detail.denied = @negado
        @tx_filter_detail.operator = @operador
        @tx_filter_detail.parenthesis = @parentesis
        @tx_filter_detail.value = @valor
        @tx_filter_detail.fieldFormat_id = @campoFormatoId
        @tx_filter_detail.position = @posision
        @tx_filter_detail.lenght = @length
        @tx_filter_detail.key_id = @@idUser
        @tx_filter_detail.save
      end
    end

    #//-- Horario y Mensaje
    @hr_start = params[:Hr_Start].split(':')
    @hrS1 = @hr_start[0]
    @hrS2 = @hr_start[1]

    @hr_end = params[:Hr_End].split(':')
    @hrE1 = @hr_end[0]
    @hrE2 = @hr_end[1]


    @hora_start = "#{@hrS1}#{@hrS2}"
    @hora_end = "#{@hrE1}#{@hrE2}"

    selectLimite = params[:select_limit]
    @limit = params[:limit]
    @Interval = params[:Interval]
    @UMInterval = params[:UMInterval]
    @TimeOut = params[:TimeOut]
    @UMTimeout = params[:UMTimeout]

    #//--- Solo se utiliza si se tiene más de un mensaje
    @Intermediate = params[:Intermediate]
    @UMIntermediate = params[:UMIntermediate]

    #//-- Evalua el limite que eligio en el select el usuario
    if  selectLimite == '1'
      @max_tran = @limit
      @min_tran = 0
    else selectLimite == '2'
      @max_tran = 0
      @min_tran = @limit
    end


    #//-- Si los inputs vienen vacios
    if @TimeOut == nil
      @TimeOut = 0
    end

    if @Intermediate == nil
      @Intermediate = 0
    end

    @tx_filter_schedules = TxFilterSchedule.new
    @tx_filter_schedules.filter_id = @tx_filter.id
    @tx_filter_schedules.Hr_Start = @hora_start
    @tx_filter_schedules.Hr_End = @hora_end
    @tx_filter_schedules.Max_Tran = @max_tran
    @tx_filter_schedules.Min_Tran = @min_tran
    @tx_filter_schedules.Interval = @Interval
    @tx_filter_schedules.UMInterval = @UMInterval
    @tx_filter_schedules.Intermediate = @Intermediate
    @tx_filter_schedules.UMIntermediate = @UMIntermediate
    @tx_filter_schedules.TimeOut = @TimeOut
    @tx_filter_schedules.UMTimeout = @UMTimeout
    @tx_filter_schedules.save


    #//-- Creacion del mensaje
    if params[:msn].present?
      params[:msn].each do |key,obj|
        @tx_schedule_message = TxScheduleMessage.new(obj.permit(:Num_Alarma, :message, :bit_date, :bit_description, :bit_escala, :bit_limit, :bit_reference))
        @tx_schedule_message.schedule_id = @tx_filter_schedules.id
        @tx_schedule_message.save
      end
    end

    respond_to do |f|
      f.html { redirect_to tx_sessions_index_path, notice: 'El filtro fue creado exitosamente.' }
      f.js
    end

  end

  # PATCH/PUT /tx_filters/1
  # PATCH/PUT /tx_filters/1.json
  def update

    @tx_filter.update(tx_filters_params)


    # respond_to do |format|
    #   if @tx_filter.update(tx_filters_params)
    #     format.html { redirect_to action: :edit, notice: 'Tx filter was successfully updated.' }
    #     format.json { render :show, status: :ok, location: @tx_filter }
    #   else
    #     format.html { render :edit }
    #     format.json { render json: @tx_filter.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # DELETE /tx_filters/1
  # DELETE /tx_filters/1.json
  def destroy


    @tx_filter.destroy
    respond_to do |format|
      format.html { redirect_to tx_sessions_index_path, notice: 'El filtro fue eliminado exitosamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tx_filters
      @tx_filter = TxFilters.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tx_filters_params
      params.require(:tx_filters).permit(:Session_id, :Group_id, :Description, :Backgroundcolor, :Lettercolor, :Order_number, :Key_id)
    end
end
