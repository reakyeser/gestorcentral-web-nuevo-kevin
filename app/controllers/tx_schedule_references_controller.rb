class TxScheduleRerefencesController < ApplicationRecord
  before_action :set_tx_schedule_reference, only: [:show, :edit, :update, :destroy]

  # POST /tx_schedule_references
  # POST /tx_schedule_references.json
  def create

  end

  # DELETE /tx_schedule_references/1
  # DELETE /tx_schedule_references/1.json
  def destroy
    @tx_schedule_reference.destroy
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_tx_schedule_reference
    @tx_schedule_reference = TxScheduleReference.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def tx_schedule_reference_params
    params.require(:tx_schedule_reference).permit(:id, :schedule_id, :fieldFormat_id, :Filter_id, :key_id)
  end
end