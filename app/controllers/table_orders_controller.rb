class TableOrdersController < ApplicationController
  before_action :set_table_order, only: [:show, :edit, :update, :destroy]

  # GET /table_orders
  # GET /table_orders.json
  def index
    @table_orders = TableOrder.all
  end

  # GET /table_orders/1
  # GET /table_orders/1.json
  def show
  end

  # GET /table_orders/new
  def new
    @table_order = TableOrder.new
  end

  # GET /table_orders/1/edit
  def edit
  end

  # POST /table_orders
  # POST /table_orders.json
  def create

    if params[:orden].present?
      orden = params[:orden]
      tabla = params[:table]

      orden2 = params[:ordenD]
      tabla2 = params[:table2]

      user_id = current_user.id

      total = params[:orden].count

      if TableOrder.find_by_user_id(user_id).present?
        elimino = TableOrder.where("user_id = #{user_id}")
        elimino.each do |todos|
          todos.destroy
        end

        orden.each do |numeros, valores|
          @table_order = TableOrder.new #(table_order_params)
          nums = numeros
          vals = valores
          @table_order.user_id = user_id
          @table_order.name_column = vals
          @table_order.order = nums
          @table_order.table_id = tabla
          @table_order.save
        end

        orden2.each do |num, val|
          @table_order = TableOrder.new #(table_order_params)
          nums = num
          vals = val
          @table_order.user_id = user_id
          @table_order.name_column = vals
          @table_order.order = nums
          @table_order.table_id = tabla2
          @table_order.save

        end

      else
        orden.each do |numeros, valores|
          @table_order = TableOrder.new #(table_order_params)
          nums = numeros
          vals = valores
          @table_order.user_id = user_id
          @table_order.name_column = vals
          @table_order.order = nums
          @table_order.table_id = tabla
          @table_order.save
        end

        orden2.each do |nume, valo|
          @table_order = TableOrder.new #(table_order_params)
          nums = nume
          vals = valo
          @table_order.user_id = user_id
          @table_order.name_column = vals
          @table_order.order = nums
          @table_order.table_id = tabla2
          @table_order.save
        end
      end
    end

    if @table_order.save
      ordenamiento = true
      data = {:message => ordenamiento}
      render :json => data, :success => true
    else
      ordenamiento = false
      data = {:message => orndenamiento}
      render :json => data, :success => false

    end


    # respond_to do |format|
    #   if @table_order.save
    #     format.html {redirect_to @table_order, notice: 'Table order was successfully created.'}
    #     format.json {render :index, status: :created, location: @table_order}
    #   else
    #     format.html {render :index}
    #     format.json {render json: @table_order.errors, status: :unprocessable_entity}
    #   end
    # end
  end

  # PATCH/PUT /table_orders/1
  # PATCH/PUT /table_orders/1.json
  def update
    respond_to do |format|
      if @table_order.update(table_order_params)
        format.html {redirect_to @table_order, notice: 'Table order was successfully updated.'}
        format.json {render :show, status: :ok, location: @table_order}
      else
        format.html {render :edit}
        format.json {render json: @table_order.errors, status: :unprocessable_entity}
      end
    end
  end

  # DELETE /table_orders/1
  # DELETE /table_orders/1.json
  def destroy
    @table_order.destroy
    respond_to do |format|
      format.html {redirect_to table_orders_url, notice: 'Table order was successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_table_order
    @table_order = TableOrder.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def table_order_params
    params.require(:table_order).permit(:user_id, :name_column, :order, :table_id)
  end
end
