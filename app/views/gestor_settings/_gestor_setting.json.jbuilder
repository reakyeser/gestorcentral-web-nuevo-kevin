json.extract! gestor_setting, :id, :blue_day, :blue_nigth, :yellow_day, :yellow_night, :red_day, :red_night, :created_at, :updated_at
json.url gestor_setting_url(gestor_setting, format: :json)
