json.extract! tx_filter_detail, :id, :created_at, :updated_at
json.url tx_filter_detail_url(tx_filter_detail, format: :json)
