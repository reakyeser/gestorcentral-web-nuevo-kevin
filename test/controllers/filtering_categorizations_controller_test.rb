require 'test_helper'

class FilteringCategorizationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @filtering_categorization = filtering_categorizations(:one)
  end

  test "should get index" do
    get filtering_categorizations_url
    assert_response :success
  end

  test "should get new" do
    get new_filtering_categorization_url
    assert_response :success
  end

  test "should create filtering_categorization" do
    assert_difference('FilteringCategorization.count') do
      post filtering_categorizations_url, params: { filtering_categorization: { name_categorization: @filtering_categorization.name_categorization } }
    end

    assert_redirected_to filtering_categorization_url(FilteringCategorization.last)
  end

  test "should show filtering_categorization" do
    get filtering_categorization_url(@filtering_categorization)
    assert_response :success
  end

  test "should get edit" do
    get edit_filtering_categorization_url(@filtering_categorization)
    assert_response :success
  end

  test "should update filtering_categorization" do
    patch filtering_categorization_url(@filtering_categorization), params: { filtering_categorization: { name_categorization: @filtering_categorization.name_categorization } }
    assert_redirected_to filtering_categorization_url(@filtering_categorization)
  end

  test "should destroy filtering_categorization" do
    assert_difference('FilteringCategorization.count', -1) do
      delete filtering_categorization_url(@filtering_categorization)
    end

    assert_redirected_to filtering_categorizations_url
  end
end
