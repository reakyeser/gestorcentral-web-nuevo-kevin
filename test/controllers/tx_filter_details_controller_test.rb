require 'test_helper'

class TxFilterDetailsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tx_filter_detail = tx_filter_details(:one)
  end

  test "should get index" do
    get tx_filter_details_url
    assert_response :success
  end

  test "should get new" do
    get new_tx_filter_detail_url
    assert_response :success
  end

  test "should create tx_filter_detail" do
    assert_difference('TxFilterDetail.count') do
      post tx_filter_details_url, params: { tx_filter_detail: {  } }
    end

    assert_redirected_to tx_filter_detail_url(TxFilterDetail.last)
  end

  test "should show tx_filter_detail" do
    get tx_filter_detail_url(@tx_filter_detail)
    assert_response :success
  end

  test "should get edit" do
    get edit_tx_filter_detail_url(@tx_filter_detail)
    assert_response :success
  end

  test "should update tx_filter_detail" do
    patch tx_filter_detail_url(@tx_filter_detail), params: { tx_filter_detail: {  } }
    assert_redirected_to tx_filter_detail_url(@tx_filter_detail)
  end

  test "should destroy tx_filter_detail" do
    assert_difference('TxFilterDetail.count', -1) do
      delete tx_filter_detail_url(@tx_filter_detail)
    end

    assert_redirected_to tx_filter_details_url
  end
end
