require 'test_helper'

class ChartControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get chart_index_url
    assert_response :success
  end

  test "should get _dynamic" do
    get chart__dynamic_url
    assert_response :success
  end

end
