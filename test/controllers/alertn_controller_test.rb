require 'test_helper'

class AlertnControllerTest < ActionDispatch::IntegrationTest
  setup do
    @alertn = alertns(:one)
  end

  test "should get index" do
    get alertns_url
    assert_response :success
  end

  test "should get new" do
    get new_alertn_url
    assert_response :success
  end

  test "should create alertn" do
    assert_difference('Alertn.count') do
      post alertns_url, params: { alertn: {  } }
    end

    assert_redirected_to alertn_url(Alertn.last)
  end

  test "should show alertn" do
    get alertn_url(@alertn)
    assert_response :success
  end

  test "should get edit" do
    get edit_alertn_url(@alertn)
    assert_response :success
  end

  test "should update alertn" do
    patch alertn_url(@alertn), params: { alertn: {  } }
    assert_redirected_to alertn_url(@alertn)
  end

  test "should destroy alertn" do
    assert_difference('Alertn.count', -1) do
      delete alertn_url(@alertn)
    end

    assert_redirected_to alertns_url
  end
end
