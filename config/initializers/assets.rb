# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
# Rails.application.config.assets.paths << Rails.root.join('node_modules')

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
# Rails.application.config.assets.precompile += %w( admin.js admin.css )

#Rails.application.config.assets.precompile += %w[base/variables.scss]
#Rails.application.config.assets.precompile += Ckeditor.assets
# Rails.application.config.assets.precompile += %w(ckeditor/*)
# Rails.application.config.assets.precompile += %w(plugin/flot/jquery.flot.cust.js)
# Rails.application.config.assets.precompile += %w(plugin/flot/jquery.flot.resize.js)
# Rails.application.config.assets.precompile += %w(plugin/flot/jquery.flot.tooltip.min.js)
# Rails.application.config.assets.precompile += %w( demo.js )
#
# Rails.application.config.assets.precompile += %w( plugin/pace/pace.js )
# Rails.application.config.assets.precompile += %w( plugin/chartjs/chart.js )
# Rails.application.config.assets.precompile += %w( metrics.js )

Rails.application.config.assets.precompile += %w( application.js )

Rails.application.config.assets.precompile += %w( style.css )
Rails.application.config.assets.precompile += %w( profiles.js )
Rails.application.config.assets.precompile += %w( profiles.css )
Rails.application.config.assets.precompile += %w( areas.js )
Rails.application.config.assets.precompile += %w( areas.css )
Rails.application.config.assets.precompile += %w( views.js )
Rails.application.config.assets.precompile += %w( views.css )
Rails.application.config.assets.precompile += %w( home.css )
Rails.application.config.assets.precompile += %w( alertc.js )
Rails.application.config.assets.precompile += %w( alertc.css )
Rails.application.config.assets.precompile += %w( rails.js )
Rails.application.config.assets.precompile += %w( niv_alertamiento.js )
Rails.application.config.assets.precompile += %w( niv_alertamiento.css )
Rails.application.config.assets.precompile += %w( con_gen.css )
Rails.application.config.assets.precompile += %w( con_gen.js )
Rails.application.config.assets.precompile += %w( alert_release.css )
Rails.application.config.assets.precompile += %w( alert_release.js )
Rails.application.config.assets.precompile += %w( user_priority.css )
Rails.application.config.assets.precompile += %w( user_priority.js )

Rails.application.config.assets.precompile += %w( chart.css )
Rails.application.config.assets.precompile += %w( chart.js )

Rails.application.config.assets.precompile += %w( alertn.js )
Rails.application.config.assets.precompile += %w( alertn.css )

Rails.application.config.assets.precompile += %w( orden.js )
Rails.application.config.assets.precompile += %w( shearch_colum.js )
Rails.application.config.assets.precompile += %w( table_orders.js )

Rails.application.config.assets.precompile += %w( filtering_categorizations.js )


Rails.application.config.assets.precompile += %w( jquery.js )
Rails.application.config.assets.precompile += %w( jquery-ui.js )

Rails.application.config.assets.precompile += %w( plugin/morris/morris.js )
Rails.application.config.assets.precompile += %w( plugin/morris/morris-chart-settings.js )
Rails.application.config.assets.precompile += %w( flot/jquery.flot.resize.js )
Rails.application.config.assets.precompile +=
    %w(plugin/flot/jquery.flot.cust.js)
Rails.application.config.assets.precompile +=
    %w(plugin/flot/jquery.flot.resize.js)
Rails.application.config.assets.precompile +=
    %w(plugin/flot/jquery.flot.tooltip.min.js)
Rails.application.config.assets.precompile += %w( plugin/chartjs/chart.js )
Rails.application.config.assets.precompile += %w( plugin/pace/pace.js )
Rails.application.config.assets.precompile += %w( jquery-ui.js )

Rails.application.config.assets.precompile += %w( cierre_session.js )
Rails.application.config.assets.precompile += %w( cierre_sesion_tran_per.js )
Rails.application.config.assets.precompile += %w( groups.css )
Rails.application.config.assets.precompile += %w( groups.js )
Rails.application.config.assets.precompile += %w( mail_groups.css )
Rails.application.config.assets.precompile += %w( mail_groups.js )
Rails.application.config.assets.precompile += %w( groups.css )
Rails.application.config.assets.precompile += %w( gestor_settings.css )
Rails.application.config.assets.precompile += %w( gestor_settings.js )

Rails.application.config.assets.precompile += %w( tx_filter_details.js )
Rails.application.config.assets.precompile += %w( tx_filter_details.css )

Rails.application.config.assets.precompile += %w( tx_sessions.js )
Rails.application.config.assets.precompile += %w( tx_sessions.scss )

Rails.application.config.assets.precompile += %w( tx_filters.js )
Rails.application.config.assets.precompile += %w( tx_filters.scss )

Rails.application.config.assets.precompile += %w( iCheck/icheck.min.js )


Rails.application.config.assets.precompile += %w(flot/jquery.flot.js )
Rails.application.config.assets.precompile += %w(flot/jquery.flot.tooltip.min.js )
Rails.application.config.assets.precompile += %w(flot/jquery.flot.resize.js )
Rails.application.config.assets.precompile += %w(flot/jquery.flot.pie.js )
Rails.application.config.assets.precompile += %w(flot/jquery.flot.time.js )
Rails.application.config.assets.precompile += %w(flot/jquery.flot.spline.js )
Rails.application.config.assets.precompile += %w(sparkline/jquery.sparkline.min.js )
Rails.application.config.assets.precompile += %w(chartjs/Chart.min.js )
Rails.application.config.assets.precompile += %w(morris/raphael-2.1.0.min.js )
Rails.application.config.assets.precompile += %w(morris/morris.js )
Rails.application.config.assets.precompile += %w(rickshaw/vendor/d3.v3.js )
Rails.application.config.assets.precompile += %w(rickshaw/rickshaw.min.js )
Rails.application.config.assets.precompile += %w(chartist/chartist.min.js )


Rails.application.config.assets.precompile += %w( transacction_type.js )
Rails.application.config.assets.precompile += %w( transacction_type.css )
Rails.application.config.assets.precompile += %w( datapicker/bootstrap-datepicker.js )

Rails.application.config.assets.precompile += %w( tx_filter_schedules.js )
Rails.application.config.assets.precompile += %w( tx_filter_schedules.css )
Rails.application.config.assets.precompile += %w( filtersnos.js )
Rails.application.config.assets.precompile += %w( white_lists.js )
Rails.application.config.assets.precompile += %w( filtersnos.scss )
Rails.application.config.assets.precompile += %w( white_lists.scss )