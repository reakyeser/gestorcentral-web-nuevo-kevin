Rails.application.routes.draw do
  resources :tx_schedule_references
  resources :tx_schedule_messages
  resources :tx_filter_schedules
  resources :white_lists
  resources :filtersnos

  resources :tx_filters
  resources :tx_filter_details
  get 'transacction_type/dynamic', to: 'transacction_type#dynamic'
  resources :tconfigs
  get 'filtering_categorizations/dynamic', to: 'filtering_categorizations#dynamic'
  # get 'tx_sessions/index'

  get 'transacction_type/index', to: 'transacction_type#index'
  resources :transacction_type #'transacction_type/index', to: 'transacction_type#index'
  resources :tx_sessions
  resources :filtering_categorizations
  resources :states, :path => 'testadosxalerta'
  resources :states
  resources :testadosxalerta
  resources :table_orders
  get 'alert_release/index'
  post 'users/expire', to: 'users#expire'

  get 'users/expire', to: 'users#expire'

  get 'user_priority/index'
  get 'user_priority/assignPriority'
  get 'user_priority/assignPriorityPer'
  get 'user_priority/sessionsTran'
  get 'user_priority/groupsPer'

  get 'niv_alertamiento/index'
  get 'con_gen/index'


  filter :locale

  get 'alertc/par4'
  get 'alertc/par3'
  get 'alertc/par2'
  get 'alertc/par1'
  get 'alertn/histTran'
  get 'alertn/transacciones_alerta'
  get 'home/alertaBoton'
  get 'home/pie3'
  get 'home/pie2'
  get 'home/pie1' #, to: 'home#pie1'
  get 'home/contadorSuperior' #, to: 'home#pie1'
  get 'home/totalAlerts'
  get 'home/alertaTransaccional'
  get 'home/alertaPerfiles'
  get 'home/mensajeAlerta'
  get 'home/totalPoratender'
  get 'home/totalAtendidas'
  get 'home/settings_index'

  get 'chart/empty'
  get 'chart/pies'
  get 'chart/dynamic'

  resources 'chart'


  get 'alertn/alertTran', to: 'alertn#alertTran'
  get 'tbitacora/tbitacora'
  get 'tbitaobse/tbitaobse'
  get 'alertn/alertPer', to: 'alertn#alertPer'
  get 'alertc/busqueda', to: 'alertc#busqueda'

  resources :groups

  resources :views
  resources :profiles
  resources :permissions
  resources :areas
  resources :home
  resources :users
  resources :alertn
  resources :alertc
  resources :con_gen
  resources :chart, only: [:index, :dynamic]
  resources :home, only: [:index, :create]
  resources :mail_groups
  resources :gestor_settings


  # get '/:locale' => 'home#index'

  # devise_for :users
  devise_for :users, :controllers => {:registrations => "user/registrations", :sessions => "user/sessions"}, path: 'auth', path_names: {sign_in: 'login', sign_out: 'logout', password: 'secret', confirmation: 'verification', unlock: 'unblock', registration: 'register', sign_up: 'cmon_let_me_in'}
  # # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  devise_scope :user do
    authenticated :user do
      root :to => 'home#index', as: :authenticated_root
    end
    unauthenticated :user do
      root :to => 'devise/sessions#new', as: :unauthenticated_root
      resources :expired_accounts
      get "expired_accounts/new"
    end
    get "auth/logout" => "devise/sessions#destroy"
  end
end
