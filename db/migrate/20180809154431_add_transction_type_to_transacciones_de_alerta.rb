class AddTransctionTypeToTransaccionesDeAlerta < ActiveRecord::Migration[5.0]
  def change
    add_column :transacciones_de_alerta, :transaction_type, :integer
  end
end
