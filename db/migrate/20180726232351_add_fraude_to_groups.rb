class AddFraudeToGroups < ActiveRecord::Migration[5.0]
  def change
    add_column :groups, :fraude, :boolean
  end
end
