# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

##################### VISTAS BASE ######################
vista = View.new
vista.name = "Home"
vista.crear = 0
vista.editar = 0
vista.eliminar = 0
vista.leer = 1
vista.save

vista = View.new
vista.name = "AlertingAndProductivityLevels"
vista.crear = 1
vista.editar = 0
vista.eliminar = 0
vista.leer = 1
vista.save

vista = View.new
vista.name = "FilterCategorizationGraph"
vista.crear = 0
vista.editar = 0
vista.eliminar = 0
vista.leer = 1
vista.save

vista = View.new
vista.name = "NewAlertTransaccional"
vista.crear = 1
vista.editar = 1
vista.eliminar = 0
vista.leer = 1
vista.save

vista = View.new
vista.name = "NewAlertPerfiles"
vista.crear = 1
vista.editar = 1
vista.eliminar = 0
vista.leer = 1
vista.save

vista = View.new
vista.name = "ConsultAlertTransaccional"
vista.crear = 1
vista.editar = 0
vista.eliminar = 0
vista.leer = 1
vista.save

vista = View.new
vista.name = "ConsultAlertPerfiles"
vista.crear = 1
vista.editar = 0
vista.eliminar = 0
vista.leer = 1
vista.save

vista = View.new
vista.name = "AlertRelease"
vista.crear = 0
vista.editar = 1
vista.eliminar = 0
vista.leer = 1
vista.save

vista = View.new
vista.name = "GeneralSearch"
vista.crear = 1
vista.editar = 0
vista.eliminar = 0
vista.leer = 1
vista.save

vista = View.new
vista.name = "Users"
vista.crear = 0
vista.editar = 1
vista.eliminar = 1
vista.leer = 1
vista.save

vista = View.new
vista.name = "Profiles"
vista.crear = 1
vista.editar = 1
vista.eliminar = 1
vista.leer = 1
vista.save

vista = View.new
vista.name = "Areas"
vista.crear = 1
vista.editar = 1
vista.eliminar = 1
vista.leer = 1
vista.save

vista = View.new
vista.name = "Sessions"
vista.crear = 1
vista.editar = 1
vista.eliminar = 1
vista.leer = 1
vista.save

vista = View.new
vista.name = "UserAttentionPriorityTransaccional"
vista.crear = 1
vista.editar = 1
vista.eliminar = 1
vista.leer = 1
vista.save

vista = View.new
vista.name = "UserAttentionPriorityPerfiles"
vista.crear = 1
vista.editar = 1
vista.eliminar = 1
vista.leer = 1
vista.save

vista = View.new
vista.name = "FilterCategorization"
vista.crear = 1
vista.editar = 1
vista.eliminar = 1
vista.leer = 1
vista.save

vista = View.new
vista.name = "States"
vista.crear = 1
vista.editar = 1
vista.eliminar = 1
vista.leer = 1
vista.save

vista = View.new
vista.name = "EmailGroups"
vista.crear = 1
vista.editar = 1
vista.eliminar = 1
vista.leer = 1
vista.save

vista = View.new
vista.name = "ScheduleConfiguration"
vista.crear = 1
vista.editar = 1
vista.eliminar = 0
vista.leer = 1
vista.save

vista = View.new
vista.name = "Views"
vista.crear = 1
vista.editar = 1
vista.eliminar = 1
vista.leer = 1
vista.save



##################### AREA CENTRAL ######################
area = Area.new
area.name = "Central"
area.save

##################### PERFIL ADMINISTRADOR CENTRAL ######################
perfil = Profile.new
perfil.name = 'Administrator Central'
perfil.area_id = 1
perfil.flag = 0
perfil.save

permiso = Permission.new
permiso.view_id = 1
permiso.view_name = View.find(1).name # Home
permiso.crear = nil
permiso.editar = nil
permiso.leer = 2
permiso.eliminar = nil
permiso.profile_id = 1
permiso.save

permiso = Permission.new
permiso.view_id = 2
permiso.view_name = View.find(2).name #Alert Levels
permiso.crear = 8
permiso.editar = nil
permiso.leer = 2
permiso.eliminar = nil
permiso.profile_id = 1
permiso.save

permiso = Permission.new
permiso.view_id = 3
permiso.view_name = View.find(3).name #Filter Categorization Graphs
permiso.crear = nil
permiso.editar = nil
permiso.leer = 2
permiso.eliminar = nil
permiso.profile_id = 1
permiso.save

permiso = Permission.new
permiso.view_id = 4
permiso.view_name = View.find(4).name #New Alert transaccional
permiso.crear = 8
permiso.editar = 4
permiso.leer = 2
permiso.eliminar = nil
permiso.profile_id = 1
permiso.save

permiso = Permission.new
permiso.view_id = 5
permiso.view_name = View.find(5).name #New Alert Perfiles
permiso.crear = 8
permiso.editar = 4
permiso.leer = 2
permiso.eliminar = nil
permiso.profile_id = 1
permiso.save

permiso = Permission.new
permiso.view_id = 6
permiso.view_name = View.find(6).name #Consult Alert transaccional
permiso.crear = 8
permiso.editar = nil
permiso.leer = 2
permiso.eliminar = nil
permiso.profile_id = 1
permiso.save

permiso = Permission.new
permiso.view_id = 7
permiso.view_name = View.find(7).name #Consult Alert perfiles
permiso.crear = 8
permiso.editar = nil
permiso.leer = 2
permiso.eliminar = nil
permiso.profile_id = 1
permiso.save

permiso = Permission.new
permiso.view_id = 8
permiso.view_name = View.find(8).name # Alert Release
permiso.crear = nil
permiso.editar = 4
permiso.leer = 2
permiso.eliminar = nil
permiso.profile_id = 1
permiso.save

permiso = Permission.new
permiso.view_id = 9
permiso.view_name = View.find(9).name #General Search
permiso.crear = 8
permiso.editar = nil
permiso.leer = 2
permiso.eliminar = nil
permiso.profile_id = 1
permiso.save

permiso = Permission.new
permiso.view_id = 10
permiso.view_name = View.find(10).name #Users
permiso.crear = nil
permiso.editar = 4
permiso.leer = 2
permiso.eliminar = 1
permiso.profile_id = 1
permiso.save

permiso = Permission.new
permiso.view_id = 11
permiso.view_name = View.find(11).name #Profiles
permiso.crear = 8
permiso.editar = 4
permiso.leer = 2
permiso.eliminar = 1
permiso.profile_id = 1
permiso.save

permiso = Permission.new
permiso.view_id = 12
permiso.view_name = View.find(12).name #Areas
permiso.crear = 8
permiso.editar = 4
permiso.leer = 2
permiso.eliminar = 1
permiso.profile_id = 1
permiso.save

permiso = Permission.new
permiso.view_id = 13
permiso.view_name = View.find(13).name #Sessions
permiso.crear = 8
permiso.editar = 4
permiso.leer = 2
permiso.eliminar = 1
permiso.profile_id = 1
permiso.save

permiso = Permission.new
permiso.view_id = 14
permiso.view_name = View.find(14).name #User Attention Priority Tran
permiso.crear = 8
permiso.editar = 4
permiso.leer = 2
permiso.eliminar = 1
permiso.profile_id = 1
permiso.save

permiso = Permission.new
permiso.view_id = 15
permiso.view_name = View.find(15).name #User Attention Priority Perf
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 1
permiso.save

permiso = Permission.new
permiso.view_id = 16
permiso.view_name = View.find(16).name #Filter categorization
permiso.crear = 8
permiso.editar = 4
permiso.leer = 2
permiso.eliminar = 1
permiso.profile_id = 1
permiso.save

permiso = Permission.new
permiso.view_id = 17
permiso.view_name = View.find(17).name #States
permiso.crear = 8
permiso.editar = 4
permiso.leer = 2
permiso.eliminar = 1
permiso.profile_id = 1
permiso.save

permiso = Permission.new
permiso.view_id = 18
permiso.view_name = View.find(18).name #Email Groups
permiso.crear = 8
permiso.editar = 4
permiso.leer = 2
permiso.eliminar = 1
permiso.profile_id = 1
permiso.save

permiso = Permission.new
permiso.view_id = 19
permiso.view_name = View.find(19).name #Schedule configuration
permiso.crear = 8
permiso.editar = 4
permiso.leer = 2
permiso.eliminar = nil
permiso.profile_id = 1
permiso.save


permiso = Permission.new
permiso.view_id = 20
permiso.view_name = View.find(20).name #Views
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 1
permiso.save

##################### PERFIL Default CENTRAL ######################
perfil = Profile.new
perfil.name = 'Default Central'
perfil.area_id = 1
perfil.flag = 1
perfil.save

permiso = Permission.new
permiso.view_id = 1
permiso.view_name = View.find(1).name # Home
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save

permiso = Permission.new
permiso.view_id = 2
permiso.view_name = View.find(2).name #Alert Levels
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save

permiso = Permission.new
permiso.view_id = 3
permiso.view_name = View.find(3).name #Filter Categorization Graphs
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save

permiso = Permission.new
permiso.view_id = 4
permiso.view_name = View.find(4).name #New Alert transaccional
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save

permiso = Permission.new
permiso.view_id = 5
permiso.view_name = View.find(5).name #New Alert Perfiles
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save

permiso = Permission.new
permiso.view_id = 6
permiso.view_name = View.find(6).name #Consult Alert transaccional
permiso.crear = 8
permiso.editar = nil
permiso.leer = 2
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save

permiso = Permission.new
permiso.view_id = 7
permiso.view_name = View.find(7).name #Consult Alert perfiles
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save

permiso = Permission.new
permiso.view_id = 8
permiso.view_name = View.find(8).name # Alert Release
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save

permiso = Permission.new
permiso.view_id = 9
permiso.view_name = View.find(9).name #General Search
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save

permiso = Permission.new
permiso.view_id = 10
permiso.view_name = View.find(10).name #Users
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save

permiso = Permission.new
permiso.view_id = 11
permiso.view_name = View.find(11).name #Profiles
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save

permiso = Permission.new
permiso.view_id = 12
permiso.view_name = View.find(12).name #Areas
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save

permiso = Permission.new
permiso.view_id = 13
permiso.view_name = View.find(13).name #Sessions
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save

permiso = Permission.new
permiso.view_id = 14
permiso.view_name = View.find(14).name #User Attention Priority Tran
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save

permiso = Permission.new
permiso.view_id = 15
permiso.view_name = View.find(15).name #User Attention Priority Perf
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save

permiso = Permission.new
permiso.view_id = 16
permiso.view_name = View.find(16).name #Filter categorization
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save

permiso = Permission.new
permiso.view_id = 17
permiso.view_name = View.find(17).name #States
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save

permiso = Permission.new
permiso.view_id = 18
permiso.view_name = View.find(18).name #Email Groups
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save

permiso = Permission.new
permiso.view_id = 19
permiso.view_name = View.find(19).name #Schedule configuration
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save


permiso = Permission.new
permiso.view_id = 20
permiso.view_name = View.find(19).name #Views
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.save
#
# # ##################### Estados de alertas ######################
# tiporeg = Ttiporegalarma.new
# tiporeg.IdTipoReg = 1
# tiporeg.Descripcion = "Logon"
# tiporeg.save
#
# tiporeg = Ttiporegalarma.new
# tiporeg.IdTipoReg = 2
# tiporeg.Descripcion = "Logoff"
# tiporeg.save
#
# tiporeg = Ttiporegalarma.new
# tiporeg.IdTipoReg = 3
# tiporeg.Descripcion = "Alertas"
# tiporeg.save
#
# tiporeg = Ttiporegalarma.new
# tiporeg.IdTipoReg = 4
# tiporeg.Descripcion = "Error Aplicación"
# tiporeg.save
#
# tiporeg = Ttiporegalarma.new
# tiporeg.IdTipoReg = 5
# tiporeg.Descripcion = "Borrar Registros BD"
# tiporeg.save
#
# tiporeg = Ttiporegalarma.new
# tiporeg.IdTipoReg = 6
# tiporeg.Descripcion = "Tickets"
# tiporeg.save
#
# tiporeg = Ttiporegalarma.new
# tiporeg.IdTipoReg = 7
# tiporeg.Descripcion = "Cambio de configuración"
# tiporeg.save
#
# tiporeg = Ttiporegalarma.new
# tiporeg.IdTipoReg = 8
# tiporeg.Descripcion = "Avisos"
# tiporeg.save
#
# -------- NUEVOS --------
# tiporeg = Ttiporegalarma.new
# tiporeg.IdTipoReg = 9
# tiporeg.Descripcion = "Create"
# tiporeg.save
#
# tiporeg = Ttiporegalarma.new
# tiporeg.IdTipoReg = 10
# tiporeg.Descripcion = "Edit"
# tiporeg.save
#
# tiporeg = Ttiporegalarma.new
# tiporeg.IdTipoReg = 11
# tiporeg.Descripcion = "Index"
# tiporeg.save
#
# tiporeg = Ttiporegalarma.new
# tiporeg.IdTipoReg = 12
# tiporeg.Descripcion = "Destroy"
# tiporeg.save
#
# tiporeg = Ttiporegalarma.new
# tiporeg.IdTipoReg = 13
# tiporeg.Descripcion = "Show"
# tiporeg.save

# tiporeg = Ttiporegalarma.new
# tiporeg.IdTipoReg = 14
# tiporeg.Descripcion = "Login"
# tiporeg.save

# tiporeg = Ttiporegalarma.new
# tiporeg.IdTipoReg = 15
# tiporeg.Descripcion = "Mailer"
# tiporeg.save

# tiporeg = Ttiporegalarma.new
# tiporeg.IdTipoReg = 16
# tiporeg.Descripcion = "Logout"
# tiporeg.save

#
# -------- APLICACION PARA GESTOR CENTRAL --------
# apl = Taplicacioneskm.new
# apl.IdAplicacion = 25
# apl.Nombre = "Gestor Central"
# apl.idProducto = 3
# apl.save